# Tissue Analysis

## Authors:
* Jonathan Legrand (jonathan.legrand@ens-lyon.fr)
* Frédéric Boudon (frederic.boudon@cirad.fr)


## Institutes:
* RDP, ENS de Lyon - UMR 5667
* CNRS (http://www.cnrs.fr)
* INRIA (http://www.inria.fr)
* CIRAD (http://www.cirad.fr)


## License:
* `Cecill-C`

## Description

An Python library providing image analysis tools to extract quantitative information from 3D segmented tissue images.

## Requirements


* timagetk 
* cellcomplex

## Installation

```
python setup.py develop --user
```