import unittest
import numpy as np

from tissue_analysis.array_tools import find_geometric_median, \
    projection_matrix
from tissue_analysis.array_tools import pointset_subspace_projection

# 2D square used to test affine deformation estimation:
xyz_2d_square = np.array([[0, 0], [1, 0], [0, 1], [1, 1]], dtype=np.float)
# 3D cube used to test affine deformation estimation:
xyz_3d_cube = np.array(
    [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0], [0, 0, 1], [1, 0, 1],
     [0, 1, 1], [1, 1, 1]], dtype=np.float)
# 3D pyramid:
xyz_3d_pyramid = np.array(
    [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0], [0.5, 0.5, 1]], dtype=np.float)


class TestArrayTools(unittest.TestCase):
    def test_geometric_median(self):
        """Test the detection of the geometric median of a set of 3D points."""
        points_3d = np.array(
            [[0, 0, 0], [0, 2, 0], [0, 3, 0], [0, 4, 0], [0, 6, 0]])
        geom_med_id = find_geometric_median(points_3d)
        self.assertEqual(2, geom_med_id)
        points_3d = np.array(
            [[0, 0, 1], [0, 2, 0], [0, 3, 1], [0, 4, 0], [0, 6, 1]])
        geom_med_id = find_geometric_median(points_3d)
        self.assertEqual(2, geom_med_id)
        # Cube with a point (last one) at its barycenter:
        points_3d = np.array(
            [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0], [0, 0, 1], [1, 0, 1],
             [0, 1, 1], [1, 1, 1], [0.5, 0.5, 0.5]])
        geom_med_id = find_geometric_median(points_3d)
        self.assertEqual(8, geom_med_id)

    def test_subspace_projection(self):
        """Test 2D subspace projection of a set of 3D points."""
        # Set of points is the 3D box of size: X=3, Y=2, Z=0.5
        st_x, st_y, st_z = 3, 2, 0.5
        A = np.array([[st_x, 0, 0], [0, st_y, 0], [0, 0, st_z]], dtype=np.float)
        points_3d = np.dot(xyz_3d_cube.copy(), A)
        points_2d = pointset_subspace_projection(points_3d)
        # The smallest dimension is in Z so they should all project halfway in Z (at 0.25) while staying at the same coordinates in X & Y:
        true_points_2d = points_3d.copy()
        true_points_2d[:, 2] = 0.25
        np.testing.assert_almost_equal(true_points_2d, points_2d, decimal=8)
        # -- Now make X the smallest dimension:
        # Set of points is the 3D box of size: X=1, Y=2, Z=3
        st_x, st_y, st_z = 1, 2, 3
        A = np.array([[st_x, 0, 0], [0, st_y, 0], [0, 0, st_z]], dtype=np.float)
        points_3d = np.dot(xyz_3d_cube.copy(), A)
        points_2d = pointset_subspace_projection(points_3d)
        # The smallest dimension is now in X so they should all project halfway in X (at 0.5) while staying at the same coordinates in Y & Z:
        true_points_2d = points_3d.copy()
        true_points_2d[:, 0] = 0.5
        np.testing.assert_almost_equal(true_points_2d, points_2d, decimal=8)

    def test_projection_matrix(self):
        """Test 2D subspace projection of a set of 3D points."""
        # Set of points is the 3D box of size: X=3, Y=2, Z=0.5
        st_x, st_y, st_z = 3, 2, 0.5
        A = np.array([[st_x, 0, 0], [0, st_y, 0], [0, 0, st_z]], dtype=np.float)
        points_3d = np.dot(xyz_3d_cube.copy(), A)
        proj_mat = projection_matrix(points_3d, 2)
        center = np.mean(points_3d, 0)
        points_2d = np.dot(points_3d - center, proj_mat) + center
        true_points_2d = pointset_subspace_projection(points_3d)
        np.testing.assert_almost_equal(true_points_2d, points_2d, decimal=8)


