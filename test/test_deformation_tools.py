import unittest
import numpy as np

from tissue_analysis.array_tools import affine_deformation_tensor
from tissue_analysis.array_tools import stretch_tensors
from tissue_analysis.array_tools import pointset_subspace_projection

# 2D square used to test affine deformation estimation:
xyz_2d_square = np.array([[0, 0], [1, 0], [0, 1], [1, 1]], dtype=np.float)
# 3D cube used to test affine deformation estimation:
xyz_3d_cube = np.array(
    [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0], [0, 0, 1], [1, 0, 1],
     [0, 1, 1], [1, 1, 1]], dtype=np.float)
# 3D pyramid:
xyz_3d_pyramid = np.array(
    [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0], [0.5, 0.5, 1]], dtype=np.float)


class TestDeformationTools(unittest.TestCase):
    def test_2d_no_deformation(self):
        """Test a simple 2D affine deformation estimation without deformation."""
        # Landmarks @ T_n:
        xyz_t1 = xyz_2d_square.copy()
        xyz_t2 = xyz_2d_square.copy()
        A_est, r2 = affine_deformation_tensor(xyz_t1, xyz_t2)
        np.testing.assert_almost_equal(np.diag([1, 1]), A_est, decimal=8)

    def test_2d_affine_deformation(self):
        """Test a simple 2D affine deformation."""
        # Landmarks @ T_n:
        xyz_t1 = xyz_2d_square.copy()
        # Create deformation matrix: 3x in X, 2x in Y:
        A = np.array([[3, 0], [0, 2]])
        # Generate the landmarks @ T_n+1:
        xyz_t2 = np.dot(xyz_t1, A)
        A_est, r2 = affine_deformation_tensor(xyz_t1, xyz_t2)
        np.testing.assert_almost_equal(A, A_est, decimal=8)

    def test_2d_affine_deformation_with_translation(self):
        """Test 2D affine deformation with translation."""
        # Landmarks @ T_n:
        xyz_t1 = xyz_2d_square.copy()
        # Create deformation matrix: 3x in X, 2x in Y:
        A = np.array([[3, 0], [0, 2]])
        # Generate the landmarks @ T_n+1 & add a translation:
        xyz_t2 = np.dot(xyz_t1, A) + np.array([1, 1])
        A_est, r2 = affine_deformation_tensor(xyz_t1, xyz_t2)
        np.testing.assert_almost_equal(A, A_est, decimal=8)

    def test_2d_stretch_tensor_estimation(self):
        """Test the estimation of 2D stretch tensors."""
        # Landmarks @ T_n:
        xyz_t1 = xyz_2d_square.copy()
        # Create deformation matrix: 3x in X, 2x in Y:
        st_x, st_y = 3, 2
        A = np.array([[st_x, 0], [0, st_y]], dtype=np.float)
        # Generate the landmarks @ T_n+1:
        xyz_t2 = np.dot(xyz_t1, A)
        A_est, r2 = affine_deformation_tensor(xyz_t1, xyz_t2)
        R, D, Qt = stretch_tensors(A_est)
        st_x_est, st_y_est = D
        np.testing.assert_almost_equal([st_x, st_y], [st_x_est, st_y_est],
                                       decimal=8)

    def test_3d_affine_deformation(self):
        """Test a simple 3D affine deformation."""
        # Landmarks @ T_n:
        xyz_t1 = xyz_3d_cube.copy()
        # Create deformation matrix: 3x in X, 2x in Y & 1/2 in Z:
        A = np.array([[3, 0, 0], [0, 2, 0], [0, 0, 0.5]], dtype=np.float)
        # Generate the landmarks @ T_n+1:
        xyz_t2 = np.dot(xyz_t1, A)
        A_est, r2 = affine_deformation_tensor(xyz_t1, xyz_t2)
        np.testing.assert_almost_equal(A, A_est, decimal=8)

    def test_3d_affine_deformation_with_translation(self):
        """Test 3D affine deformation with translation."""
        # Landmarks @ T_n:
        xyz_t1 = xyz_3d_cube.copy()
        # Create deformation matrix: 3x in X, 2x in Y & 1/2 in Z:
        A = np.array([[3, 0, 0], [0, 2, 0], [0, 0, 0.5]], dtype=np.float)
        # Generate the landmarks @ T_n+1 & add a translation:
        xyz_t2 = np.dot(xyz_t1, A) + np.array([2, 1, 0.5])
        A_est, r2 = affine_deformation_tensor(xyz_t1, xyz_t2)
        np.testing.assert_almost_equal(A, A_est, decimal=8)

    def test_3d_stretch_tensor_estimation(self):
        """Test the estimation of 3D stretch tensors."""
        # Landmarks @ T_n:
        xyz_t1 = xyz_3d_cube.copy()
        # Create deformation matrix: 3x in X, 2x in Y & 1/2 in Z:
        st_x, st_y, st_z = 3, 2, 0.5
        A = np.array([[st_x, 0, 0], [0, st_y, 0], [0, 0, st_z]], dtype=np.float)
        # Generate the landmarks @ T_n+1:
        xyz_t2 = np.dot(xyz_t1, A)
        A_est, r2 = affine_deformation_tensor(xyz_t1, xyz_t2)
        R, D, Qt = stretch_tensors(A_est)
        st_x_est, st_y_est, st_z_est = D
        np.testing.assert_almost_equal([st_x, st_y, st_z],
                                       [st_x_est, st_y_est, st_z_est],
                                       decimal=8)

    def test_subspace_projection_deformation_estimation(self):
        """Test 2D subspace projection of a set of 3D points & deformation estimation."""
        # Landmarks @ T_n:
        xyz_t1 = xyz_3d_pyramid.copy()
        # Create deformation matrix: 3x in X, 2x in Y & 1/2 in Z:
        st_x, st_y, st_z = 3, 2, 0.5
        A = np.array([[st_x, 0, 0], [0, st_y, 0], [0, 0, st_z]], dtype=np.float)
        # Generate the landmarks @ T_n+1:
        xyz_t2 = np.dot(xyz_t1, A)
        # Flatten T_n & T_n+1 landmarks:
        flat_xyz_t1 = pointset_subspace_projection(xyz_t1)
        flat_xyz_t2 = pointset_subspace_projection(xyz_t2)
        A_est, r2 = affine_deformation_tensor(flat_xyz_t1, flat_xyz_t2)
        A[2, 2] = 0
        np.testing.assert_almost_equal(A, A_est, decimal=8)
        R, D, Qt = stretch_tensors(A_est)
        st_x_est, st_y_est, st_z_est = D
        np.testing.assert_almost_equal([st_x, st_y, 0],
                                       [st_x_est, st_y_est, st_z_est],
                                       decimal=8)