
import unittest

import numpy as np

from timagetk.components import SpatialImage

from tissue_analysis.property_spatial_image import PropertySpatialImage

def square_tissue_image(voxelsize=(0.25,0.25,0.5)):
    size = np.round(20*np.ones(3)/np.array(voxelsize)).astype(int)

    array = np.ones(tuple(size),np.uint8)

    array[:size[0]//2,:size[1]//3,size[2]//2:] = 2
    array[size[0]//2:,:2*size[1]//3,size[2]//2:] = 3
    array[size[0]//2:,2*size[1]//3:,size[2]//2:] = 4
    array[:size[0]//2,size[1]//3:,size[2]//2:] = 5

    return SpatialImage(array, voxelsize=voxelsize)


class TestPropertySpatialImage(unittest.TestCase):

    def setUp(self):
        self.seg_img = square_tissue_image()
        self.p_img = None

    def tearDown(self):
        pass

    def test_property_spatial_image(self):
        self.p_img = PropertySpatialImage(self.seg_img)

    def test_barycenter_property(self):
        """Test the computation of the barycenter property on an image."""
        self.p_img = PropertySpatialImage(self.seg_img)
        self.p_img.compute_image_property('barycenter')
        cell_z = 3*self.seg_img.shape[2]*self.seg_img.voxelsize[2]/4
        assert np.all(np.isclose(self.p_img.image_property('barycenter').values()[:,2],cell_z,atol=self.seg_img.voxelsize[2]))
        
    def test_volume_property(self):
        """Test the computation of the volume property on an image."""
        self.p_img = PropertySpatialImage(self.seg_img)
        self.p_img.compute_image_property('volume')
        assert np.all([label in self.p_img.image_property('volume').keys() for label in [2,3,4,5]])

    def test_neighbors_property(self):
        """Test the computation of the layer property on an image."""
        self.p_img = PropertySpatialImage(self.seg_img)
        self.p_img.compute_image_property('neighborhood_size')
        assert np.all([label in self.p_img.image_property('neighborhood_size').keys() for label in [2,3,4,5]])

    def test_layer_property(self):
        """Test the computation of the layer property on an image."""
        self.p_img = PropertySpatialImage(self.seg_img)
        self.p_img.compute_image_property('layer')
        assert np.all(self.p_img.image_property('layer').values()==1)

    def test_image_meshes(self):
        self.p_img = PropertySpatialImage(self.seg_img)
        self.p_img.compute_cell_meshes(sub_factor=4)

