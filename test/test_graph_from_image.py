
import unittest

import numpy as np

from timagetk.components import SpatialImage

from tissue_analysis.temporal_graph_from_image import graph_from_image

def square_tissue_image(voxelsize=(0.25,0.25,0.5)):
    size = np.round(20*np.ones(3)/np.array(voxelsize)).astype(int)

    array = np.ones(tuple(size),np.uint8)

    array[:size[0]//2,:size[1]//3,size[2]//2:] = 2
    array[size[0]//2:,:2*size[1]//3,size[2]//2:] = 3
    array[size[0]//2:,2*size[1]//3:,size[2]//2:] = 4
    array[:size[0]//2,size[1]//3:,size[2]//2:] = 5

    return SpatialImage(array, voxelsize=voxelsize)


class TestGraphFromImage(unittest.TestCase):

    def setUp(self):
        self.seg_img = square_tissue_image()
        self.img_graph = None

    def tearDown(self):
        pass

    def test_graph_from_image(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=[],
                                          ignore_cells_at_stack_margins=False)
        assert len(list(self.img_graph.vertices())) == 4

    def test_graph_boundingbox_property(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=['boundingbox'],
                                          ignore_cells_at_stack_margins=False)

    def test_graph_volume_property(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=['volume'],
                                          ignore_cells_at_stack_margins=False)

    def test_graph_barycenter_property(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=['barycenter'],
                                          ignore_cells_at_stack_margins=False)

    def test_graph_L1_property(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=['L1'],
                                          ignore_cells_at_stack_margins=False)

    def test_graph_L2_property(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=['L2'],
                                          ignore_cells_at_stack_margins=False)

    def test_graph_border_property(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=['border'],
                                          ignore_cells_at_stack_margins=False)

    def test_graph_inertia_axis_property(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=['inertia_axis'],
                                          ignore_cells_at_stack_margins=False)

    def test_graph_wall_area_property(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=['wall_area'],
                                          ignore_cells_at_stack_margins=False)

    def test_graph_epidermis_area_property(self):
        self.img_graph = graph_from_image(self.seg_img,
                                          spatio_temporal_properties=['epidermis_area'],
                                          ignore_cells_at_stack_margins=False)
