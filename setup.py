#!/usr/bin/env python
# -*- coding: utf-8 -*-

# {# pkglts, pysetup.kwds
# format setup arguments

from setuptools import setup, find_packages


short_descr = "This package enable the analysis of 4D tissue data using spatio-temporal cellular features extraction, organisation and clustering."
readme = open('README.rst').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')



setup_kwds = dict(
    name='tissue_analysis',
    version="0.10.0",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="Jonathan Legrand",
    author_email="jonathan.legrand AT ens-lyon.fr",
    url='https://https://gitlab.inria.fr/mosaic/tissue_analysis',
    license='cecill-c',
    zip_safe=False,

    packages=pkgs,
    package_dir={'': 'src'},
    setup_requires=[
        ],
    install_requires=[
        ],
    tests_require=[
        "coverage",
        "mock",
        "nose",
        ],
    entry_points={},
    keywords='',
    
    test_suite='nose.collector',
    )
# #}
# change setup_kwds below before the next pkglts tag

#setup_kwds['entry_points']['oalab.world'] = ['oalab.world/tissue_analysis = tissue_analysis.tissue_analysis_oalab.plugin.world']

# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}
