# -*- python -*-
# -*- coding: utf-8 -*-
#
#       TissueAnalysis
#
#       Copyright 2018 CNRS - ENS Lyon
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# -----------------------------------------------------------------------------

"""
Regroup functions dedicated to the transformation of array based coordinates.
"""
import copy
import math
import warnings

import numpy as np
from scipy import ndimage as nd
from sklearn import linear_model, metrics
from scipy.linalg import svd


def coordinates_centering2D(coordinates, mean=None):
    """Center a set of 'coordinates' around their 'mean'.

    Parameters
    ----------
    coordinates : np.array
        a set of coordinates to center
    mean : list|np.array, optional
        if None (default) the mean is computed before centering, else should be
        a length-2 list or array

    Returns
    -------
    np.array
        the centered set of coordinates
    """
    try:
        x, y = coordinates
    except ValueError:
        x, y = coordinates.T
    if mean is None:
        mean = np.mean(np.array([x, y]), 1)
    # Now perform centering operation:
    x = x - mean[0]
    y = y - mean[1]
    return np.array([x, y])


def coordinates_centering3D(coordinates, mean=None):
    """Center a set of 'coordinates' around their 'mean'.

    Parameters
    ----------
    coordinates : np.array
        a set of coordinates to center
    mean : list|np.array, optional
        if None (default) the mean is computed before centering, else should be
        a length-3 list or array

    Returns
    -------
    np.array
        the centered set of coordinates
    """
    try:
        x, y, z = coordinates
    except ValueError:
        x, y, z = coordinates.T
    if mean is None:
        mean = np.mean(np.array([x, y, z]), 1)
    # Now perform centering operation:
    x = x - mean[0]
    y = y - mean[1]
    z = z - mean[2]
    return np.array([x, y, z])


def compute_covariance_matrix(coordinates):
    """Function computing the covariance matrix of a given set of 'coordinates'.

    The covariance matrix of X, a set of coordinate is computed as follow:
        cov(X) = 1/N * X . X^T,
    where:
        - N is the number of coordinate;
        - '.' is the dot product;
        - '^T' is the transposes matrix

    Parameters
    ----------
    coordinates : np.array
        poinset of coordinates

    Returns
    ------
    np.array
        the covariance matrix.
    """
    try:
        assert max(coordinates.shape) > 3
    except:
        raise ValueError('Too small number of coordinates!')
    if not isinstance(coordinates, np.ndarray):
        coordinates = np.array(coordinates)
    if coordinates.shape[0] > 3:
        coordinates = coordinates.T
    return 1. / max(coordinates.shape) * np.dot(coordinates, coordinates.T)


def eigen_values_vectors(cov_matrix):
    """Extract the eigen vectors and associated values from a covariance matrix.

    Parameters
    ----------
    cov_matrix : np.array
        poinset of coordinates

    Returns
    -------
    eig_val : np.array
        length-3 list of sorted eigen values associated to the eigen vectors
    eig_vec : np.array
        3x3 np.array of eigen vectors --by rows-- associated to sorted eigen values
    """
    assert max(cov_matrix.shape) <= 3
    # np.linalg.eig return eigenvectors by column !!
    eig_val, eig_vec = np.linalg.eig(cov_matrix)
    decreasing_index = eig_val.argsort()[::-1]
    eig_val, eig_vec = eig_val[decreasing_index], eig_vec[:, decreasing_index]
    eig_vec = np.array(eig_vec).T  # ... our standard is by rows !
    return eig_val, eig_vec


def find_geometric_median(coordinates):
    """Function finding the geometric median of an array of coordinates

    Parameters
    ----------
    coordinates : np.array
        a Nx3 array of coordinates.

    Returns
    -------
    int
        the index of the geometric median point for the array of coordinates.

    Example:
    --------
    >>> from tissue_analysis.array_tools import find_geometric_median
    >>> ar = np.array([[0,0,0], [0,1,0], [0,2,0], [0,3,0], [0,4,0]])
    >>> find_geometric_median(ar)
    2
    >>> # To get the median coordinates use the index:
    >>> ar[2]
    [0, 2, 0]
    """
    nax = np.newaxis
    # Need an array with 3D coordinates as rows:
    if coordinates.shape[0] == 3:
        coordinates = coordinates.T
    # Compute the pairwise distance matrix:
    pw_dist = np.linalg.norm(coordinates[:, nax] - coordinates[nax, :], axis=2)
    # Return the index of the point with the shortest distance to all others: the median voxel!
    return np.argmin(pw_dist.sum(axis=1))


def projection_matrix(coordinates, subspace_rank=2):
    """Compute the projection matrix of a coordinates according to given subspace_rank.

    Parameters
    ----------
    coordinates : np.array
        list of coordinates of shape (n_points x init_dim), with n_points >= init_dim.
    subspace_rank : int, optional
        the dimension reduction to apply, with init_dim > subspace_rank > 0.

    Returns
    -------
    np.array
        the (d x d) projection matrix, with d=subspace_rank
    """
    coordinates = np.array(coordinates)
    n_points, init_dim = coordinates.shape
    if not n_points >= init_dim:
        raise ValueError(
            "The `coordinates` parameter should be of shape (n_points, init_dim), with n_points >= init_dim")
    if not init_dim > subspace_rank:
        raise ValueError(
            "The subspace rank in which to project the `coordinates` should be inferior to its initial dimensionallity.")
    assert subspace_rank > 0
    # -- Compute the centered coordinates:
    centroid = coordinates.mean(axis=0)
    if sum(centroid) != 0:
        # - Compute the centered matrix:
        centered_coordinates = coordinates - centroid
    else:
        centered_coordinates = coordinates
    # -- Compute the Singular Value Decomposition (SVD) of centered coordinates:
    U, D, V = np.linalg.svd(centered_coordinates, full_matrices=False)
    V = V.T
    # -- Compute the projection matrix:
    H = np.dot(V[:, 0:subspace_rank], V[:, 0:subspace_rank].T)
    return H


def pointset_subspace_projection(coordinates, subspace_rank=2):
    """Project 'pointset' array onto its subspace of rank 'subspace_rank'.

    Parameters
    ----------
    coordinates : np.array
        list of coordinates of shape (n_points x init_dim), with n_points >= init_dim
    dimension_reduction : int
        the dimension reduction to apply, with init_dim > subspace_rank > 0

    Returns
    -------
    np.array
        the (n_points x subspace_rank) project pointset array

    Raises
    ------
    ValueError
        if n_points <= init_dim
        if init_dim >= subspace_rank

    Examples
    --------
    >>> import numpy as np
    >>> from tissue_analysis.array_tools import pointset_subspace_projection
    >>> coords = np.array([[1., 1., 1.], [5., 1., 1.], [1., 5., 1.], [5., 5., 1.], [5., 2., 1.]])
    >>> coords_2d = pointset_subspace_projection(coords, 2)

    """
    coordinates = np.array(coordinates)
    n_points, init_dim = coordinates.shape
    if not n_points >= init_dim:
        raise ValueError(
            "The `coordinates` parameter should be of shape (n_points, init_dim), with n_points >= init_dim")
    if not init_dim > subspace_rank:
        raise ValueError(
            "The subspace rank in which to project the `coordinates` should be inferior to its initial dimensionallity.")
    assert subspace_rank > 0
    # -- Compute the centered coordinates:
    centroid = coordinates.mean(axis=0)
    if sum(centroid) != 0:
        # - Compute the centered matrix:
        centered_coordinates = coordinates - centroid
    else:
        centered_coordinates = coordinates
    # Get the projection matrix and the centered coordinates:
    proj_mat = projection_matrix(coordinates, subspace_rank)
    # Return the subspace projection of the coordinates:
    return np.dot(centered_coordinates, proj_mat) + centroid


def pairwise_distance_matrix(x, y):
    """ Compute the pairwise distance from two arrays of coordinates.

    Parameters
    ----------
    x : np.ndarray
        a [N, D] array with N the number of coordinates and D their dimensionality
    y : np.ndarray
        a [N, D] array with N the number of coordinates and D their dimensionality

    Returns
    -------
    np.ndarray
        a [N, N] pairwise distance matrix
    """
    from numpy.linalg import norm
    return norm(y[np.newaxis, :] - x[:, np.newaxis], axis=2)


def affine_deformation_tensor(xyz_t1, xyz_t2):
    """ Compute the affine deformation matrix between two time-points (t1 & t2).

    With `xyz_t1` and `xyz_t2` being the landmarks coordinates, the affine
    transformation matrix A is obtained as follow:
        xyz_t2 = A . xyz_t1 + epsilon,
    where epsilon is the registration error.

    Parameters
    ----------
    xyz_t1 : np.array
        (N x d) matrix giving the N landmarks coordinates before deformation;
    xyz_t2 : np.array
        (N x d) matrix giving the N landmarks coordinates after deformation.
    With 'd' the dimensionality (i.e. d=2 if 2D, d=3 if 3D)

    Returns
    -------
    np.array
        (d x d) matrix giving the affine transformation matrix between the
        centered vertices position of two time points;
    float
        the r2 score, assessing the quality of the registration.

    Note
    ----
    R^2 score function also available in sklearn: sklearn.metrics.r2_score
    R^2 (coefficient of determination) regression score function.
    Best possible score is 1.0 and it can be negative (because the model can be arbitrarily worse).
    A constant model that always predicts the expected value of y, disregarding the input features, would get a R^2 score of 0.0.

    Examples
    --------

    """
    regr = linear_model.LinearRegression(fit_intercept=True, normalize=False)
    regr.fit(xyz_t1, xyz_t2)
    # return regr.coef_, regr.score(xyz_t1, xyz_t2)
    return regr.coef_, metrics.r2_score(xyz_t1, xyz_t2)


def stretch_tensors(deformation_matrix):
    """ Compute the stretch tensors, before & after registration, and the
    associated norms.

    Parameters
    ----------
    deformation_matrix : np.array
        (d x d) deformation matrix (typically from 'affine_deformation_tensor');

    Returns
    -------
    R : list
        length-d list of d-vectors giving the before-deformation directions;
    D : list
        length-d list of floats giving the extent of the deformation in each directions;
    Q : list
        length-d list of d-vectors after-deformation directions.
    """
    ##  Singular Value Decomposition (SVD) of `deformation_matrix`.
    R, D, Q = svd(deformation_matrix)
    return R, D, Q.T


def fractional_anisotropy_eigenval(eigenvalues):
    """Compute the fractional anisotropy of a diffusion tensor.

    Let $D$ be the diffusion tensor and $\lambda_i$ its eigenvalues:
    $$ \text{FA} = \sqrt{\frac{3}{2}} \frac{\sqrt{(\lambda_1 - \hat{\lambda})^2 + (\lambda_2 - \hat{\lambda})^2 + (\lambda_3 - \hat{\lambda})^2}}{\sqrt{\lambda_1^2 + \lambda_2^2 + \lambda_3^2}}$$
    with the mean $\hat{\lambda} = (\lambda_1 + \lambda_2 + \lambda_3)/3$

    Parameters
    ----------
    eigenvalues : list
        list of eigenvalues of a diffusion tensor

    Returns
    -------
    float
        the fractional anisotropy as defined above

    Note
    ----
    Can not be negative!

    """
    assert len(eigenvalues) == 3
    l1, l2, l3 = eigenvalues
    if (l1 + l2 + l3) == 0:
        return None

    l = (l1 + l2 + l3) / 3.
    return math.sqrt(3. / 2.) * (
            math.sqrt(
                (l1 - l) ** 2 + (l2 - l) ** 2 + (l3 - l) ** 2) / math.sqrt(
        l1 ** 2 + l2 ** 2 + l3 ** 2))


def fractional_anisotropy_tensor(stretch_tensor):
    """
    Compute fractional anisotropy of a diffusion tensor $D$.
    $$ \text{FA} = \sqrt{\frac{1}{2}} (3 - \frac{1}{trace(R^2)} $$
    where R is the "normalized" diffusion tensor: $R=frac{D}/{trace(D)}$.
    """
    assert stretch_tensor.shape == tuple([3, 3])
    D = stretch_tensor
    R = D / np.matrix.trace(D)
    return math.sqrt(1. / 2. * (3 - (1. / np.matrix.trace(R ** 2))))


def strain_anisotropy_2d(eigenval):
    """ Compute the 2D strain anisotropy.

    Parameters
    ----------
    eigenval : list
        length-2 list of strain values

    Returns
    -------
    float
        the 2D strain anisotropy
    """
    l1, l2 = eigenval
    return (l1 - l2) / (l1 + l2)


def sort_boundingbox(boundingbox, label_1, label_2):
    """Use this to determine which label as the smaller boundingbox !"""
    assert isinstance(boundingbox, dict)
    if (label_1 not in boundingbox) and label_2 in boundingbox:
        return label_2, label_1
    if label_1 in boundingbox and (label_2 not in boundingbox):
        return label_1, label_2
    if (label_1 not in boundingbox) and (
    label_2 not in boundingbox):
        return None, None

    bbox_1 = boundingbox[label_1]
    bbox_2 = boundingbox[label_2]
    vol_bbox_1 = (bbox_1[0].stop - bbox_1[0].start) * (
                bbox_1[1].stop - bbox_1[1].start) * (
                             bbox_1[2].stop - bbox_1[2].start)
    vol_bbox_2 = (bbox_2[0].stop - bbox_2[0].start) * (
                bbox_2[1].stop - bbox_2[1].start) * (
                             bbox_2[2].stop - bbox_2[2].start)

    return (label_1, label_2) if vol_bbox_1 < vol_bbox_2 else (label_2, label_1)


def find_smallest_boundingbox(image, label_1, label_2):
    """Return the smallest boundingbox within `image` between cell-labels `label_1` & `label_2`."""
    boundingbox = nd.find_objects(image, max_label=max([label_1, label_2]))
    boundingbox = {label_1: boundingbox[label_1 - 1], label_2: boundingbox[
        label_2 - 1]}  # we do 'label_x - 1' since 'nd.find_objects' start at '1' (and not '0') !
    label_1, label_2 = sort_boundingbox(boundingbox, label_1, label_2)
    return boundingbox[label_1]


def euclidean_distance(ptsA, ptsB):
    """Function computing the Euclidean distance between two points.
    Can be 2D or 3D coordinates.

    Args:
      ptsA: (list/numpy.array) - 2D/3D coordinates of point A;
      ptsB: (list/numpy.array) - 2D/3D coordinates of point B;

    Returns:
      The Euclidean distance between points A & B.
    """
    if len(ptsA) != len(ptsB):
        warnings.warn("It seems that the points are not in the same space!")
        return None

    if len(ptsA) == 2:
        return math.sqrt((ptsA[0] - ptsB[0]) ** 2 + (ptsA[1] - ptsB[1]) ** 2)

    if len(ptsA) == 3:
        return math.sqrt((ptsA[0] - ptsB[0]) ** 2 + (ptsA[1] - ptsB[1]) ** 2 + (
                    ptsA[2] - ptsB[2]) ** 2)


def outliers_exclusion(data, std_multiplier=3, display_data_plot=False):
    """
    Return a list or a dict (same type as `data`) cleaned out of outliers.
    Outliers are detected according to a distance from standard deviation.
    """
    from numpy import std, mean
    tmp = copy.deepcopy(data)
    if isinstance(data, list):
        borne = mean(tmp) + std_multiplier * std(tmp)
        N = len(tmp)
        n = 0
        while n < N:
            if (tmp[n] > borne) or (tmp[n] < -borne):
                tmp.pop(n)
                N = len(tmp)
            else:
                n += 1
    if isinstance(data, dict):
        borne = mean(tmp.values()) + std_multiplier * std(tmp.values())
        for n in data:
            if (tmp[n] > borne) or (tmp[n] < -borne):
                tmp.pop(n)
    if display_data_plot:
        import matplotlib.pyplot as plt
        if isinstance(data, list):
            plt.plot(data)
            plt.plot(tmp)
        plt.show()
        if isinstance(data, dict):
            plt.plot(data.values())
            plt.plot(tmp.values())
        plt.show()
    return tmp