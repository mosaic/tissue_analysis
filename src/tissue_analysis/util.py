# -*- python -*-
# -*- coding: utf-8 -*-
#
#       TissueAnalysis
#
#       Copyright 2017 INRIA - CIRAD - INRA - ENS Lyon
#
#       File author(s):
#           Vincent Mirabet <vincent.mirabet@ens-lyon.fr>
#           Jonathan LEGRAND <jonathan.legrand@ens-lyon.fr>
#
###############################################################################

"""
This module regroup a set of miscellaneous methods used across the package.
"""
import math
import time
from os.path import splitext, split


def flatten_list(l):
    """
    Flatten everything that's a list!
    (i.e. [[1,2],3] -> [1,2,3])
    """
    import collections
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, str):
            for sub in flatten_list(el):
                yield sub
        else:
            yield el


def elapsed_time(start, stop=None, round_to=3):
    """
    Return a rounded elapsed time float.

    Parameters
    ----------
    start : float
        start time
    stop : float, optional
        stop time, if None, get it now
    round : int, optional
        number of decimals to returns

    Returns
    -------
    float
        rounded elapsed time
    """
    import time

    if stop is None:
        stop = time.time()

    t = round(stop - start, round_to)
    print("done in {}s".format(t))

    return


def stuple(t):
    """
    Sort tuple 't' and return it

    Parameters
    ----------
    t : tuple
        the tuple to sort

    Returns
    -------
    tuple(sorted(t))
    """
    return tuple(sorted(t))


def rtuple(t):
    """
    Reverse tuple 't' and return it

    Parameters
    ----------
    t : tuple
        the tuple to reverse

    Returns
    -------
    tuple(reversed(t))
    """
    return tuple(reversed(t))


def min_percent_step(N, default_step=5):
    """Compute the minimum step to apply when printing percentage of progress.

    Parameters
    ----------
    N : int
        number of element over which to increment
    default_step : int
        default increment

    Returns
    -------
    minimum_step : int
        the minimum step to use

    """
    return max(default_step, 100 / N)


def percent_progress(progress, n, N, step=None):
    """Print a percentage of progress of n over N.

    Parameters
    ----------
    progress : int
        progress state
    n : int
        iteration number
    N : int
        max iteration number
    step : int
        progress step to apply for printing

    Returns
    -------
    int
        percentage of progress print if any

    """
    if step is None:
        step = min_percent_step(N)

    if n == 0:
        print("0%...")
    if n * 100 / float(N) >= progress + step:
        print("{}%...".format(progress + step))
        progress += step
    if n + 1 == N:
        print("100%")

    return progress


def euclidean_distance(ptsA, ptsB):
    """Compute the Euclidean distance between two points.

    Can be 1D, 2D or 3D coordinates.

    Parameters
    ----------
    ptsA : int|float|list|numpy.array
        1D/2D/3D coordinates of point A. int & float types are for 1D case
    ptsB : int|float|list|numpy.array
        1D/2D/3D coordinates of point B

    Returns
    -------
    float
        the Euclidean distance between points A & B.

    Note
    ----
    int & float types are for 1D case, for 2D or 3D use list or np.array

    """
    # If not 1D, make sure ptsA and ptsB are of same dimensionality
    if len(ptsA) != len(ptsB):
        raise ValueError("It seems that the points are not in the same space!")
    # 2D case:
    if len(ptsA) == 2:
        return math.sqrt(
            (ptsA[0] - ptsB[0]) ** 2 + (ptsA[1] - ptsB[1]) ** 2)
    # 3D case:
    if len(ptsA) == 3:
        return math.sqrt(
            (ptsA[0] - ptsB[0]) ** 2 + (ptsA[1] - ptsB[1]) ** 2 + (
                ptsA[2] - ptsB[2]) ** 2)

def fname_NoExt(fname):
    """
    Return the filename without extension.
    """
    # remove the path:
    fname = split(fname)[1]

    # take care of possible gzip compression:
    fname, ext = splitext(fname)
    if ext == ".gz":
        fname, ext = splitext(fname)

    return fname

def today_date(sep="-"):
    """
    Function returning a string matching the date of the day.
    Format as dd-mm-yyy. It is possible to change the separator.
    """
    t = time.localtime()
    today = [str(t.tm_mday).zfill(2), str(t.tm_mon).zfill(2),
             str(t.tm_year).zfill(4)]
    today_str = sep.join(today)
    return today_str


def today_date_time(date_sep="-", time_sep=":", sep="_"):
    """
    Function returning a string matching the date of the day.
    Format as dd-mm-yyy_hh:mm:ss. It is possible to change the separators.
    """
    t = time.localtime()
    time_list = [str(t.tm_hour).zfill(2), str(t.tm_min).zfill(2),
             str(t.tm_sec).zfill(2)]
    time_str = time_sep.join(time_list)
    today_str = today_date(date_sep)
    return today_str + sep +  time_str
