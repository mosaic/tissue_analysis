# -*- python -*-
# -*- coding: utf-8 -*-
#
#       TissueAnalysis
#
#       Copyright 2006 - 2012 INRIA - CIRAD - INRA
#
#       File author(s):
#           Eric MOSCARDI <eric.moscardi@sophia.inria.fr>
#           Jonathan LEGRAND <jonathan.legrand@ens-lyon.fr>
#           Frederic BOUDON <frederic.boudon@cirad.fr>
#
################################################################################
from timagetk.components.slices import real_indices, dilation
from tissue_analysis.array_tools import sort_boundingbox, \
    find_smallest_boundingbox, compute_covariance_matrix, \
    coordinates_centering3D, eigen_values_vectors
from tissue_analysis.util import percent_progress

__license__ = "Cecill-C"
__revision__ = " $Id$ "

import warnings, copy
import numpy as np, scipy.ndimage as nd, copy as cp

from os.path import split

from timagetk.components import SpatialImage

try:
    from openalea.plantgl.algo import r_neighborhood
    from openalea.plantgl.algo import principal_curvatures
except ImportError:
    print("PlantGL is not installed, no curvature estimation will be available !")

def wall(mask_img, label_id):
    """
    Function detecting wall position for a given cell-id (`label_id`) within a segmented image (`mask_img`).
    """
    img = (mask_img == label_id).get_array().astype(int)
    dil = nd.binary_dilation(img)
    contact = (np.asarray(dil).astype(int) - np.asarray(img).astype(int)).astype(bool)
    return mask_img[contact]


def contact_surface(mask_img, label_id):
    """
    TODO
    """
    img = wall(mask_img, label_id)
    return set(np.unique(img))


def hollow_out_cells(image, background, remove_background=True, verbose=True):
    """
    Laplacian filter used to dectect and return an Spatial Image containing only cell walls.
    (The Laplacian of an image highlights regions of rapid intensity change.)

    Args:
       image: (SpatialImage) - Segmented image (tissu);
       background: (int) - label representing the background (to remove).

    Returns:
       m: (SpatialImage) - Spatial Image containing hollowed out cells (only walls).
    """
    if verbose: print('Hollowing out cells... ')
    b = nd.laplace(image)
    mask = b != 0
    m = image * mask
    if remove_background:
        mask = m != background
        m = m * mask
    if verbose: print('Done !!')
    return m


def return_list_of_vectors(tensor, by_row=True):
    """Return a list of vectors from a 3x3 array read by row or columns."""
    if isinstance(tensor, dict):
        return dict([(k, return_list_of_vectors(t, by_row)) for k, t in
                     tensor.items()])
    elif isinstance(tensor, list) and tensor[0].shape == (3, 3):
        return [return_list_of_vectors(t, by_row) for t in tensor]
    else:
        if by_row:
            return [tensor[v] for v in range(len(tensor))]
        else:
            return [tensor[:, v] for v in range(len(tensor))]


NPLIST, LIST, DICT = list(range(3))


class AbstractSpatialImageAnalysis(object):
    """
    This object is desinged to help the analysis of segmented tissues.
    It can extract 2D or 3D cellular features (volume, wall-area, ...) and the topology of a segmented tissue (SpatialImage).
    """

    def __init__(self, image, ignoredlabels=[], return_type=DICT,
                 background=None):
        """
        ..warning :: Label features in the images are an arithmetic progression of continous integers.
        By default, we save a property or information only if it can be used by several functions.

        Args:
           image: (SpatialImage) - basically a two- or tri-dimensional array presenting cell label position in space (segmented image);
           ignoredlabels: (int|list) - label or list of labels to ignore when computing cell features;
           return_type: (NPLIST|LIST|DICT) - define the type used to return computed features (DICT is safer!);
           background: (int) - image-label (int) refering to the background ;
        """
        # -- We make sure that `image` is of type 'SpatialImage':
        if isinstance(image, SpatialImage):
            self.image = image
        else:
            self.image = SpatialImage(image)

        # -- We use this to avoid (when possible) computation of cell-properties (background, cells in image margins, ...)
        if isinstance(ignoredlabels, int):
            ignoredlabels = [ignoredlabels]
        self._ignoredlabels = set(ignoredlabels)

        # -- Sounds a bit paranoiac but useful !!
        if background is not None:
            if not isinstance(background, int):
                raise ValueError(
                    "The label you provided as background is not an integer !")
            if background not in self.image:
                print(" WARNING!!! The background you provided has not been detected in the image !")
            self._ignoredlabels.update([background])
        else:
            warnings.warn(
                "No value defining the background, some functionalities won't work !")

        # -- Saving useful and shared informations:
        try:
            self._voxelsize = image.voxelsize  # voxelsize in real world units (float, float, float);
        except AttributeError:
            print("Could not detect voxelsize informations from the provided `image`!")
            self._voxelsize = np.ones(len(image.shape))
            print("Set by default to '{}'...".format(self._voxelsize))
        self._background = background  # image-label (int) refering to the background;
        # -- Creating masked attributes:
        self._labels = None  # list of image-labels refering to cell-labels;
        self._bbox = None  # list of boundingboxes (minimal cube containing a cell) sorted as self._labels;
        self._kernels = None  #
        self._neighbors = None  #
        self._cell_layer1 = None  #
        self._center_of_mass = {}  # voxel units

        # -- Saving normalized meta-informations:
        try:
            self.filepath, self.filename = split(image.info["Filename"])
        except:
            self.filepath, self.filename = None, None
        try:
            self.info = dict(
                [(k, v) for k, v in image.info.items() if k != "Filename"])
        except:
            pass

        self.return_type = return_type

    def is3D(self):
        return False

    def background(self):
        return self._background

    def ignoredlabels(self):
        return self._ignoredlabels

    def add2ignoredlabels(self, list2add, verbose=False):
        """
        Add labels to the ignoredlabels list (set) and update the self._labels cache.
        """
        if isinstance(list2add, int):
            list2add = [list2add]

        if verbose: print('Adding labels', list2add, 'to the list of labels to ignore...')
        self._ignoredlabels.update(list2add)
        if verbose: print('Updating labels list...')
        self._labels = self.__labels()

    def consideronlylabels(self, list2consider, verbose=False):
        """
        Add labels to the ignoredlabels list (set) and update the self._labels cache.
        """
        if isinstance(list2consider, int):
            list2consider = [list2consider]

        toignore = set(np.unique(self.image)) - set(list2consider)
        integers = np.vectorize(lambda x: int(x))
        toignore = integers(list(toignore)).tolist()

        if verbose: print('Adding labels', toignore, 'to the list of labels to ignore...')
        self._ignoredlabels.update(toignore)
        if verbose: print('Updating labels list...')
        self._labels = self.__labels()

    def convert_return(self, values, labels=None, overide_return_type=None):
        """
        This function convert outputs of analysis functions.
        """
        tmp_save_type = copy.copy(self.return_type)
        if not overide_return_type is None:
            self.return_type = overide_return_type
        # -- In case of unique label, just return the result for this label
        if not labels is None and isinstance(labels, int):
            self.return_type = copy.copy(tmp_save_type)
            return values
        # -- return a numpy array
        elif self.return_type == NPLIST:
            self.return_type = copy.copy(tmp_save_type)
            return values
        # -- return a standard python list
        elif self.return_type == LIST:
            if isinstance(values, list):
                return values
            else:
                self.return_type = copy.copy(tmp_save_type)
                return values.tolist()
        # -- return a dictionary
        else:
            self.return_type = copy.copy(tmp_save_type)
            return dict(list(zip(labels, values)))

    def labels(self):
        """
        Return the list of labels used.

        :Examples:

        >>> import numpy as np
        >>> a = np.array([[1, 2, 7, 7, 1, 1],
                          [1, 6, 5, 7, 3, 3],
                          [2, 2, 1, 7, 3, 3],
                          [1, 1, 1, 4, 1, 1]])

        >>> from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
        >>> analysis = SpatialImageAnalysis(a)

        >>> analysis.labels()
        [1,2,3,4,5,6,7]
        """
        if self._labels is None: self._labels = self.__labels()
        return self._labels

    def __labels(self):
        """
        Compute the actual list of labels.
        :IMPORTANT: `background` is not in the list of labels.
        """
        labels = set(np.unique(self.image)) - self._ignoredlabels
        return list(map(int, labels))

    def nb_labels(self):
        """
        Return the number of labels.

        :Examples:

        >>> import numpy as np
        >>> a = np.array([[1, 2, 7, 7, 1, 1],
                          [1, 6, 5, 7, 3, 3],
                          [2, 2, 1, 7, 3, 3],
                          [1, 1, 1, 4, 1, 1]])

        >>> from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
        >>> analysis = SpatialImageAnalysis(a)

        >>> analysis.nb_labels()
        7
        """
        if self._labels is None: self._labels = self.__labels()
        return len(self._labels)

    def label_request(self, labels):
        """
        The following lines are often needed to ensure the correct format of cell-labels, as well as their presence within the image.
        """
        if isinstance(labels, int):
            if not labels in self.labels():
                print("The following id was not found within the image labels: {}".format(
                    labels))
            labels = [labels]
        elif isinstance(labels, list):
            labels = list(set(labels) & set(self.labels()))
            not_in_labels = list(set(labels) - set(self.labels()))
            if not_in_labels != []:
                print("The following ids were not found within the image labels: {}".format(
                    not_in_labels))
        elif labels is None:
            labels = self.labels()
        elif isinstance(labels, str):
            if labels.lower() == 'all':
                labels = self.labels()
            elif labels.lower() == 'l1':
                labels = self.cell_first_layer()
            elif labels.lower() == 'l2':
                labels = self.cell_second_layer()
            else:
                pass
        else:
            raise ValueError(
                "This is not usable as `labels`: {}".format(labels))

        return labels

    def center_of_mass(self, labels=None, real=True, verbose=False):
        """
        Return the center of mass of the labels.

        Args:
           labels: (int) - single label number or a sequence of label numbers of the objects to be measured.
            If labels is None, all labels are used.
           real: (bool) - If True (default), center of mass is in real-world units else in voxels.

        :Examples:

        >>> import numpy as np
        >>> a = np.array([[1, 2, 7, 7, 1, 1],
                          [1, 6, 5, 7, 3, 3],
                          [2, 2, 1, 7, 3, 3],
                          [1, 1, 1, 4, 1, 1]])

        >>> from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
        >>> analysis = SpatialImageAnalysis(a)

        >>> analysis.center_of_mass(7)
        [0.75, 2.75, 0.0]

        >>> analysis.center_of_mass([7,2])
        [[0.75, 2.75, 0.0], [1.3333333333333333, 0.66666666666666663, 0.0]]

        >>> analysis.center_of_mass()
        [[1.8, 2.2999999999999998, 0.0],
         [1.3333333333333333, 0.66666666666666663, 0.0],
         [1.5, 4.5, 0.0],
         [3.0, 3.0, 0.0],
         [1.0, 2.0, 0.0],
         [1.0, 1.0, 0.0],
         [0.75, 2.75, 0.0]]
        """
        # Check the provided `labels`:
        labels = self.label_request(labels)

        if verbose: print("Computing cells center of mass:")
        center = {}
        N = len(labels)
        percent = 0
        for n, l in enumerate(labels):
            if verbose and (n * 100 / N >= percent): print("{}%...".format(
                percent)); percent += 5
            if verbose and (n + 1 == N): print("100%")
            if l in self._center_of_mass:
                center[l] = self._center_of_mass[l]
            else:
                try:
                    slices = self.boundingbox(l, real=False)
                    crop_im = self.image[slices]
                    c_o_m = np.array(
                        nd.center_of_mass(crop_im, crop_im, index=l))
                    c_o_m = [c_o_m[i] + sl.start for i, sl in
                             enumerate(slices)]
                except:
                    crop_im = self.image
                    c_o_m = np.array(
                        nd.center_of_mass(crop_im, crop_im, index=l))
                self._center_of_mass[l] = c_o_m
                center[l] = c_o_m

        if real:
            center = dict(
                [(l, np.multiply(center[l], self._voxelsize)) for l in labels])

        if len(labels) == 1:
            return center[labels[0]]
        else:
            return center

    def boundingbox(self, labels=None, real=False):
        """
        Return the bounding box of a label.

        :Examples:

        >>> import numpy as np
        >>> a = np.array([[1, 2, 7, 7, 1, 1],
                          [1, 6, 5, 7, 3, 3],
                          [2, 2, 1, 7, 3, 3],
                          [1, 1, 1, 4, 1, 1]])

        >>> from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
        >>> analysis = SpatialImageAnalysis(a)

        >>> analysis.boundingbox(7)
        (slice(0, 3), slice(2, 4), slice(0, 1))

        >>> analysis.boundingbox([7,2])
        [(slice(0, 3), slice(2, 4), slice(0, 1)), (slice(0, 3), slice(0, 2), slice(0, 1))]

        >>> analysis.boundingbox()
        [(slice(0, 4), slice(0, 6), slice(0, 1)),
        (slice(0, 3), slice(0, 2), slice(0, 1)),
        (slice(1, 3), slice(4, 6), slice(0, 1)),
        (slice(3, 4), slice(3, 4), slice(0, 1)),
        (slice(1, 2), slice(2, 3), slice(0, 1)),
        (slice(1, 2), slice(1, 2), slice(0, 1)),
        (slice(0, 3), slice(2, 4), slice(0, 1))]
        """
        if labels == 0:
            return nd.find_objects(self.image == 0)[0]

        if self._bbox is None:
            self._bbox = nd.find_objects(self.image)

        if labels is None:
            labels = copy.copy(self.labels())
            if self.background() is not None:
                labels.append(self.background())

        # bbox of object labelled 1 to n are stored into self._bbox. To access i-th element, we have to use i-1 index
        if isinstance(labels, list):
            bboxes = [self._bbox[i - 1] for i in labels]
            if real:
                return self.convert_return(
                    [real_indices(bbox, self._voxelsize) for bbox in bboxes],
                    labels)
            else:
                return self.convert_return(bboxes, labels)

        else:
            try:
                if real:
                    return real_indices(self._bbox[labels - 1], self._voxelsize)
                else:
                    return self._bbox[labels - 1]
            except:
                return None

    def neighbors(self, labels=None, min_contact_area=None, real_area=True,
                  verbose=True):
        """
        Return the list of neighbors of a label.

        :WARNING:
            If `min_contact_area` is given it should be in real world units.

        Args:
           labels: (None|int|list) - label or list of labels of which we want to return the neighbors. If none, neighbors for all labels found in self.image will be returned.
           min_contact_area: (None|int|float) - value of the min contact area threshold.
           real_area: (bool) - indicate wheter the min contact area is a real world value or a number of voxels.

        :Examples:

        >>> import numpy as np
        >>> a = np.array([[1, 2, 7, 7, 1, 1],
                          [1, 6, 5, 7, 3, 3],
                          [2, 2, 1, 7, 3, 3],
                          [1, 1, 1, 4, 1, 1]])

        >>> from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
        >>> analysis = SpatialImageAnalysis(a)

        >>> analysis.neighbors(7)
        [1, 2, 3, 4, 5]

        >>> analysis.neighbors([7,2])
        {7: [1, 2, 3, 4, 5], 2: [1, 6, 7] }

        >>> analysis.neighbors()
        {1: [2, 3, 4, 5, 6, 7],
         2: [1, 6, 7],
         3: [1, 7],
         4: [1, 7],
         5: [1, 6, 7],
         6: [1, 2, 5],
         7: [1, 2, 3, 4, 5] }
        """
        if (min_contact_area is not None) and verbose:
            if real_area:
                try:
                    print("Neighbors will be filtered according to a min contact area of %.2f \u03BCm\u00B2" % min_contact_area)
                except:
                    print("Neighbors will be filtered according to a min contact area of %.2f micro m 2" % min_contact_area)
            else:
                print("Neighbors will be filtered according to a min contact area of %d voxels" % min_contact_area)
        if labels is None:
            return self._all_neighbors(min_contact_area, real_area)
        elif not isinstance(labels, list):
            return self._neighbors_with_mask(labels, min_contact_area,
                                             real_area)
        else:
            return self._neighbors_from_list_with_mask(labels, min_contact_area,
                                                       real_area)

    def _neighbors_with_mask(self, label, min_contact_area=None,
                             real_area=True):
        if not self._neighbors is None and label in self._neighbors.keys():
            result = self._neighbors[label]
            if min_contact_area is None:
                return result
            else:
                return self._neighbors_filtering_by_contact_area(label, result,
                                                                 min_contact_area,
                                                                 real_area)

        try:
            slices = self.boundingbox(label)
            ex_slices = dilation(slices)
            mask_img = self.image[ex_slices]
        except:
            mask_img = self.image
        neigh = list(contact_surface(mask_img, label))
        if min_contact_area is not None:
            neigh = self._neighbors_filtering_by_contact_area(label, neigh,
                                                              min_contact_area,
                                                              real_area)

        return neigh

    def _neighbors_from_list_with_mask(self, labels, min_contact_area=None,
                                       real_area=True):
        if (not self._neighbors is None) and (
                sum([i in self._neighbors.keys() for i in labels]) == len(
                labels)):
            result = dict([(i, self._neighbors[i]) for i in labels])
            if min_contact_area is None:
                return result
            else:
                return self._filter_with_area(result, min_contact_area,
                                              real_area)

        edges = {}
        for label in labels:
            try:
                slices = self.boundingbox(label)
                ex_slices = dilation(slices)
                mask_img = self.image[ex_slices]
            except:
                mask_img = self.image
            neigh = list(contact_surface(mask_img, label))
            if min_contact_area is not None:
                neigh = self._neighbors_filtering_by_contact_area(label, neigh,
                                                                  min_contact_area,
                                                                  real_area)
            edges[label] = neigh

        return edges

    def _all_neighbors(self, min_contact_area=None, real_area=True):
        if not self._neighbors is None:
            result = self._neighbors
            if min_contact_area is None:
                return result
            else:
                return self._filter_with_area(result, min_contact_area,
                                              real_area)

        edges = {}  # store src, target
        slice_label = self.boundingbox()
        if self.return_type == 0 or self.return_type == 1:
            slice_label = dict(
                (label + 1, slices) for label, slices in enumerate(slice_label))
            # label_id = label +1 because the label_id begin at 1
            # and the enumerate begin at 0.
        for label_id, slices in slice_label.items():
            # sometimes, the label doesn't exist and 'slices' is None, hence we do a try/except:
            try:
                ex_slices = dilation(slices)
                mask_img = self.image[tuple(ex_slices)]
            except:
                mask_img = self.image
            neigh = list(contact_surface(mask_img, label_id))
            edges[label_id] = neigh

        self._neighbors = edges
        if min_contact_area is None:
            return edges
        else:
            return self._filter_with_area(edges, min_contact_area, real_area)

    def _filter_with_area(self, neighborhood_dictionary, min_contact_area,
                          real_area):
        """
        Function filtering a neighborhood dictionary according to a minimal contact area between two neigbhors.

        Args:
           neighborhood_dictionary: (dict) - dictionary of neighborhood to be filtered.
           min_contact_area: (None|int|float) - value of the min contact area threshold.
           real_area: (bool) - indicate wheter the min contact area is a real world value or a number of voxels.
        """
        filtered_dict = {}
        for label in neighborhood_dictionary.keys():
            filtered_dict[label] = self._neighbors_filtering_by_contact_area(
                label, neighborhood_dictionary[label], min_contact_area,
                real_area)

        return filtered_dict

    def _neighbors_filtering_by_contact_area(self, label, neighbors,
                                             min_contact_area, real_area):
        """
        Function used to filter the returned neighbors according to a given minimal contact area between them!

        Args:
           label: (int) - label of the image to threshold by the min contact area.
           neighbors` (list) - list of neighbors of the `label: to be filtered.
           min_contact_area: (None|int|float) - value of the min contact area threshold.
           real_area: (bool) - indicate wheter the min contact area is a real world value or a number of voxels.
        """
        areas = self.cell_wall_area(label, neighbors, real_area)
        nei = cp.copy(neighbors)
        for i, j in areas.keys():
            if areas[(i, j)] < min_contact_area:
                nei.remove(i if j == label else j)

        return nei

    def neighbor_kernels(self):
        if self._kernels is None:
            if self.is3D():
                X1kernel = np.zeros((3, 3, 3), np.bool)
                X1kernel[:, 1, 1] = True
                X1kernel[0, 1, 1] = False
                X2kernel = np.zeros((3, 3, 3), np.bool)
                X2kernel[:, 1, 1] = True
                X2kernel[2, 1, 1] = False
                Y1kernel = np.zeros((3, 3, 3), np.bool)
                Y1kernel[1, :, 1] = True
                Y1kernel[1, 0, 1] = False
                Y2kernel = np.zeros((3, 3, 3), np.bool)
                Y2kernel[1, :, 1] = True
                Y2kernel[1, 2, 1] = False
                Z1kernel = np.zeros((3, 3, 3), np.bool)
                Z1kernel[1, 1, :] = True
                Z1kernel[1, 1, 0] = False
                Z2kernel = np.zeros((3, 3, 3), np.bool)
                Z2kernel[1, 1, :] = True
                Z2kernel[1, 1, 2] = False
                self._kernels = (
                X1kernel, X2kernel, Y1kernel, Y2kernel, Z1kernel, Z2kernel)
            else:
                X1kernel = np.zeros((3, 3), np.bool)
                X1kernel[:, 1] = True
                X1kernel[0, 1] = False
                X2kernel = np.zeros((3, 3), np.bool)
                X2kernel[:, 1] = True
                X2kernel[2, 1] = False
                Y1kernel = np.zeros((3, 3), np.bool)
                Y1kernel[1, :] = True
                Y1kernel[1, 0] = False
                Y2kernel = np.zeros((3, 3), np.bool)
                Y2kernel[1, :] = True
                Y2kernel[1, 2] = False
                self._kernels = (X1kernel, X2kernel, Y1kernel, Y2kernel)

        return self._kernels

    def neighbors_number(self, labels=None, min_contact_area=None,
                         real_area=True, verbose=True):
        """
        Return the number of neigbors of each label.
        """
        nei = self.neighbors(labels, min_contact_area, real_area, verbose)
        if isinstance(nei, dict):
            return dict([(k, len(v)) for k, v in nei.items()])
        else:
            return len(nei)

    def get_all_wall_binary_image(self):
        """
        Returns a binary image made of the walls positions only.
        """
        lp = nd.laplace(self.image)
        return lp / lp

    def get_voxel_face_surface(self):
        a = self._voxelsize
        if len(a) == 3:
            return np.array([a[1] * a[2], a[2] * a[0], a[0] * a[1]])
        if len(a) == 2:
            return np.array([a[0], a[1]])

    def wall_voxels_between_two_cells(self, label_1, label_2, bbox=None,
                                      verbose=False):
        """
        Return the voxels coordinates defining the contact wall between two labels.

        Args:
           image: (ndarray of ints) - Array containing objects defined by labels
           label_1: (int) - object id #1
           label_2: (int) - object id #2
           bbox: (dict, optional) - If given, contain a dict of slices

        Returns:
         - xyz 3xN array.
        """

        if bbox is not None:
            if isinstance(bbox, dict):
                label_1, label_2 = sort_boundingbox(bbox, label_1, label_2)
                boundingbox = bbox[label_1]
            elif isinstance(bbox, tuple) and len(bbox) == 3:
                boundingbox = bbox
            else:
                try:
                    boundingbox = find_smallest_boundingbox(self.image, label_1,
                                                            label_2)
                except:
                    print("Could neither use the provided value of `bbox`, nor gess it!")
                    boundingbox = tuple(
                        [(0, s - 1, None) for s in self.image.shape])
            dilated_bbox = dilation(boundingbox)
            dilated_bbox_img = self.image[dilated_bbox]
        else:
            try:
                boundingbox = find_smallest_boundingbox(self.image, label_1,
                                                        label_2)
            except:
                dilated_bbox_img = self.image

        mask_img_1 = (dilated_bbox_img == label_1)
        mask_img_2 = (dilated_bbox_img == label_2)

        struct = nd.generate_binary_structure(3, 2)
        dil_1 = nd.binary_dilation(mask_img_1, structure=struct)
        dil_2 = nd.binary_dilation(mask_img_2, structure=struct)
        x, y, z = np.where(((dil_1 & mask_img_2) | (dil_2 & mask_img_1)) == 1)

        if bbox is not None:
            return np.array((x + dilated_bbox[0].start,
                             y + dilated_bbox[1].start,
                             z + dilated_bbox[2].start))
        else:
            return np.array((x, y, z))

    def wall_voxels_per_cell(self, label_1, bbox=None, neighbors=None,
                             neighbors2ignore=[], verbose=False):
        """
        Return the voxels coordinates of all walls from one cell.
        There must be a contact defined between two labels, the given one and its neighbors.
        If no 'neighbors' list is provided, we first detect all neighbors of 'label_1'.

        Args:
           image: (ndarray of ints) - Array containing objects defined by labels
           label_1: (int): cell id #1.
           bbox: (dict, optional) - dictionary of slices defining bounding box for each labelled object.
           neighbors` (list, optional) - list of neighbors for the object `label_1:.
           neighbors2ignore` (list, optional) - labels of neighbors to ignore while considering separation between the object `label_1: and its neighbors. All ignored labels will be returned as 0.

        Returns:
           coord: (dict): *keys= [min(labels_1,neighbors[n]), max(labels_1,neighbors[n])]; *values= xyz 3xN array.
        """
        # -- We use the bounding box to work faster (on a smaller image)
        if isinstance(bbox, dict):
            boundingbox = bbox(label_1)
        elif (isinstance(bbox, tuple) or isinstance(bbox, list)) and isinstance(
                bbox[0], slice):
            boundingbox = bbox
        elif bbox is None:
            boundingbox = self.boundingbox(label_1)
        dilated_bbox = dilation(dilation(boundingbox))
        dilated_bbox_img = self.image[dilated_bbox]

        # -- Binary mask saying where the label_1 can be found on the image.
        mask_img_1 = (dilated_bbox_img == label_1)
        struct = nd.generate_binary_structure(3, 2)
        dil_1 = nd.binary_dilation(mask_img_1, structure=struct)

        # -- We edit the neighbors list as required:
        if neighbors is None:
            neighbors = self.neighbors(label_1)
        if isinstance(neighbors, int):
            neighbors = [neighbors]
        if isinstance(neighbors, dict) and len(neighbors) != 1:
            neighborhood = neighbors
            neighbors = copy.copy(neighborhood[label_1])
        if neighbors2ignore != []:
            for nei in neighbors2ignore:
                try:
                    neighbors.remove(nei)
                except:
                    pass

        coord = {}
        neighbors_not_found = []
        for label_2 in neighbors:
            # -- Binary mask saying where the label_2 can be found on the image.
            mask_img_2 = (dilated_bbox_img == label_2)
            dil_2 = nd.binary_dilation(mask_img_2, structure=struct)
            # -- We now intersect the two dilated binary mask to find the voxels defining the contact area between two objects:
            x, y, z = np.where(
                ((dil_1 & mask_img_2) | (dil_2 & mask_img_1)) == 1)
            if x != []:
                if label_2 not in neighbors2ignore:
                    coord[min(label_1, label_2), max(label_1,
                                                     label_2)] = np.array((x +
                                                                           dilated_bbox[
                                                                               0].start,
                                                                           y +
                                                                           dilated_bbox[
                                                                               1].start,
                                                                           z +
                                                                           dilated_bbox[
                                                                               2].start))
            else:
                if verbose:
                    print("Couldn't find a contact between neighbor cells {} and {}".format(
                        label_1, label_2))
                neighbors_not_found.append(label_2)

        if neighbors_not_found:
            print("Some walls have not been found comparing to the `neighbors` list of {}: {}".format(
                label_1, neighbors_not_found))

        return coord

    def cells_walls_coords(self):
        """Return coordinates of the voxels defining a cell wall.

        This function thus returns any voxel in contact with one of different label.

        Args:
          image (SpatialImage) - Segmented image (tissu)

        Returns:
          x,y,z (list) - coordinates of the voxels defining the cell boundaries (walls).
        """
        if self.is3D():
            image = hollow_out_cells(self.image, self.background, verbose=True)
        else:
            image = copy.copy(self.image)
            image[np.where(image == self.background)] = 0

        if self.is3D():
            x, y, z = np.where(image != 0)
            return list(x), list(y), list(z)
        else:
            x, y = np.where(image != 0)
            return list(x), list(y)

    def cell_wall_area(self, label_id, neighbors, real=True):
        """
        Return the area of contact between a label and its neighbors.
        A list or a unique id can be given as neighbors.

        :Examples:

        >>> import numpy as np
        >>> a = np.array([[1, 2, 7, 7, 1, 1],
                          [1, 6, 5, 7, 3, 3],
                          [2, 2, 1, 7, 3, 3],
                          [1, 1, 1, 4, 1, 1]])

        >>> from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
        >>> analysis = SpatialImageAnalysis(a)

        >>> analysis.cell_wall_area(7, 2)
        1.0
        >>> analysis.cell_wall_area(7, [2,5])
        {(2, 7): 1.0, (5, 7): 2.0}
        """

        resolution = self.get_voxel_face_surface()
        try:
            dilated_bbox = dilation(self.boundingbox(label_id))
            dilated_bbox_img = self.image[tuple(dilated_bbox)]
        except:
            # ~ dilated_bbox = tuple( [slice(0,self.image.shape[i]-1) for i in xrange(len(self.image.shape))] ) #if no slice can be found we use the whole image
            dilated_bbox_img = self.image

        mask_img = (dilated_bbox_img == label_id).get_array().astype(int)

        xyz_kernels = self.neighbor_kernels()

        unique_neighbor = not isinstance(neighbors, list)
        if unique_neighbor:
            neighbors = [neighbors]

        wall = {}
        for a in range(len(xyz_kernels)):
            dil = nd.binary_dilation(mask_img, structure=xyz_kernels[a])
            frontier = dilated_bbox_img[(np.asarray(dil).astype(int) - np.asarray(mask_img).astype(int)).astype(bool)]

            for n in neighbors:
                nb_pix = len(frontier[frontier == n])
                if real:
                    area = float(nb_pix * resolution[a // 2])
                else:
                    area = nb_pix
                i, j = min(label_id, n), max(label_id, n)
                wall[(i, j)] = wall.get((i, j), 0.0) + area

        if unique_neighbor:
            return next(wall.itervalues())
        else:
            return wall

    def wall_areas(self, neighbors=None, real=True):
        """
        Return the area of contact between all neighbor labels.
        If neighbors is not given, it is computed first.

        :Examples:

        >>> import numpy as np
        >>> a = np.array([[1, 2, 7, 7, 1, 1],
                          [1, 6, 5, 7, 3, 3],
                          [2, 2, 1, 7, 3, 3],
                          [1, 1, 1, 4, 1, 1]])

        >>> from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
        >>> analysis = SpatialImageAnalysis(a)

        >>> analysis.wall_areas({ 1 : [2, 3], 2 : [6] })
       {(1, 2): 5.0, (1, 3): 4.0, (2, 6): 2.0 }

        >>> analysis.wall_areas()
        {(1, 2): 5.0, (1, 3): 4.0, (1, 4): 2.0, (1, 5): 1.0, (1, 6): 1.0, (1, 7): 2.0, (2, 6): 2.0, (2, 7): 1.0, (3, 7): 2, (4, 7): 1, (5, 6): 1.0, (5, 7): 2.0 }
        """
        if neighbors is None: neighbors = self.neighbors()
        areas = {}
        for label_id, lneighbors in neighbors.items():
            # To avoid computing twice the same wall area, we select walls between i and j with j > i.
            neigh = [n for n in lneighbors if n > label_id]
            if len(neigh) > 0:
                lareas = self.cell_wall_area(label_id, neigh, real=real)
                for i, j in lareas.keys():
                    areas[(i, j)] = areas.get((i, j), 0.0) + lareas[(i, j)]
        return areas

    def cell_first_layer(self, filter_by_area=True, minimal_external_area=10,
                         real_area=True):
        """
        Extract a list of labels corresponding to the external layer of cells.
        """
        integers = lambda l: list(map(int, l))
        if self._cell_layer1 is None:  # _cell_layer1 contains always all the l1-cell labels.
            self._cell_layer1 = integers(self.neighbors(self.background()))

        cell_layer1 = self._cell_layer1
        if filter_by_area:
            print()
            labels_area = self.cell_wall_area(self.background(),
                                              self._cell_layer1, real_area)
            cell_layer1 = [label for label in self._cell_layer1 if ((
                                                                        tuple(
                                                                                [
                                                                                    self.background(),
                                                                                    label]) in labels_area) and (
                                                                                labels_area[
                                                                                    (
                                                                                    self.background(),
                                                                                    label)] > minimal_external_area))]

        return list(set(cell_layer1) - self._ignoredlabels)

    def cell_second_layer(self, filter_by_area=True, minimal_L1_area=10,
                          real_area=True):
        """
        Extract a list of labels corresponding to the second layer of cells.
        """
        L1_neighbors = self.neighbors(self.cell_first_layer(), minimal_L1_area,
                                      real_area, True)
        l2 = set([])
        for nei in L1_neighbors.values():
            l2.update(nei)

        self._cell_layer2 = list(
            l2 - set(self._cell_layer1) - self._ignoredlabels)
        return self._cell_layer2

    def __voxel_first_layer(self, keep_background=True):
        """
        Extract the first layer of voxels at the surface of the biological object.
        """
        print("Extracting the first layer of voxels...")
        mask_img_1 = (self.image == self.background())
        struct = nd.generate_binary_structure(3, 1)
        dil_1 = nd.binary_dilation(mask_img_1, structure=struct)

        layer = dil_1 - mask_img_1

        if keep_background:
            return self.image * layer + mask_img_1
        else:
            return self.image * layer

    def voxel_first_layer(self, keep_background=True):
        """
        Function extracting the first layer of voxels in contact with the background.
        """
        if self._voxel_layer1 is None:
            self._voxel_layer1 = self.__voxel_first_layer(keep_background)
        return self._voxel_layer1

    def wall_voxels_per_cells_pairs(self, labels=None, neighborhood=None,
                                    only_epidermis=False,
                                    ignore_background=False,
                                    min_contact_area=None, real_area=True,
                                    verbose=True):
        """
        Extract the coordinates of voxels defining the 'wall' between a pair of labels.
        :WARNING: if dimensionality = 2, only the cells belonging to the outer layer of the object will be used.

        Args:
           labels: (int|list) - label or list of labels to extract walls coordinate with its neighbors.
           neighborhood: (list|dict) - list of neighbors of label if isinstance(labels,int), if not neighborhood should be a dictionary of neighbors by labels.
           only_epidermis: (bool) - indicate if we work with the whole image or just the first layer of voxels (epidermis).
           ignore_background: (bool) - indicate whether we want to return the coordinate of the voxels defining the 'epidermis wall' (in contact with self.background()) or not.
           min_contact_area: (None|int|float) - value of the min contact area threshold.
           real_area: (bool) - indicate wheter the min contact surface is a real world value or a number of voxels.
        """
        if only_epidermis:
            image = self.voxel_first_layer(True)
        else:
            image = self.image

        compute_neighborhood = False
        if neighborhood is None:
            compute_neighborhood = True
        if isinstance(labels, list) and isinstance(neighborhood, dict):
            labels = [label for label in labels if label in neighborhood]

        if labels is None and not only_epidermis:
            labels = self.labels()
        elif labels is None and only_epidermis:
            labels = np.unique(image)
        elif isinstance(labels, list):
            labels.sort()
            if not isinstance(neighborhood, dict):
                compute_neighborhood = True
        elif isinstance(labels, int):
            labels = [labels]
        else:
            raise ValueError("Couldn't find any labels.")

        dict_wall_voxels = {}
        N = len(labels)
        percent = 0
        for n, label in enumerate(labels):
            if verbose and n * 100 / float(N) >= percent: print("{}%...".format(
                percent)); percent += 10
            if verbose and n + 1 == N: print("100%")
            # - We compute or use the neighborhood of `label`:
            if compute_neighborhood:
                neighbors = self.neighbors(label, min_contact_area, real_area)
            else:
                if isinstance(neighborhood, dict):
                    neighbors = copy.copy(neighborhood[label])
                if isinstance(neighborhood, list):
                    neighbors = neighborhood
            # - We create a list of neighbors to ignore:
            if ignore_background:
                neighbors2ignore = [n for n in neighbors if n not in labels]
            else:
                neighbors2ignore = [n for n in neighbors if
                                    n not in labels + [self.background()]]
            # - We remove the couples of labels from which the "wall voxels" are already extracted:
            for nei in neighbors:
                if (min(label, nei), max(label, nei)) in dict_wall_voxels:
                    neighbors.remove(nei)
            # - If there are neighbors left in the list, we extract the "wall voxels" between them and `label`:
            if neighbors != []:
                dict_wall_voxels.update(
                    self.wall_voxels_per_cell(label, self.boundingbox(label),
                                              neighbors, neighbors2ignore,
                                              verbose=False))

        return dict_wall_voxels

    def fuse_labels_in_image(self, labels, verbose=True):
        """ Modify the image so the given labels are fused (to the min value)."""
        assert isinstance(labels, list) and len(labels) >= 2
        assert self.background() not in labels

        min_lab = min(labels)
        labels.remove(min_lab)
        N = len(labels)
        percent = 0
        if verbose: print("Fusing the following {} labels: {} to value '{}'.".format(
            N, labels, min_lab))
        for n, label in enumerate(labels):
            if verbose and n * 100 / float(N) >= percent: print("{}%...".format(
                percent)); percent += 5
            if verbose and n + 1 == N: print("100%")
            try:
                bbox = self.boundingbox(label)
                xyz = np.where((self.image[bbox]) == label)
                self.image[tuple((
                                 xyz[0] + bbox[0].start, xyz[1] + bbox[1].start,
                                 xyz[2] + bbox[2].start))] = min_lab
            except:
                print("No boundingbox found for cell id #{}, skipping...".format(
                    label))
                continue
        print("Done!")
        return None

    def remove_labels_from_image(self, labels, erase_value=0, verbose=True):
        """
        Use remove_cell to iterate over a list of cell to remove if there is more cells to keep than to remove.
        If there is more cells to remove than to keep, we fill a "blank" image with those to keep.
        :!!!!WARNING!!!!:
        This function modify the SpatialImage 'self.image' !
        :!!!!WARNING!!!!:
        """
        # - Make sure 'labels' is a list:
        if isinstance(labels, int): labels = [labels]
        # - Make sure the background is not in the list of labels to remove!
        try:
            labels.remove(self.background())
        except:
            pass
        # - Now we can safely remove 'labels' using boundingboxes to speed-up computation and save memory:
        N = len(labels)
        percent = 0
        if verbose: print("Removing", N, "cell-labels.")
        for n, label in enumerate(labels):
            if verbose and n * 100 / float(N) >= percent: print("{}%...".format(
                percent)); percent += 5
            if verbose and n + 1 == N: print("100%")
            try:
                xyz = np.where((self.image[self.boundingbox(label)]) == label)
                self.image[tuple((xyz[0] + self.boundingbox(label)[0].start,
                                  xyz[1] + self.boundingbox(label)[1].start,
                                  xyz[2] + self.boundingbox(label)[
                                      2].start))] = erase_value
            except:
                print("No boundingbox found for cell id #{}, skipping...".format(
                    label))
                continue
        # - We now update the 'self._ignoredlabels' labels list:
        self._ignoredlabels.update([erase_value])
        [self._ignoredlabels.discard(label) for label in labels]

        if verbose: print('Done !!')

    def remove_stack_margin_labels_from_image(self, erase_value=0,
                                              voxel_distance_from_margin=5,
                                              verbose=True):
        """
        :!!!!WARNING!!!!:
        This function modify the SpatialImage 'self.image' !
        :!!!!WARNING!!!!:
        Function removing cells at the margins, because most probably partially acquired.
        """
        if verbose: print("Deleting cells at the margins of the stack from 'self.image'...")
        self.remove_labels_from_image(
            self.labels_at_stack_margins(voxel_distance_from_margin),
            erase_value, verbose)


class SpatialImageAnalysis3D(AbstractSpatialImageAnalysis):
    """
    Class dedicated to 3D objects.
    """

    def __init__(self, image, ignoredlabels=[], return_type=DICT,
                 background=None):
        AbstractSpatialImageAnalysis.__init__(self, image, ignoredlabels,
                                              return_type, background)
        self._voxel_layer1 = None
        self.principal_curvatures = {}
        self.principal_curvatures_normal = {}
        self.principal_curvatures_directions = {}
        self.principal_curvatures_origin = {}
        self.curvatures_tensor = {}
        self.external_wall_geometric_median = {}
        self.epidermis_wall_median_voxel = {}

    def is3D(self):
        return True

    def volume(self, labels=None, real=True):
        """
        Return the volume of the labels.

        Args:
           labels: (int) - single label number or a sequence of
            label numbers of the objects to be measured.
            If labels is None, all labels are used.

           real: (bool) - If real = True, volume is in real-world units else in voxels.

        :Examples:

        >>> import numpy as np
        >>> a = np.array([[1, 2, 7, 7, 1, 1],
                          [1, 6, 5, 7, 3, 3],
                          [2, 2, 1, 7, 3, 3],
                          [1, 1, 1, 4, 1, 1]])

        >>> from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
        >>> analysis = SpatialImageAnalysis(a)

        >>> analysis.volume(7)
        4.0

        >>> analysis.volume([7,2])
        [4.0, 3.0]

        >>> analysis.volume()
        [10.0, 3.0, 4.0, 1.0, 1.0, 1.0, 4.0]
        """
        # Check the provided `labels`:
        labels = self.label_request(labels)

        volume = nd.sum(np.ones_like(self.image), self.image,
                        index=np.int16(labels))
        # convert to real-world units if asked:
        if real is True:
            if self.image.ndim == 2:
                volume = np.multiply(volume,
                                     (self._voxelsize[0] * self._voxelsize[1]))
            elif self.image.ndim == 3:
                volume = np.multiply(volume, (
                            self._voxelsize[0] * self._voxelsize[1] *
                            self._voxelsize[2]))
            volume.tolist()

        if not isinstance(labels, int):
            return self.convert_return(volume, labels)
        else:
            return volume

    def inertia_axis(self, labels=None, real=True, verbose=False):
        """
        Return the inertia axis of cells, also called the shape main axis.
        Return 3 (3D-oriented) vectors by rows and 3 (length) values.
        """
        # Check the provided `labels`:
        labels = self.label_request(labels)

        # results
        inertia_eig_vec = []
        inertia_eig_val = []
        N = len(labels)
        percent = 0
        for i, label in enumerate(labels):
            if verbose and i * 100 / float(N) >= percent: print("{}%...".format(
                percent)); percent += 10
            if verbose and i + 1 == N: print("100%")
            slices = self.boundingbox(label, real=False)
            center = copy.copy(self.center_of_mass(label, real=False))
            # project center into the slices sub_image coordinate
            if slices is not None:
                for s, sl in enumerate(slices):
                    center[s] = center[s] - sl.start
                label_image = (self.image[slices] == label)
            else:
                print('No boundingbox found for label {}'.format(label))
                label_image = (self.image == label)

            # compute the indices of voxel with adequate label
            xyz = label_image.nonzero()
            if len(xyz) == 0:
                continue  # obviously no reasons to go further !
            coord = coordinates_centering3D(xyz, center)
            # compute the variance-covariance matrix (1/N*P.P^T):
            cov = compute_covariance_matrix(coord)
            # Find the eigen values and vectors.
            eig_val, eig_vec = eigen_values_vectors(cov)
            # convert to real-world units if asked:
            if real:
                for n in range(3):
                    eig_val[n] *= np.linalg.norm(
                        np.multiply(eig_vec[n], self._voxelsize))

            inertia_eig_vec.append(eig_vec)
            inertia_eig_val.append(eig_val)

        if len(labels) == 1:
            return return_list_of_vectors(inertia_eig_vec[0]), inertia_eig_val[
                0]
        else:
            return self.convert_return(return_list_of_vectors(inertia_eig_vec),
                                       labels), self.convert_return(
                inertia_eig_val, labels)

    def reduced_inertia_axis(self, labels=None, real=True, verbose=False):
        """
        Return the REDUCED (centered coordinates standardized) inertia axis of cells, also called the shape main axis.
        Return 3 (3D-oriented) vectors by rows and 3 (length) values.
        """
        # Check the provided `labels`:
        labels = self.label_request(labels)

        # results
        inertia_eig_vec = []
        inertia_eig_val = []
        N = len(labels)
        percent = 0
        for i, label in enumerate(labels):
            if verbose and i * 100 / float(N) >= percent: print("{}%...".format(
                percent)); percent += 10
            if verbose and i + 1 == N: print("100%")
            slices = self.boundingbox(label, real=False)
            center = copy.copy(self.center_of_mass(label, real=False))
            # project center into the slices sub_image coordinate
            if slices is not None:
                for s, sl in enumerate(slices):
                    center[s] = center[s] - sl.start
                label_image = (self.image[slices] == label)
            else:
                print('No boundingbox found for label {}'.format(label))
                label_image = (self.image == label)

            # compute the indices of voxel with adequate label
            xyz = label_image.nonzero()
            if len(xyz) == 0:
                continue  # obviously no reasons to go further !
            coord = coordinates_centering3D(xyz, center)
            # compute the variance-covariance matrix (1/N*P.P^T):
            cov = compute_covariance_matrix(coord)
            # Find the eigen values and vectors.
            eig_val, eig_vec = eigen_values_vectors(cov)
            # convert to real-world units if asked:
            if real:
                for n in range(3):
                    eig_val[n] *= np.linalg.norm(
                        np.multiply(eig_vec[n], self._voxelsize))

            inertia_eig_vec.append(eig_vec)
            inertia_eig_val.append(eig_val)

        if len(labels) == 1:
            return return_list_of_vectors(inertia_eig_vec[0], by_row=1), \
                   inertia_eig_val[0]
        else:
            return self.convert_return(
                return_list_of_vectors(inertia_eig_vec, by_row=1),
                labels), self.convert_return(inertia_eig_val, labels)

    def labels_at_stack_margins(self, voxel_distance_from_margin=5):
        """
        Return a list of cells in contact with the margins of the stack (SpatialImage).
        All ids within a defined (5 by default) voxel distance form the margins will be used to define cells as 'in image margins'.
        """
        vx_dist = voxel_distance_from_margin
        margins = []
        margins.extend(np.unique(self.image[:vx_dist, :, :]))
        margins.extend(np.unique(self.image[-vx_dist:, :, :]))
        margins.extend(np.unique(self.image[:, :vx_dist, :]))
        margins.extend(np.unique(self.image[:, -vx_dist:, :]))
        margins.extend(np.unique(self.image[:, :, :vx_dist]))
        margins.extend(np.unique(self.image[:, :, -vx_dist:]))

        return list(set(margins) - {self._background})

    def region_boundingbox(self, labels):
        """
        This function return a boundingbox of a region including all cells (provided by `labels`).

        Args:
           labels: (list): list of cells ids;
        Returns:
        # - [x_start,y_start,z_start,x_stop,y_stop,z_stop] # 09.12.15: changed to the next line to match boundingbox order (as returned by scipy.ndimage.find_object !!
         - (slice(x_start,x_stop), slice(y_start,y_stop), slice(z_start,z_stop))
        """
        if isinstance(labels, list) and len(labels) == 1:
            return self.boundingbox(labels[0])
        if isinstance(labels, int):
            return self.boundingbox(labels)

        dict_slices = self.boundingbox(labels)
        # -- We start by making sure that all cells have an entry (key) in `dict_slices`:
        not_found = []
        for c in labels:
            if c not in dict_slices.keys():
                not_found.append(c)
        if len(not_found) != 0:
            warnings.warn(
                'You have asked for unknown cells labels: ' + " ".join(
                    [str(k) for k in not_found]))

        # -- We now define a slice for the region including all cells:
        x_start, y_start, z_start, x_stop, y_stop, z_stop = np.inf, np.inf, np.inf, 0, 0, 0
        for c in labels:
            x, y, z = dict_slices[c]
            x_start = min(x.start, x_start)
            y_start = min(y.start, y_start)
            z_start = min(z.start, z_start)
            x_stop = max(x.stop, x_stop)
            y_stop = max(y.stop, y_stop)
            z_stop = max(z.stop, z_stop)

        return (
        slice(x_start, x_stop), slice(y_start, y_stop), slice(z_start, z_stop))

    def cells_voxel_layer(self, labels, region_boundingbox=False,
                          single_frame=False):
        """
        This function extract the first layer of voxel surrounding a cell defined by `label`
        Args:
           label: (int|list) - cell-label for which we want to extract the first layer of voxel;
           region_boundingbox: (bool) - if True, consider a boundingbox surrounding all labels, instead of each label alone.
           single_frame: (bool) - if True, return only one array with all voxels position defining cell walls.
        :Output:
         returns a binary image: 1 where the cell-label of interest is, 0 elsewhere
        """
        if isinstance(labels, int):
            labels = [labels]
        if single_frame:
            region_boundingbox = True

        if not isinstance(region_boundingbox, bool):
            if sum([isinstance(s, slice) for s in region_boundingbox]) == 3:
                bbox = region_boundingbox
            else:
                print("TypeError: Wong type for 'region_boundingbox', should either be bool or la tuple of slices")
                return None
        elif isinstance(region_boundingbox, bool) and region_boundingbox:
            bbox = self.region_boundingbox(labels)
        else:
            bboxes = self.boundingbox(labels, real=False)

        # Generate the smaller eroding structure possible:
        struct = nd.generate_binary_structure(3, 2)
        if single_frame:
            vox_layer = np.zeros_like(self.image[bbox], dtype=int)
        else:
            vox_layer = {}
        for clabel in labels:
            if region_boundingbox:
                bbox_im = self.image[bbox]
            else:
                bbox_im = self.image[bboxes[clabel]]
            # Creating a mask (1 where the cell-label of interest is, 0 elsewhere):
            mask_bbox_im = (bbox_im == clabel)
            # Erode the cell using the structure:
            eroded_mask_bbox_im = nd.binary_erosion(mask_bbox_im,
                                                    structure=struct)
            if single_frame:
                vox_layer += np.array(mask_bbox_im - eroded_mask_bbox_im,
                                      dtype=int)
            else:
                vox_layer[clabel] = np.array(mask_bbox_im - eroded_mask_bbox_im,
                                             dtype=int)

        if len(labels) == 1:
            return vox_layer[clabel]
        else:
            return vox_layer

class SpatialImageAnalysis2D(AbstractSpatialImageAnalysis):
    """
    Class dedicated to 2D objects.
    """

    def __init__(self, image, ignoredlabels=None, return_type=DICT,
                 background=None, no_label_id=0):
        AbstractSpatialImageAnalysis.__init__(self, image, ignoredlabels,
                                              return_type, background,
                                              no_label_id)

    def is2D(self):
        return True

    def labels_at_stack_margins(self, voxel_distance_from_margin=1):
        """
        Return a list of cells in contact with the margins of the stack (LabelledImage).
        All ids within a defined (1 by default) voxel_distance_from_margin will be considered.
        """
        vx_dist = voxel_distance_from_margin
        margins = []
        margins.extend(np.unique(self.image[0:vx_dist, :]))
        margins.extend(np.unique(self.image[-vx_dist:, :]))
        margins.extend(np.unique(self.image[:, 0:vx_dist]))
        margins.extend(np.unique(self.image[:, -vx_dist:]))

        return list(set(margins) - {self.image.background})

    def inertia_axis(self, labels=None, center_of_mass=None, real=True,
                     verbose=True):
        """
        Return the inertia axis of cells, also called the shape main axis.
        Returns 2 (2D-oriented) vectors and 2 (length) values.
        """
        # Check the provided labels:
        labels = self.labels_checker(labels)
        vxs = np.array(self._voxelsize).reshape([3, 1])

        # results
        inertia_eig_vec = []
        inertia_eig_val = []
        nb_labels = len(labels)
        progress = 0
        for i, label in enumerate(labels):
            if verbose:
                progress = percent_progress(progress, n, nb_labels)
            slices = self.boundingbox(label, real=False)
            center = copy.copy(self.center_of_mass(label, real=False))
            # project center into the slices sub_image coordinate
            if slices is not None:
                for n, s in enumerate(slices):
                    center[n] = center[n] - s.start
                label_image = (self.image[slices] == label)
            else:
                print('No boundingbox found for label {}'.format(label))
                label_image = (self.image == label)

            # compute the indices of voxel with adequate label
            x, y, z = label_image.nonzero()
            if len(x) == 0:
                continue  # obviously no reasons to go further !
            # difference with the center
            x = x - center[0]
            y = y - center[1]
            coord = np.array([x, y])
            # convert to real-world units if asked:
            if real:
                coord = vxs * coord
            # compute 1/n_vox * P . P^T
            cov = 1. / len(x) * np.dot(coord, coord.T)
            # Find the eigen values and vectors.
            eig_val, eig_vec = np.linalg.eig(cov)
            eig_vec = np.array(eig_vec).T
            inertia_eig_vec.append(eig_vec)
            inertia_eig_val.append(eig_val)

        if len(labels) == 1:
            return return_list_of_vectors(inertia_eig_vec[0], by_row=True), inertia_eig_val[0]
        else:
            return self.convert_return(
                return_list_of_vectors(inertia_eig_vec, by_row=True), labels), self.convert_return(inertia_eig_val, labels)


def are_these_labels_neighbors(labels, neighborhood):
    """
    This function allows you to make sure the provided labels are all connected neighbors according to a known neighborhood.
    """
    intersection = set()
    for label in labels:
        try:
            inter = set(neighborhood[label]) & set(
                labels)  # it's possible that `neighborhood` does not have key `label`
        except:
            inter = set()
        if inter == set(labels) - {label}:
            return True
        if inter != set():
            intersection.update(inter)

    if intersection == set(labels):
        return True
    else:
        return False


def SpatialImageAnalysis(image, *args, **kwd):
    """
    Constructeur. Detect automatically if the image is 2D or 3D.
    For 3DS (surfacic 3D) a keyword-argument 'surf3D=True' should be passed.
    """
    # -- If 'image' is a string, it should relate to the filename and we try to load it using imread:
    if isinstance(image, str):
        from timagetk.components import imread
        image = imread(image)
    # ~ print args, kwd
    assert len(image.shape) in [2, 3]

    # -- Check if the image is 2D
    if len(image.shape) == 2 or image.shape[2] == 1:
        return SpatialImageAnalysis2D(image, *args, **kwd)
    # -- Else it's considered as a 3D image.
    else:
        return SpatialImageAnalysis3D(image, *args, **kwd)


def read_id_list(filename, sep='\n'):
    """
    Read a *.txt file containing a list of ids separated by `sep`.
    """
    f = open(filename, 'r')
    r = f.read()

    k = r.split(sep)

    list_cell = []
    for c in k:
        if c != '':
            list_cell.append(int(c))

    return list_cell


def save_id_list(id_list, filename, sep='\n'):
    """
    Read a *.txt file containing a list of ids separated by `sep`.
    """
    f = open(filename, 'w')
    for k in id_list:
        f.write(str(k))
        f.write(sep)

    f.close()
