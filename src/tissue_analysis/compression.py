# -*- python -*-
# -*- coding: utf-8 -*-
#
#       TissueAnalysis
#
#       Copyright 2018 CNRS - ENS Lyon - INRIA
#
#       File author(s):
#           Jonathan LEGRAND <jonathan.legrand@ens-lyon.fr>
#
################################################################################

__license__ = "Private/UnderDev"
__revision__ = " $Id$ "

LZ4_IMPORT_ERROR = """LZ4 python-binding are not installed!
Use pip to install it: 'pip install lz4'.
"""

LZ4_VERSION_ERROR = "LZ4 version should be >= '0.19.1', you have '{}'."

GZIP_IMPORT_ERROR = """GZIP library is not installed!
Use pip to install it: 'pip install gzip'.
"""

import time as t
import pickle
from os.path import splitext
from os import environ

conda_env = environ.get('CONDA_DEFAULT_ENV', None)
if conda_env:
    LZ4_IMPORT_ERROR += "Inside a conda env: 'miniconda2/envs/{}/bin/pip install lz4'".format(
        conda_env)
    GZIP_IMPORT_ERROR += "Inside a conda env: 'conda install gzip'"
    GZIP_IMPORT_ERROR += "Inside a conda env: 'miniconda2/envs/{}/bin/pip install gzip'".format(
        conda_env)

# - Test for lz4 library availability and version:
HAS_LZ4 = False
try:
    import lz4.frame
    assert lz4.__version__ > '0.19.1'
except ImportError:
    raise ImportError(LZ4_IMPORT_ERROR)
except AssertionError:
    raise ValueError(LZ4_VERSION_ERROR.format(lz4.__version__))
else:
    HAS_LZ4 = True

# - Test for gzip library availability:
HAS_GZIP = False
try:
    import gzip
except ImportError:
    raise ImportError(GZIP_IMPORT_ERROR)
else:
    HAS_GZIP = True

# - Define dictionary mapping method names to extensions:
METHOD_EXT = {'gzip': '.gz',
              'lz4': '.lz'}

# - Define available extensions and methods:
AVAIL_EXT, AVAIL_METHOD = [], []
if HAS_LZ4:
    AVAIL_METHOD.append('lz4')
    AVAIL_EXT.append(METHOD_EXT['lz4'])
if HAS_GZIP:
    AVAIL_METHOD.append('gzip')
    AVAIL_EXT.append(METHOD_EXT['gzip'])


class Compress(object):
    """
    Compression module, used cPickle and compression algorithms to open/write
    python objects.

    LZ4 algorithm is faster but as a lower compression ratio than GZIP.
    GIZP algorithm is more common and has an higher compression ration than LZ4,
    but is also slower.

    By default we prefer the faster method (if available): LZ4.

    Extensions are:
      * '.lz' for LZ4;
      * '.gz' for GZIP.
    """

    def __init__(self, filename=None, method=None):
        """
        Parameters
        ----------
        filename: str, optional
            the name of the compressed file
        method: str, optional
            name of the compression method to use, if None (default), try to
            guess it from the file extension
        """
        if HAS_LZ4:
            self.default_method = 'lz4'
            self.default_ext = METHOD_EXT['lz4']
        elif HAS_GZIP:
            self.default_method = 'gzip'
            self.default_ext = METHOD_EXT['gzip']
        else:
            raise ImportError("Could not import LZ4 or GZIP libraries. ABORT.")

        # If method is None, we do not compress:
        if method and method in AVAIL_METHOD:
            self.method = method
        else:
            # self.method = self.default_method
            self.method = None

        self.filename = None
        if filename:
            self.filename = filename
            self._check_ext()

    def __str__(self):
        """
        """
        print("Using default compression algorithm: {}".format(self.default_method))

    def _check_ext(self):
        """
        Make sure the file has the appropriate extension.
        """
        fname, ext = splitext(self.filename)
        if ext == "" or ext not in AVAIL_EXT:
            if self.method:
                self.filename += METHOD_EXT[self.method]
            else:
                self.filename += self.default_ext
        else:
            self._guess_method()
        return

    def _guess_method(self):
        """
        Guess the compression method based on the file extension

        Parameters
        ----------
        filename: str
            the name of the compressed file

        Returns
        -------
        method: str
            name of the compression algorithm to use
        """
        fname, ext = splitext(self.filename)
        ext_method = {v: k for k, v in METHOD_EXT.items()}
        try:
            assert ext in ext_method
        except AssertionError:
            # self.method = self.default_method
            # self.filename += METHOD_EXT[self.method]
            print("Could not guess compression method from extension, no compression will be used!")
        else:
            self.method = ext_method[ext]
        return

    def open(self, filename=None, method=None):
        """
        Open compressed file using correct method based on filename extension

        Parameters
        ----------
        filename: str, optional
            the name of the compressed file to open, optional if given to init
        method: str, optional
            name of the compression method to use, if None (default), try to
            guess it from the file extension

        Returns
        -------
        obj: object
            the decompressed & unpickled object saved in the file
        """
        if filename:
            self.filename = filename
            self._check_ext()
            if method is None:
                self._guess_method()
        else:
            try:
                assert self.filename is not None
            except AssertionError:
                raise ValueError("Please give a filename!")

        start = t.time()
        if self.method == 'gzip':
            if not HAS_GZIP:
                raise ImportError(GZIP_IMPORT_ERROR)
            with gzip.open(self.filename, 'rb') as f:
                # - Use pickle to recover file
                obj = pickle.load(f)
        elif self.method == 'lz4':
            if not HAS_LZ4:
                raise ImportError(LZ4_IMPORT_ERROR)
            with lz4.frame.open(self.filename, 'rb') as f:
                # - Use pickle to recover file
                obj = pickle.load(f)
        else:
            with open(self.filename, 'rb') as f:
                # - Use pickle to recover file
                obj = pickle.load(f)

        et = round(t.time() - start, 3)
        print("Successfully loaded file '{}' in {}s".format(self.filename, et))
        return obj

    def write(self, obj, filename=None, method=None):
        """
        Write compressed file using correct method based on filename extension.

        Parameters
        ----------
        obj: object
            python object to write as compressed file
        filename: str
            the name of the compressed file to open
        method: str, optional
            name of the compression method to use, if None (default), try to
            guess it from the file extension and write uncompressed file if
            unknown extension
        """
        if filename:
            self.filename = filename
            self._check_ext()
            if method is None:
                self._guess_method()
        else:
            try:
                assert self.filename is not None
            except AssertionError:
                raise ValueError("Please give a filename!")

        start = t.time()
        if self.method == 'gzip':
            if not HAS_GZIP:
                raise ImportError(GZIP_IMPORT_ERROR)
            with gzip.open(self.filename, 'wb') as f:
                # - Use pickle to dump to compressed file
                pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
        elif self.method == 'lz4':
            if not HAS_LZ4:
                raise ImportError(LZ4_IMPORT_ERROR)
            with lz4.frame.open(self.filename, 'wb') as f:
                # - Use pickle to dump to compressed file
                pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
        else:
            with open(self.filename, 'wb') as f:
                # - Use pickle to dump to compressed file
                pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

        et = round(t.time() - start, 3)
        print("Successfully written file '{}' in {}s".format(self.filename, et))
        return
