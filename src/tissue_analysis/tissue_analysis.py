# -*- python -*-
# -*- coding: utf-8 -*-
#
#       TissueAnalysis
#
#       Copyright 2018 CNRS - ENS Lyon
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# -----------------------------------------------------------------------------

"""
Regroup functions dedicated to the analysis of TissueImage by quantification
of label (cell), wall (cell-wall) and vertex (cell-vertex) properties.
"""

__license__ = "Private/UnderDev"
__revision__ = " $Id$ "

import numpy as np
import scipy.ndimage as nd

from tissue_analysis.util import elapsed_time, euclidean_distance, \
    stuple
from tissue_analysis.util import percent_progress

from tissue_analysis.array_tools import find_geometric_median
from timagetk.components.tissue_image import TissueImage
from timagetk.components.slices import dilation


class TissueProperty(object):
    """
    Base class for property computation.
    """

    def __init__(self, image=None, **kwargs):
        """ Constructor.

        Parameters
        ----------
        image: TissueImage, optional
            labelled image to use for property quantification

        """
        # - Hidden attributes initialisation:
        # -- TissueImage:
        self._image = None
        # -- List of ALL ids:
        self._ids = set([])
        # -- List of ids to exclude from computation (when possible)
        self._excluded_ids = set([])
        # -- Dictionary of properties:
        # * keys are "property names"
        # * values are id-based dictionary with their "property values"
        self._property = {}
        # -- Element order, used for extraction of topological elements coordinates
        self._elem_order = None

        # - Image initialisation:
        if image is not None:
            self._set_image(image)

    @property
    def image(self):
        """Get the labelled image.

        Returns
        -------
        TissueImage
            labelled image
        """
        return self._image

    @image.setter
    def image(self, image):
        """Set the TissueImage to use for property quantification.

        Parameters
        ----------
        image : TissueImage
            labelled image to use for property quantification
        """
        self._set_image(image)
        return

    def _set_image(self, image):
        """
        Hidden method to set the hidden attribute `_image`, the TissueImage to
        use for property quantification.

        Parameters
        ----------
        image : TissueImage
            labelled image to use for property quantification
        """
        try:
            assert isinstance(image, TissueImage)
        except AssertionError:
            raise TypeError("Input 'image' should be a TissueImage.")
        self._image = image
        return

    @property
    def excluded_ids(self):
        """Get the list of excluded ids.

        Returns
        -------
        list
            list of excluded ids
        """
        return self._excluded_ids

    @excluded_ids.setter
    def excluded_ids(self, id_list):
        """Set or update the list of excluded ids.

        Parameters
        ----------
        id_list : list
            list of ids to add to the list of excluded ids
        """
        try:
            assert isinstance(id_list, list)
        except AssertionError:
            raise TypeError("Input 'id_list' should be a list.")

        self._excluded_ids = set(self._excluded_ids) & set(id_list)
        return

    def clear_excluded_ids(self):
        """Clear the list of excluded ids. """
        self._excluded_ids = []
        return

    def _used_ids(self):
        return self._ids - self._excluded_ids

    def _filter_ids(self, ids):
        """Filter a given list of `ids` based on the used ids.

        Parameters
        ----------
        ids : list
            list of ids to

        Returns
        -------
        list
            intersection between given `ids` and used ids (ie. excluding `self._excluded_ids`)

        """
        return list(self._used_ids() & set(ids))

    def _filter_dict(self, dict2filter, ids):
        """Filter a dictionary by its keys with a list of ids.

        Parameters
        ----------
        dict2filter : dictionary
            a dictionary to filter
        ids : list
            list of ids to keep in the keys of the returned dictionary

        Returns
        -------
        dict
            key filtered dictionary
        """
        # - Nothing to filter in such obvious cases:
        if ids is None or ids == []:
            return dict2filter

        # - Check the input type of ids:
        try:
            assert isinstance(ids, list) or isinstance(ids, set)
        except AssertionError:
            raise TypeError("Input 'ids' should be a list!")

        # - If the list of "filtering ids" & the list of "all ids" are different:
        if set(ids) != self._ids:
            # Make sure the given list of tuple are known:
            ids = set(ids) & self._ids
            # Filter the dict:
            dict2filter = {k: dict2filter[k] for k in ids}

        return dict2filter

    def _has_or_init_ppty(self, ppty_name):
        """Test if `ppty_name` exists or initialize it. """
        try:
            assert ppty_name in self._property
        except AssertionError:
            self._property[ppty_name] = {}

    def _missing_ppty_ids(self, ppty_name, ids):
        """ Define the list missing ids (keys) in `ppty_name` with respect to
        given list of `ids`.

        Parameters
        ----------
        ppty_name : str
            name of the property that should contains all defined ids
        ids : list
            the list of ids that should be found in `ppty_name` dictionary. This
            list of is filtered by the set of used ids (`self._used_ids()`)

        Returns
        -------
        list
            list of existing but missing ids in `ppty_name` dictionary

        """
        # - Make sure `ppty_name` exists in `self._property` to avoid `KeyError`:
        self._has_or_init_ppty(ppty_name)
        # - Filter `ids` to avoid computation of undefined ids:
        ids = self._filter_ids(ids)
        return list(set(ids) - set(self._property[ppty_name].keys()))

    def list_properties(self):
        """List the available properties.

        Returns
        -------
        list(str)
            the list of properties
        """
        return self._property.keys()

    def get_property(self, ppty_name, ids=None):
        """Get a property named `ppty_name`.

        If the property does not exist, None is returned.

        Parameters
        ----------
        ppty_name : str
            name of the property to return
        ids : list, optional
            if given (default=None) filter the ids returned

        Returns
        -------
        dict
            dictionary of property `ppty_name`
        """
        try:
            ppty = self._property[ppty_name]
        except KeyError:
            return None
        else:
            return self._filter_dict(ppty, ids)

    def get_coordinates(self, ids=None, real=False):
        """
        Get the coordinates of voxels defining the elements of structural order
        `self._elem_order`.

        Parameters
        ----------
        ids : list(tuple), optional
            if given (default=None) filter the ids returned
        real : bool, optional
            if False (default) the coordinates are in voxel unit, else in real
            units

        Returns
        -------
        dict
            {ids: np.array} dictionary of coordinates, type of element is given
            by `self._elem_order`. Array dimension is the one of the image.
        """
        elem_order = self._elem_order
        try:
            coord = self._property['coordinates']
            assert coord != {}
        except KeyError or AssertionError:
            # Get the topological elements of self._elem_order :
            coord = self.image.topological_elements(elem_order)[elem_order]
            # Save them in the wall_property dictionary
            self._property['coordinates'] = coord

        if self._ids == []:
            # Set the list of ids:
            self._ids = coord.keys()

        coord = self._filter_dict(coord, ids)

        if real:
            vxs = np.array(self.image.voxelsize)
            coord = {k: v * vxs for k, v in coord.items()}

        return coord

    def get_medians(self, ids=None, real=False):
        """Get the computed medians of the topological element.

        Parameters
        ----------
        ids : list(tuple), optional
            if given (default=None) filter the returned ids
        real : bool, optional
            if False (default) the medians are in voxel unit, else in real units

        Returns
        -------
        dict
            {ids: np.array} dictionary of medians coordinates

        """
        coord = self.get_coordinates()

        try:
            medians = self._property['median']
            assert medians != {}
        except KeyError or AssertionError:
            # Compute the wall medians voxel:
            medians = {}
            for k in self._ids:
                if k != (0, 1):
                    median_id = find_geometric_median(coord[k])
                    medians[k] = coord[k][median_id]
            # medians = {k: coord[k][find_geometric_median(coord[k])]
            #            for k in self._ids}
            self._property['median'] = medians

        medians = self._filter_dict(medians, ids)

        if real:
            vxs = np.array(self.image.voxelsize)
            medians = {k: v * vxs for k, v in medians.items()}

        return medians


class LabelProperty(TissueProperty):
    """
    Class gathering methods for 2D and 3D label based properties.
    """

    def __init__(self, image, **kwargs):
        """ Constructor.

        Parameters
        ----------
        image : TissueImage
            labelled image to use for property quantification

        Other Parameters
        ----------------
        excluded_labels : list
            list of labels to exclude from label-type property computation

        """
        TissueProperty.__init__(self, image, **kwargs)

        self._ids = set(self.image.cells())

        excluded_labels = kwargs.get('excluded_labels', None)
        if excluded_labels is not None:
            self.excluded_ids = excluded_labels

    def center_of_mass(self, labels=None, real=True, verbose=False):
        """Return the center of mass of the labels.

        Parameters
        ----------
        labels : None|int|list(int)|str, optional
            if None (default) returns all labels.
            if an integer, make sure it is in self.labels()
            if a list of integers, make sure they are in self.labels()
            if a string, should be in LABEL_STR to get corresponding
            list of cells (case insensitive)
        real : bool, optional
            If True (default), center of mass is in real-world units else in voxels.
        verbose : bool, optional
            control verbosity

        Examples
        --------
        >>> import numpy as np
        >>> a = np.array([[1, 6, 5, 7, 7, 3],
                          [1, 6, 5, 7, 3, 3],
                          [1, 2, 2, 7, 3, 3],
                          [1, 1, 2, 4, 4, 4]])
        >>> from timagetk.components import TissueImage
        >>> img = TissueImage(np.array([a, a, a]), background=1, no_label_id=0)
        >>> from tissue_analysis.tissue_analysis import TissueAnalysis
        >>> analysis = TissueAnalysis(img)

        >>> analysis.label.center_of_mass(7)
        [0.75, 2.75, 0.0]

        >>> analysis.label.center_of_mass([7,2])
        [[0.75, 2.75, 0.0], [1.3333333333333333, 0.66666666666666663, 0.0]]

        >>> analysis.label.center_of_mass()
        [[1.8, 2.2999999999999998, 0.0],
         [1.3333333333333333, 0.66666666666666663, 0.0],
         [1.5, 4.5, 0.0],
         [3.0, 3.0, 0.0],
         [1.0, 2.0, 0.0],
         [1.0, 1.0, 0.0],
         [0.75, 2.75, 0.0]]
        """
        # Check the provided labels:
        labels = self.image.labels(labels)
        # - Check we have all necessary bounding boxes...
        bboxes = self.image.boundingbox(labels, real=False, verbose=verbose)

        # - Initialize the "center_of_mass" property if necessary:
        try:
            assert "center_of_mass" in self._property
        except AssertionError:
            self._property["center_of_mass"] = {}

        t_start = time.time()
        if verbose:
            print("Computing cells center of mass:")

        center = {}
        nb_labels = len(labels)
        progress = 0
        for n, label in enumerate(labels):
            if verbose:
                progress = percent_progress(progress, n, nb_labels)
            try:
                assert label in self._property["center_of_mass"]
            except AssertionError:
                try:
                    crop = bboxes[label]
                    crop_im = self.image.get_array()[crop]
                    com = np.array(
                        nd.center_of_mass(crop_im, crop_im, index=label))
                    com = [com[i] + sl.start for i, sl in enumerate(crop)]
                except ValueError:
                    crop_im = self.image.get_array()
                    com = np.array(nd.center_of_mass(crop_im, crop_im, index=label))
                self._property["center_of_mass"][label] = com
                center[label] = com
            else:
                center[label] = self._property["center_of_mass"][label]

        if real:
            vxs = self.image.voxelsize
            center = {l: np.multiply(center[l], vxs) for l in labels}

        if verbose:
            elapsed_time(t_start)

        if len(labels) == 1:
            return center[labels[0]]
        else:
            return center

    def neighbors(self, labels=None, verbose=True):
        """ Return the neighbors dictionary per label.

        Parameters
        ----------
        labels : None|int|list(int) or str, optional
            labels for which neighbors should be returned, if None (default) returns all labels.
        verbose : bool, optional
            control code verbosity

        Returns
        -------
        dict
            {i: neighbors(i)} for i in `labels`

        Notes
        -----
        If `labels` is an integer, make sure it is in self.labels()
        If `labels` is a list of integers, make sure they are in self.labels()
        If `labels` is a string, should be in LABEL_STR to get corresponding
        list of cells (case insensitive)

        """
        return self.image.neighbors(labels=labels, verbose=verbose)

    def list_epidermal_labels_from_neighbors(self, **kwargs):
        """ List the epidermal labels from the detected neighborhood.

        Other Parameters
        ----------------
        verbose : bool, optional
            control code verbosity

        Returns
        -------
        list
            list of the epidermal labels

        """
        bkg = self.image.background
        neighbors = self.neighbors(verbose=kwargs.get('verbose', False))
        return [label for label, nei in neighbors.items() if bkg in nei]


def label_size(array, labels=None):
    """Compute the size, or number of occurrence, of a list of labels in a labelled
    array.

    Parameters
    ----------
    array : np.array
        labelled numpy array
    labels : list
        list of labels found in the array

    Returns
    -------
    dict
        label besad dictionary of "label size"

    Examples
    --------
    >>> import numpy as np
    >>> a = np.array([[1, 1, 1, 1, 1, 1],
                      [6, 6, 5, 7, 7, 3],
                      [6, 2, 5, 7, 3, 3],
                      [2, 2, 2, 4, 4, 4]])
    >>> label_size(a)
    {1: 6.0, 2: 4.0, 3: 3.0, 4: 3.0, 5: 2.0, 6: 3.0, 7: 3.0}
    >>> label_size(a, 1)
    {1: 6.0}
    >>> label_size(a, [2, 5])
    {2: 4.0, 5: 2.0}
    """
    if labels is None:
        labels = list(np.unique(array))
    if isinstance(labels, int):
        labels = [labels]

    size = list(nd.sum(np.ones_like(array), array, index=np.uint16(labels)))
    return dict(zip(labels, size))


class LabelProperty2D(LabelProperty):
    """
    Class gathering methods specific to 2D images.
    """

    def __init__(self, images, **kwargs):
        """

        Parameters
        ----------
        images
        kwargs
        """
        LabelProperty.__init__(self, images, **kwargs)

    def area(self, labels=None, real=True, **kwargs):
        """
        Compute the area of a label.

        Parameters
        ----------
        labels: list, optional
            list of labels for which property computation should be done, by
            default (None) use them all
        real: bool, optional

        kwargs

        Returns
        -------

        """
        verbose = kwargs.get('verbose', True)
        # - Check the provided labels:
        labels = self.labels(labels)
        n_lab = len(labels)

        if verbose:
            print("Computing 'area' property for {} labels:".format(n_lab))
            t_start = time.time()

        # - Compute the area using scipy.ndimage.sum()
        area = label_size(self.get_array(), labels)
        # - Convert to real-world units if asked:
        if real:
            vxs = self.voxelsize
            area = {l: np.multiply(a, np.prod(vxs)) for l, a in area.items()}

        if verbose:
            elapsed_time(t_start)
        return area


class LabelProperty3D(LabelProperty):
    """
    Class gathering methods specific to labels and 3D images.
    """

    def __init__(self, images, **kwargs):
        """

        Parameters
        ----------
        images
        kwargs
        """
        LabelProperty.__init__(self, images, **kwargs)

    def volume(self, labels=None, real=True, **kwargs):
        """ Compute the volume of the labels.

        Parameters
        ----------
        labels : None|int, optional
            label or list of labels to be measured. If None (default), all
            labels are used (except those in self._excludedlabels)
        real : bool, optional
            if True (default), volume is in real units else in voxels

        Other Parameters
        ----------------
        verbose : bool
            if ``True`` increase code verbosity

        Returns
        -------
        dict
            label based dictionary for volume property

        """
        if labels is None:
            labels = self._used_ids()

        # - Check the provided labels against `self._used_ids()`:
        missing_labels = self._missing_ppty_ids("volume", labels)

        if missing_labels != []:
            self._volume(missing_labels, **kwargs)

        volume = self.get_property("volume", labels)

        # - Convert to real-world units if asked:
        if real:
            vxs = self.image.voxelsize
            volume = {l: np.multiply(v, np.prod(vxs)) for l, v in
                      volume.items()}

        return volume

    def _volume(self, labels, **kwargs):
        """ Hidden method computing the cell volume. """
        verbose = kwargs.get('verbose', True)
        t_start = time.time()
        n_lab = len(labels)
        if verbose:
            print("Computing 'volume' property for {} labels:".format(n_lab))

        # - Compute the volumes using scipy.ndimage.sum()
        volume = label_size(self.image.get_array(), labels)

        if verbose:
            elapsed_time(t_start)

        self._property['volume'].update(volume)
        return


    def distance_to_neighbors(self, labels=None, real=True, **kwargs):
        """ Compute the distance between the neighboring label barycenters.

        Parameters
        ----------
        labels : None|int, optional
            label or list of labels to be measured. If None (default), all
            labels are used (except those in self._excludedlabels)
        real : bool, optional
            if True (default), distance is in real units else in voxels

        Other Parameters
        ----------------
        verbose : bool
            if ``True`` increase code verbosity

        Returns
        -------
        dict
            label based dictionary for distance between neighbors barycenter

        """
        verbose = kwargs.get('verbose', True)
        # - Check the provided labels:
        labels = self.image.labels(labels)
        n_lab = len(labels)

        t_start = time.time()
        if verbose:
            print("Computing 'distance_to_neighbors' property for {} labels:".format(n_lab))

        neighbors = self.image.neighbors(labels, verbose=verbose)
        com = self.center_of_mass(labels, real=real, verbose=verbose)

        bary_dist = {label: [euclidean_distance(com[label], com[nei]) for nei in neighbors[label] if nei in com] for label in labels}

        if verbose:
            elapsed_time(t_start)

        return bary_dist

    def average_distance_to_neighbors(self, labels=None, real=True, **kwargs):
        """ Compute the average distance of a label to its neighbors.

        Parameters
        ----------
        labels : None|int, optional
            label or list of labels to be measured. If None (default), all
            labels are used (except those in self._excludedlabels)
        real : bool, optional
            if True (default), distance is in real units else in voxels

        Other Parameters
        ----------------
        verbose : bool
            if ``True`` increase code verbosity

        Returns
        -------
        dict
            label based dictionary for average distance to its neighbors

        """
        verbose = kwargs.get('verbose', True)
        # - Check the provided labels:
        labels = self.image.labels(labels)
        n_lab = len(labels)

        t_start = time.time()
        bary_dist = self.distance_to_neighbors(labels, real, **kwargs)

        if verbose:
            print("Computing 'average_distance_to_neighbors' property for {} labels:".format(n_lab))
        average_dist = {label: np.mean(bary_dist[label]) for label in labels}

        if verbose:
            elapsed_time(t_start)

        return average_dist

class WallProperty(TissueProperty):
    """
    Class gathering methods for 2D and 3D wall based properties.

    Walls are defined by their label-pair, ie. the couple of labels in contact.
    """

    def __init__(self, image, **kwargs):
        """
        Constructor.

        Parameters
        ----------
        image: TissueImage
            labelled image to use for property quantification
        """
        TissueProperty.__init__(self, image, **kwargs)

        # - Declare new attributes:
        self._elem_order = 2


class WallProperty2D(WallProperty):
    """
    Class gathering methods specific to 2D wall based properties.
    """

    def __init__(self, image, **kwargs):
        """
        Constructor.

        Parameters
        ----------
        image: TissueImage
            labelled image to use for property quantification
        """
        WallProperty.__init__(self, image, **kwargs)

    def _neighbor_kernels(self):
        """ Define the kernel used during wall length computation.

        Returns
        -------
        tuple
            kernel
        """
        if self._kernels is None:
            X1kernel = np.zeros((3, 3), np.bool)
            X1kernel[:, 1] = True
            X1kernel[0, 1] = False
            X2kernel = np.zeros((3, 3), np.bool)
            X2kernel[:, 1] = True
            X2kernel[2, 1] = False
            Y1kernel = np.zeros((3, 3), np.bool)
            Y1kernel[1, :] = True
            Y1kernel[1, 0] = False
            Y2kernel = np.zeros((3, 3), np.bool)
            Y2kernel[1, :] = True
            Y2kernel[1, 2] = False
            self._kernels = (X1kernel, X2kernel, Y1kernel, Y2kernel)

        return self._kernels

    def _pixel_edge_length(self):
        """The length of pixel edges.

        Returns
        -------
        np.array:
            vxs_x, vxs_y;
        """
        vxs = self.image.voxelsize
        return np.array([vxs[0], vxs[1]])

class WallProperty3D(WallProperty):
    """
    Class gathering methods specific to 3D wall based properties.
    """

    def __init__(self, image, **kwargs):
        """ Constructor.

        Parameters
        ----------
        image: TissueImage
            labelled image to use for property quantification
        """
        WallProperty.__init__(self, image, **kwargs)

    def _neighbor_kernels(self):
        """Define the kernel used during wall area computation.

        Returns
        -------
        tuple
            kernel
        """
        if self._kernels is None:
            X1kernel = np.zeros((3, 3, 3), np.bool)
            X1kernel[:, 1, 1] = True
            X1kernel[0, 1, 1] = False
            X2kernel = np.zeros((3, 3, 3), np.bool)
            X2kernel[:, 1, 1] = True
            X2kernel[2, 1, 1] = False
            Y1kernel = np.zeros((3, 3, 3), np.bool)
            Y1kernel[1, :, 1] = True
            Y1kernel[1, 0, 1] = False
            Y2kernel = np.zeros((3, 3, 3), np.bool)
            Y2kernel[1, :, 1] = True
            Y2kernel[1, 2, 1] = False
            Z1kernel = np.zeros((3, 3, 3), np.bool)
            Z1kernel[1, 1, :] = True
            Z1kernel[1, 1, 0] = False
            Z2kernel = np.zeros((3, 3, 3), np.bool)
            Z2kernel[1, 1, :] = True
            Z2kernel[1, 1, 2] = False
            self._kernels = (X1kernel, X2kernel, Y1kernel, Y2kernel, Z1kernel, Z2kernel)

        return self._kernels

    def list_epidermal_walls(self):
        """ Returns the list of detected epidermal walls.

        Returns
        -------
        list
            list of epidermal walls as a pair of tuples.
        """
        bkg = self.image.background
        return [id for id in self._used_ids() if bkg in id]

    def _voxel_face_area(self):
        """ The area of each voxels face.

        There is three unique faces: x*y, x*z and y*z.

        Returns
        -------
        np.array:
            [vxs_y*vxs_z, vxs_x*vxs_z, vxs_x*vxs_y]
        """
        vxs = self.image.voxelsize
        return np.array([vxs[1] * vxs[2], vxs[2] * vxs[0], vxs[0] * vxs[1]])

    def area(self, labelpairs=None, real=True, **kwargs):
        """ Compute the wall area for given label pairs.

        Parameters
        ----------
        labelpairs : list
            list of label-pairs defining walls
        real : bool, optional
            if True (default), volume is in real units else in voxels

        Other Parameters
        ----------------
        verbose : bool
            if ``True`` increase code verbosity

        Returns
        -------
        dict
            wall based dictionary for area property

        """
        if labelpairs is None:
            labelpairs = self._used_ids()

        # - Check the provided labels against `self._used_ids()`:
        missing_labels = self._missing_ppty_ids("area", labelpairs)

        if missing_labels != []:
            self._area(missing_labels, **kwargs)

        if real:
            area = self.get_property("area", labelpairs)
        else:
            area = self.get_property("area_voxel", labelpairs)

        return area

    def _area(self, labelpairs, **kwargs):
        """ Hidden method computing the area. """
        verbose = kwargs.get('verbose', True)
        t_start = time.time()
        n_lab = len(labelpairs)
        if verbose:
            print("Computing 'area' property for {} label-pairs:".format(n_lab))

        for label, neighbor in labelpairs:
            self._wall_area_from_label_neighbors(label, neighbor)

        if verbose:
            elapsed_time(t_start)

        return

    def wall_area_from_label_neighbors(self, label, neighbors, real=True):
        """ Compute contact area between a cells and its neighbors.

        Parameters
        ----------
        label : int
            the label for which to return the contact surface areas
        neighbors : list
            list of neighbors to use as contact surface
        real : bool, optional
            if True (default), area is in real units else in voxels

        Returns
        -------
        dict
            contact surface areas with SORTED label-pairs
        """
        self._wall_area_from_label_neighbors(label, neighbors)

        if real:
            ppty = self._property['area']
        else:
            ppty = self._property['area_voxel']

        return {stuple([label, nei]): ppty[stuple([label, nei])] for nei in neighbors}


    def _wall_area_from_label_neighbors(self, label, neighbors):
        """ Compute contact area between a cells and its neighbors.

        Parameters
        ----------
        label : int
            the label for which to return the contact surface areas
        neighbors : list
            list of neighbors to use as contact surface

        Returns
        -------
        dict
            real contact surface areas with SORTED label-pairs
        dict
            voxel contact surface areas with SORTED label-pairs

        """
        vfa = self._voxel_face_area()

        # Try to crop the image around the label of interest:
        try:
            d_bbox = dilation(self.image.boundingbox(label))
            dilated_bbox_img = self.image[d_bbox]
        except ValueError:
            # if no bounding box can be found for 'label', use the whole image
            dilated_bbox_img = self.image
        # Create a mask defining 'label' position in the image:
        mask_img = (dilated_bbox_img == label)

        wall_voxel, wall = {}, {}
        xyz_kernels = self._neighbor_kernels()
        for a in range(len(xyz_kernels)):
            dil = nd.binary_dilation(mask_img, structure=xyz_kernels[a])
            frontier = dilated_bbox_img[dil ^ mask_img]
            for n in neighbors:
                nb_pix = len(frontier[frontier == n])
                area = float(nb_pix * vfa[a // 2])
                i, j = stuple([label, n])
                wall[(i, j)] = wall.get((i, j), 0.0) + area
                wall_voxel[(i, j)] = wall_voxel.get((i, j), 0.0) + nb_pix

        # - Update the area properties:
        self._property['area'].update(wall)
        self._property['area_voxel'].update(wall_voxel)
        return


class EdgeProperty(TissueProperty):
    """
    Class gathering methods for 2D and 3D edge based properties.
    """

    def __init__(self, image, **kwargs):
        """
        Constructor.

        Parameters
        ----------
        image: TissueImage
            labelled image to use for property quantification
        """
        TissueProperty.__init__(self, image, **kwargs)

        # - Declare new attributes:
        self._elem_order = 1


class EdgeProperty2D(EdgeProperty):
    """
    Class gathering methods for 2D and 3D edge based properties.
    """

    def __init__(self, image, **kwargs):
        """
        Constructor.

        Parameters
        ----------
        image: TissueImage
            labelled image to use for property quantification
        """
        EdgeProperty.__init__(self, image, **kwargs)


class EdgeProperty3D(EdgeProperty):
    """
    Class gathering methods for 2D and 3D edge based properties.
    """

    def __init__(self, image, **kwargs):
        """
        Constructor.

        Parameters
        ----------
        image: TissueImage
            labelled image to use for property quantification
        """
        EdgeProperty.__init__(self, image, **kwargs)

    def list_epidermal_edges(self):
        """ Returns the list of detected epidermal edges.

        Returns
        -------
        list
            list of epidermal edges as a triplet of tuples.
        """
        bkg = self.image.background
        return [id for id in self._used_ids() if bkg in id]


class VertexProperty(TissueProperty):
    """
    Class gathering methods for 2D and 3D vertex based properties.
    """

    def __init__(self, image, **kwargs):
        """
        Constructor.

        Parameters
        ----------
        image: TissueImage
            labelled image to use for property quantification
        """
        TissueProperty.__init__(self, image, **kwargs)

        # - Declare new attributes:
        self._elem_order = 0


class VertexProperty2D(VertexProperty):
    """
    Class gathering methods for 2D and 3D vertex based properties.
    """

    def __init__(self, image, **kwargs):
        """ Constructor.

        Parameters
        ----------
        image: TissueImage
            labelled image to use for property quantification
        """
        VertexProperty.__init__(self, image, **kwargs)


class VertexProperty3D(VertexProperty):
    """
    Class gathering methods for 2D and 3D vertex based properties.
    """

    def __init__(self, image, **kwargs):
        """ Constructor.

        Parameters
        ----------
        image: TissueImage
            labelled image to use for property quantification
        """
        VertexProperty.__init__(self, image, **kwargs)

    def list_epidermal_vertices(self):
        """ Returns the list of detected epidermal vertices.

        Returns
        -------
        list
            list of epidermal vertices as a triplet of tuples.
        """
        bkg = self.image.background
        return [id for id in self._used_ids() if bkg in id]


class TissueAnalysis(object):

    def __init__(self, image, **kwargs):
        """ Constructor. Detect automatically if the image is 2D or 3D.

        Parameters
        ----------
        image : TissueImage
            the labelled image to use for property quantification

        Other Parameters
        ----------------
        auto_init : bool
            if False (default=True), do not perform objects initialisation
        excludedlabels : list
            list of labels to ignore if found in the image, default is None

        Notes
        -----
        Except 'surf3D', positional and keyword arguments are passed to
        the called SpatialImageAnalysis?D
        Not defining a 'background' value might prevent some functionality to work

        Examples
        --------

        """
        # - Check 'image' instance type:
        try:
            assert isinstance(image, TissueImage)
        except AssertionError:
            raise TypeError("Input 'image' should be a TissueImage.")
        # - Check 'image' dimensionality:
        try:
            assert image.ndim in [2, 3]
        except AssertionError:
            raise ValueError("Input 'image' should be 2D, 3DS or 3D!")

        # -- Check if the image is 2D:
        try:
            assert image.shape[2] == 1
        except AssertionError:
            pass
        except IndexError:
            pass
        else:
            image = image.to_2D()

        if image.is2D():
            self.label = LabelProperty2D(image, **kwargs)
            self.wall = WallProperty2D(image, **kwargs)
            self.edge = EdgeProperty2D(image, **kwargs)
            self.vertex = VertexProperty2D(image, **kwargs)
        else:
            self.label = LabelProperty3D(image, **kwargs)
            self.wall = WallProperty3D(image, **kwargs)
            self.edge = EdgeProperty3D(image, **kwargs)
            self.vertex = VertexProperty3D(image, **kwargs)

        if kwargs.get('auto_init', True):
            topo_init_start = time.time()
            # - Extracting all topological elements coordinates:
            elements_coord = self.label.image.topological_elements()
            # - Initialisation of label, wall, edge & vertex list and coordinates dict:
            # -- Initialise label set:
            self.label._ids = set(self.label.image.labels())
            # -- Get wall ids set and coordinates:
            self.wall._ids = set(elements_coord[2].keys())
            self.wall._property['coordinates'] = elements_coord[2]
            # -- Get edge ids set and coordinates:
            self.edge._ids = set(elements_coord[1].keys())
            self.edge._property['coordinates'] = elements_coord[1]
            # -- Get vertex ids set and coordinates:
            self.vertex._ids = set(elements_coord[0].keys())
            self.vertex._property['coordinates'] = elements_coord[0]
            print("Topological element initialisation, ")
            print(elapsed_time(topo_init_start))

if __name__ == "__main__":
    """
    For help, in a terminal: `$ python tissue_analysis.py -h`
    """
    import time
    import argparse

    from timagetk.util import elapsed_time
    from timagetk.io.util import assert_exists, get_pne

    parser = argparse.ArgumentParser(
        description='Extract spatial information from a segmented image.')

    parser.add_argument('image', type=str, nargs=1,
                        help="segmented image to analyse.")

    # optional arguments:
    parser.add_argument('--output_path', type=str, nargs=1,
                        help="if specified, change the output path.")

    args = parser.parse_args()
    # - Variables definition from argument parsing:
    # -- Segmented image:
    img_fname = args.image
    # - Get name and extension:
    path, name, ext = get_pne(img_fname)
    print("Got segmented image: '{}'".format(name))

    # - OPTIONAL ARGUMENTS:
    # -- Output path:
    if args.output_path:
        assert_exists(args.output_path)
    else:
        args.output_path = path

