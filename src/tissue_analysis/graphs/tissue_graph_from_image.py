# -*- python -*-
# -*- coding: utf-8 -*-
#
#       TissueAnalysis
#
#       Copyright 2018 ENS - CNRS- INRIA
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
################################################################################

"""
This module provide a class that construct a TissueGraph 3D/4D from 1/several 
labelled images. In the later case a list of lineage between the labelled images
is required.
"""

__license__ = "Cecill-C"
__revision__ = " $Id$ "

import os
import math
import warnings
import numpy as np

from tissue_analysis.util import stuple
from tissue_analysis.util import fname_NoExt
from tissue_analysis.util import today_date_time

from tissue_analysis.graphs.lineage import Lineage
from tissue_analysis.graphs.lineage import lineage_from_file
from tissue_analysis.graphs.lineage import DEFAULT_LINEAGE_FMT
from tissue_analysis.graphs.lineage import AVAILABLES_LINEAGE_FMT

from tissue_analysis.graphs.tissue_graph import TissueGraph3D
from tissue_analysis.graphs.tissue_graph import TissueGraph4D

from tissue_analysis.spatial_image_analysis import DICT
from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
from tissue_analysis.array_tools import projection_matrix

from timagetk.io import imread
from timagetk.components import SpatialImage

from cellcomplex.graph import PropertyGraph

def zip_brackets(functions, variables):
    """
    Return a string '<f_name>(<variable>)' for f_name in functions and for var in variables.
    """
    return ["{}({})".format(f, v) for v in variables for f in functions]


DEFAULT_TMP_FNAME = "tmp_tgfi_process"

################################################################################
# - SPATIAL properties:
################################################################################
"""
SPATIAL property definitions:
------------------------------
  * SPATIAL_DIFF_FUNCTIONS should be applicable to any CELL_SCALAR & the 
  WALL_SCALAR related to the epidermal wall.
  They should also work on CELL_TP_SCALAR.


Cell level:
~~~~~~~~~~~
These properties are to be found under / associated to 'vertex_properties'.

  * 'L1' is a boolean indicating if the cell is located in the first layer of 
  the tissue (ie. the epidermis);
  
  * 'L2' is a boolean indicating if the cell is located in the second layer of 
  the tissue (ie. bellow the epidermis);
  
  * 'marginal_labels' is a boolean indicating if the cell is at the margin of
  the analysed tissue;
  
  * 'area' is the total area of the cell walls;
  
  * 'volume' is the REAL volume of the cell;
  
  * 'neighbors_count' is the number of neighbors per cell;
  
  * 'shape_anisotropy_3D' is the fractional anisotropy of the inertia tensor;
  
  * 'barycenter' is the barycenter of the cell;
  
  * 'boundingbox' is the square bounding box surrounding the cell;
  
  * 'inertia_tensor_3D' is the 3D inertia tensor of the cell;


Wall level:
~~~~~~~~~~~
Except for the epidermal wall, these properties are to be found under / 
associated to 'edge_properties'.

:WARNING: there is no edge between the epidermal cells and the background, thus 
all "epidermis related features" are saved as vertex_properties to the
corresponding epidermal cell!

:WARNING: there is no structure dedicated to the "wall edge" !!!

  * 'wall_area' is the REAL area of each wall;

  * 'epidermis_area' is the REAL area of the epidermal wall (vertex_property);

  * 'wall_*_curvature', where * can be in ['gaussian', 'mean', 'ratio'], 
  are the related transformation of the wall principal curvatures k1 & k2, see
  'wall_principal_curvatures';

  * 'epidermis_*_curvature', where * can be in ['gaussian', 'mean', 'ratio'], 
  are the related transformation of the epidermal wall principal curvatures
  k1 & k2, see 'epidermis_principal_curvatures';

  * 'epidermis_local_*_curvature', where * can be in ['gaussian', 'mean', 
  'ratio'], are the related transformation of the epidermal wall locally 
  estimated principal curvatures k1 & k2, see epidermis_local_principal_curvature;

  * 'wall_median' & 'wall_median_voxel' are the median voxel of the wall, 
  respectively in REAL and VOXEL units;

  * 'wall_edge_median'  & 'wall_edge_median_voxel' are the median voxel of the 
  wall edge, respectively in REAL and VOXEL units;
  ! NOT AVAILABLE YET !

  * 'epidermis_median' & 'epidermis_median_voxel' are the median voxel
  of the epidermal wall, respectively in REAL and VOXEL units;

  * 'epidermis_edge_median' & 'epidermis_edge_median_voxel' are the 
  median voxel of the epidermal wall edge, respectively in REAL and VOXEL units;

  * 'wall_orientation' is (or at least should be) the normal to the wall;

  * 'rank2_projection_matrix' is the rank-2 subspace projection matrix allowing
  to project all voxels associated to the wall in a 2D space (flattening);

  * 'wall_principal_curvatures' is the estimated principal curvatures (k1 & k2)
  based on the wall voxels;

  * 'epidermis_principal_curvatures' is the estimated principal curvatures (k1 &
  k2) based on the epidermal wall voxels;

  * 'epidermis_local_principal_curvature' is the estimated principal curvatures
  (k1 & k2) based on a sampling the voxels of the epidermal layer for a given 
  radius around the epidermal wall median (ie. might span more or less than the
  epidermal wall of a given cell);
"""
# TODO: change 'epidermis_*median*' to 'epidermis_*median*' ?
# TODO: create a structure to keep the "wall_edes" as we do with the vertex & edges?
# TODO: check that 'wall_orientation' contains the "normal vector to the wall" and if so, remane it 'wall_normal'

# - Spatial differentiation function (to be used with CELL_SCALAR & epidermal WALL_SCALAR):
SPATIAL_DIFF_FUNCTIONS = ['Laplacian', 'MeanAbsoluteDeviation']

# - CELL level:
# -- Computable list of cell groups:
CELL_LIST = ['L1', 'L2', 'marginal_labels']
# -- Computable scalars:
CELL_SCALAR = ['area', 'volume', 'neighbors_count', 'shape_anisotropy_3D']
CELL_SCALAR += zip_brackets(SPATIAL_DIFF_FUNCTIONS, CELL_SCALAR)
# -- Computable vectors:
CELL_COORDINATE = ['barycenter', 'boundingbox']
# -- Computable tensors:
CELL_TENSOR = ['inertia_tensor_3D']

# - REQUIREMENTS:
CELL_REQUIREMENTS = {
    "area": ['wall_area'],
    "shape_anisotropy_3D": ['inertia_tensor_3D']
}

# - WALL level:
# :WARNING: there is no edge between L1 cells and the background, thus all epidermis related features are saved as vertex_properties (associated to the defined vertex)!
# -- Computable list of wall groups:
WALL_LIST = []
# TODO: add 'epidermis', 'L1_anticlinal', 'L1/L2', ...
# -- Computable scalars:
WALL_SCALAR = ['wall_area', 'epidermis_area',
               'wall_gaussian_curvature',
               'wall_mean_curvature',
               'wall_curvature_ratio',
               'epidermis_gaussian_curvature',
               'epidermis_mean_curvature',
               'epidermis_curvature_ratio',
               'epidermis_local_gaussian_curvature',
               'epidermis_local_mean_curvature',
               'epidermis_local_curvature_ratio']
# -- Computable vectors:
WALL_COORDINATE = ['wall_median', 'wall_median_voxel',
                   'wall_edge_median', 'wall_edge_median_voxel',
                   'epidermis_median', 'epidermis_median_voxel',
                   'epidermis_edge_median', 'epidermis_edge_median_voxel']
# -- Computable tensors:
WALL_TENSOR = ['wall_orientation', 'epidermis_orientation',
               'rank2_projection_matrix', 'epidermis_rank2_projection_matrix',
               'wall_principal_curvatures', 'epidermis_principal_curvatures',
               'epidermis_local_principal_curvature']
# :WARNING: 'epidermis_local_principal_curvature' is not exactly a "wall-based" tensor since it use local sampling of the epidermis around the epidermal wall barycenter

# - REQUIREMENTS:
WALL_REQUIREMENTS = {
    # -- Computable scalars:
    "epidermis_area": ['L1'],
    "wall_gaussian_curvature": ['wall_principal_curvatures'],
    "wall_mean_curvature": ['wall_principal_curvatures'],
    "wall_curvature_ratio": ['wall_principal_curvatures'],
    "epidermis_gaussian_curvature": ['epidermis_principal_curvatures'],
    "epidermis_mean_curvature": ['epidermis_principal_curvatures'],
    "epidermis_curvature_ratio": ['epidermis_principal_curvatures'],
    "epidermis_local_gaussian_curvature": [
        'epidermis_local_principal_curvature'],
    "epidermis_local_mean_curvature": ['epidermis_local_principal_curvature'],
    "epidermis_local_curvature_ratio": ['epidermis_local_principal_curvature'],
    # -- Computable tensors:
    "wall_orientation": ['wall_median', 'epidermis_median'],
    "epidermis_orientation": ['L1', 'epidermis_median_voxel'],
    "wall_principal_curvatures": ['L1', 'wall_median_voxel'],
    "epidermis_principal_curvatures": ['L1', 'epidermis_median_voxel'],
    "epidermis_local_principal_curvature": ['L1', 'epidermis_median_voxel'],
}

# 'wall_orientation' requires:
#  - 'wall_median_voxel' as origins for CGAL curvature estimation function (with parameter 'fitting_degree=0').
# 'wall_principal_curvatures' requires:
#  - 'wall_median_voxel' as origins for CGAL curvature estimation function (with parameter 'fitting_degree=2').

CELL_SPATIAL_FEATURE = CELL_LIST + CELL_SCALAR + CELL_COORDINATE + CELL_TENSOR
WALL_SPATIAL_FEATURE = WALL_LIST + WALL_SCALAR + WALL_COORDINATE + WALL_TENSOR
SPATIAL_FEATURES = CELL_SPATIAL_FEATURE + WALL_SPATIAL_FEATURE
# TODO: SPATIAL_DIFF_FUNCTIONS could also apply to epidermal WALL_SCALAR
# TODO: SPATIAL_DIFF_FUNCTIONS could also apply to CELL_TP_SCALAR


################################################################################
# - TEMPORAL properties:
################################################################################
"""
TEMPORAL property definitions:
==============================
  * TEMPORAL_DIFF_FUNCTIONS should be applicable to any CELL_SCALAR & the 
  WALL_SCALAR related to the epidermal wall.

Cell level:
~~~~~~~~~~~
List of cells:
--------------

Cell scalars:
-------------
  * 'volumetric_strain_rate' is the product of the 3D strain rates (see 
  'strain_rate_3D') of a cell;

  * 'cell_strain_rate_anisotropy' is the fractional anisotropy of the 3D strain 
  rates (see 'strain_rate_3D') of a cell;

Cell coordinates:
-----------------
  * '3D_landmarks' is the temporal association of the REAL 3D coordinates for 
  the wall median (see 'wall_median');
  # TODO: IT COULD USE THE 'wall_edge_median' ALSO!

Cell vectors:
-------------
  * 'cell_stretch_ratio' is the is the 3D absolute REAL deformation of the cell
  obtained by SVD of the wall's 3D affine deformation matrix (see  
  'cell_aff_def_tensor');

  * 'strain_rate_3D' is the 3D time-relative REAL deformation of the cell
  obtained by temporal scaling of the 3D stretch ratio (see 
  'cell_stretch_ratio');

Cell tensors:
-------------
  * 'cell_aff_def_tensor' is the 3D affine deformation tensor of the cell 
  obtained using its 3D landmarks (see '3D_landmarks');

  * 'normed_stretch_3D' is the 3D stretch directions before and after the affine
  deformation of the cell (see 'cell_aff_def_tensor');

  * 'normed_stretch_3D_before' is the 3D stretch directions before the affine
  deformation of the cell (see 'cell_aff_def_tensor');

  * 'normed_stretch_3D_after' is the 3D stretch directions after the affine
  deformation of the cell (see 'cell_aff_def_tensor');


Wall level:
~~~~~~~~~~~
:WARNING: there is no edge between the epidermal cells and the background, thus 
all "epidermis related features" are saved as vertex_properties to the
corresponding epidermal cell!

List of walls:
--------------
  * 'division_wall' is a boolean indicating if the wall is new compared to the
  previous time-point;

Wall scalars:
-------------
  * 'areal_strain_rate' is the product of the 2D strain rates (see
  'strain_rate_2D') of a wall;

  * 'strain_rate_anisotropy_2D' is the difference over the sum of the 2D strain 
  rates (see 'strain_rate_2D') of a wall;

  * 'expansion_anisotropy' is the min over max ratio of the 2D strain rates (see
  'strain_rate_2D') of a wall;

  * 'epidermis_areal_strain_rate' is the product of the 2D strain rates (see
  'strain_rate_2D') of the epidermal wall;

  * 'epidermis_strain_rate_anisotropy_2D' is the difference over the sum of the 
  2D strain rates (see 'epidermis_strain_rate_2D') of the epidermal wall;

  * 'epidermis_expansion_anisotropy' is the min over max ratio of the 2D strain 
  rates (see 'epidermis_strain_rate_2D') of the epidermal wall;

Wall coordinates:
-----------------
  * '3DS_landmarks' is the temporal association of the REAL 3D coordinates for 
  the wall-edge medians (see 'wall_edge_median') and wall median (see 
  'wall_median');
  ! NOT AVAILABLE YET !

  * 'epidermis_3DS_landmarks' is temporal association of the REAL 3D coordinates
  for the epidermal wall-edge medians (see 'epidermis_edge_median') and 
  epidermal wall median (see 'epidermis_median');

Wall vectors:
-------------
  * 'stretch_ratios_2D' is the 2D absolute REAL deformation of the wall obtained
   by SVD of the wall's 2D affine deformation matrix (see  
  'flat_wall_aff_def_tensor');

  * 'strain_rate_2D' is the 2D time-relative REAL deformation of the wall
  obtained by temporal scaling of the 2D stretch ratio (see 'stretch_ratios_2D');

  * 'epidermis_stretch_ratios_2D' is the 2D absolute REAL deformation of the 
  epidermal wall obtained by SVD of the wall's 2D affine deformation matrix (see  
  'flat_wall_aff_def_tensor');

  * 'epidermis_strain_rate_2D' is the 2D time-relative REAL deformation of the 
  epidermal wall obtained by temporal scaling of the 2D stretch ratio (see
   'stretch_ratios_2D');

Wall tensors:
-------------
  * 'wall_aff_def_tensor' is the 3D affine deformation tensor of the wall 
  obtained using its 3D landmarks (see '3DS_landmarks');
  ! NOT AVAILABLE YET !

  * 'wall_epidermis_def_tensor' is the 3D affine deformation tensor of the 
  epidermal wall its 3D landmarks (see 'epidermis_3DS_landmarks');

  * 'flat_wall_aff_def_tensor' is the 2D affine deformation tensor of the wall 
  obtained using its 3D landmarks (see '3DS_landmarks') projected in the rank-2 
  subspace of the wall (see 'rank2_projection_matrix');
  ! NOT AVAILABLE YET !

  * 'flat_epidermis_aff_def_tensor' is the 2D affine deformation tensor of the 
  epidermal wall obtained using its 3D landmarks (see 'epidermis_3DS_landmarks')
  projected in the rank-2 subspace of the wall (see 
  'epidermis_rank2_projection_matrix');

  * 'normed_stretch_2D' is the 2D stretch directions before and after the affine
  deformation of the wall (see 'flat_wall_aff_def_tensor');

  * 'normed_stretch_2D' is the 2D stretch directions before and after the affine
  deformation of the wall (see 'flat_wall_aff_def_tensor');

  * 'normed_stretch_2D_before' is the 2D stretch directions before the affine
  deformation of the wall (see 'flat_wall_aff_def_tensor');

  * 'epidermis_normed_stretch_2D_before' is the 2D stretch directions before the
  affine deformation of the epidermal wall (see 'flat_epidermis_aff_def_tensor');

  * 'epidermis_normed_stretch_2D_after' is the 2D stretch directions after the 
  affine deformation of the epidermal wall (see 'flat_epidermis_aff_def_tensor');

  * 'epidermis_normed_stretch_2D_after' is the 2D stretch directions after the 
  affine deformation of the epidermal wall (see 'flat_epidermis_aff_def_tensor');
"""
TEMPORAL_DIFF_FUNCTIONS = ['temporal_rate', 'log_temporal_rate',
                           'temporal_change', 'relative_temporal_change']

# - CELL level:
# -- Computable list of cell groups:
CELL_TP_LIST = []
# TODO: add 'dividing_cells', ...
# -- Computable scalars:
CELL_TP_SCALAR = ['volumetric_strain_rate', 'cell_strain_rate_anisotropy']
CELL_TP_SCALAR += zip_brackets(TEMPORAL_DIFF_FUNCTIONS, CELL_SCALAR)
# -- Computable vectors:
CELL_TP_COORDINATE = ['3D_landmarks']
CELL_TP_VECTOR = ['cell_stretch_ratio', 'strain_rate_3D']
# -- Computable tensors:
CELL_TP_TENSOR = ['cell_aff_def_tensor', 'normed_stretch_3D',
                  'normed_stretch_3D_before', 'normed_stretch_3D_after']

# - REQUIREMENTS:
CELL_TP_REQUIREMENTS = {
    "3D_landmarks": ['wall_median', 'epidermis_median'],
    "cell_aff_def_tensor": ['3D_landmarks'],
    "normed_stretch_3D": ['cell_aff_def_tensor'],
}

# - WALL level:
# :WARNING: there is no edge between L1 cells and the background, thus all epidermis related features are saved as vertex_properties (associated to the defined vertex)!
# -- Computable list of wall groups:
WALL_TP_LIST = ['division_wall']
# -- Computable scalars:
WALL_TP_SCALAR = ['areal_strain_rate', 'strain_rate_anisotropy_2D',
                  'expansion_anisotropy', 'epidermis_areal_strain_rate',
                  'epidermis_strain_rate_anisotropy_2D',
                  'epidermis_expansion_anisotropy']
# -- Computable vectors:
WALL_TP_COORDINATE = ['3DS_landmarks', 'epidermis_3DS_landmarks']
WALL_TP_VECTOR = ['stretch_ratios_2D', 'strain_rate_2D',
                  'epidermis_3DS_stretch_ratios_2D',
                  'epidermis_3DS_strain_rate_2D']
# TODO: fuse 'COORDINATE' & 'VECTOR' ?
# -- Computable tensors:
WALL_TP_TENSOR = ['wall_aff_def_tensor', 'flat_wall_aff_def_tensor',
                  'normed_stretch_2D', 'normed_stretch_2D_before',
                  'normed_stretch_2D_after', 'epidermis_aff_def_tensor',
                  'flat_epidermis_aff_def_tensor',
                  'epidermis_normed_stretch_2D',
                  'epidermis_normed_stretch_2D_before',
                  'epidermis_normed_stretch_2D_after']

# - REQUIREMENTS:
WALL_TP_REQUIREMENTS = {
    # -- Computable scalars:
    "areal_strain_rate": ['strain_rate_2D'],
    "strain_rate_anisotropy_2D": ['strain_rate_2D'],
    "expansion_anisotropy": ['strain_rate_2D'],
    "epidermis_areal_strain_rate": ['epidermis_strain_rate_2D'],
    "epidermis_strain_rate_anisotropy_2D": ['epidermis_strain_rate_2D'],
    "epidermis_expansion_anisotropy": ['epidermis_strain_rate_2D'],
    # -- Computable vectors:
    "3DS_landmarks": ['wall_median', 'wall_edge_median'],
    "epidermis_3DS_landmarks": ['epidermis_median', 'epidermis_edge_median'],
    # -- Computable tensors:
    "wall_aff_def_tensor": ['3DS_landmarks'],
    "epidermis_aff_def_tensor": ['epidermis_3DS_landmarks'],
    "flat_wall_aff_def_tensor": ['3DS_landmarks', 'rank2_projection_matrix'],
    "flat_epidermis_aff_def_tensor": ['epidermis_3DS_landmarks',
                                      'epidermis_rank2_projection_matrix'],
    "normed_stretch_2D": ['flat_wall_aff_def_tensor'],
    "epidermis_normed_stretch_2D": ['flat_epidermis_aff_def_tensor'],
    "normed_stretch_2D_before": ['flat_wall_aff_def_tensor'],
    "epidermis_normed_stretch_2D_before": ['flat_epidermis_aff_def_tensor'],
    "normed_stretch_2D_after": ['flat_wall_aff_def_tensor'],
    "epidermis_normed_stretch_2D_after": ['flat_epidermis_aff_def_tensor']
}

CELL_TEMPORAL_FEATURE = CELL_TP_LIST + CELL_TP_SCALAR + CELL_TP_COORDINATE + CELL_TP_VECTOR + CELL_TP_TENSOR
WALL_TEMPORAL_FEATURE = WALL_TP_LIST + WALL_TP_SCALAR + WALL_TP_COORDINATE + WALL_TP_VECTOR + CELL_TP_TENSOR
TEMPORAL_FEATURES = CELL_TEMPORAL_FEATURE + WALL_TEMPORAL_FEATURE


# TODO: SPATIAL_DIFF_FUNCTIONS could also apply to epidermal WALL_TP_SCALAR

# :TODO: is 3DS really useful ??
# TEMPORAL_SCALAR_FEATURES_3DS = ['expansion_anisotropy3DS',
#                                 'areal_strain_rate3DS',
#                                 'epidermal_strain_rate_anisotropy_3D',
#                                 'epidermal_growth_anisotropy_3D']


def not_bkgd(indices, bkgd):
    a, b = indices
    if a == bkgd:
        if b == bkgd:
            raise ValueError(indices)
        else:
            return b
    elif b == bkgd:
        return a
    else:
        raise ValueError(indices)


def label_neighbors(spia, label, labelset=None, min_area=5., real_area=True):
    """
    Get the neighbors of `label` from a SpatialImageAnalysis object.

    Parameters
    ----------
    spia: AbstractSpatialImageAnalysis
        AbstractSpatialImageAnalysis object build from an image;
    label: int
        label for which to get the neighbors
    labelset: set, optional
        restrict the returned list of neighbors to this set of labels
    min_area: float, optional
        area threshold to consider two cells as in contact, 4. by default
    real_area: bool, optional
        if True (default) the `min_area` is in real-unit, else in voxels

    Returns
    -------
    neighbors: set
        the set of `label` neighbors
    """
    try:
        assert isinstance(label, int)
    except AssertionError:
        raise TypeError("Parameter 'label' should be an integer")
    neighbors = spia.neighbors(label, min_area, real_area)
    if not labelset:
        return set(neighbors)
    else:
        return set(neighbors) & labelset


def get_labelpairs_from_neighbors(neighbors=None, bak_neigh=None,
                                  labels_w_undef_neigh=None, background=None):
    """
    Returns a list of wall defining label pairs from a `neighborhood` dict, and
    lists of neighbors to the background and undefined label.

    Parameters
    ----------
    neighbors: dict, optional
        dictionary of neighbors
    bak_neigh: list, optional
        list of background neighbors
    labels_w_undef_neigh: list, optional
        list of neighbors to undefined-value
    background: int, optional
        background label

    Returns
    -------
    list of unique sorted label-pairs from provided parameters
    """
    labelpairs = set([])
    if neighbors:
        nei_labelpairs = {stuple((l1, l2)) for l1, nei in neighbors.items() for
                          l2 in nei}
        labelpairs.update(nei_labelpairs)

    if bak_neigh:
        try:
            assert background is not None
        except AssertionError:
            raise ValueError(
                "Please specify 'background' if specifying 'bak_neigh' list!")
        bak_nei_labelpairs = {(background, nei) for nei in bak_neigh}
        labelpairs.update(bak_nei_labelpairs)

    if labels_w_undef_neigh:
        unlab_nei_labelpairs = {(0, nei) for nei in labels_w_undef_neigh}
        labelpairs.update(unlab_nei_labelpairs)

    return labelpairs


def topological_graph_from_neighborhood(neighborhood, marginal_labels=None,
                                        relabel=False):
    """
    Function generating a spatial graph ('PropertyGraph') based on a
    neighborhood dictionary.

    Parameters
    ----------
    neighborhood: dict
        dictionary giving neighbors of each label.
    marginal_labels: list, optional
        list of labels at the margins (stack edges) of the SpatialImage, will be
        used to save the complete topology, even at the edges of the tissue.
    relabel: bool, optional
        if True, default False, the returned topological graph (PropertyGraph)
        has relabelled ids starting at 0, else the image labels are kept.

    Returns
    -------
    graph: PropertyGraph
        the topological graph built from the neighborhood dictionary.

    Notes
    -----
    By default, we here keep the label ids from `neighborhood` instead of
    performing a "relabelling". It is thus easier to compare image labels and
    'PropertyGraph' vertex ids.
    If `marginal_labels` is not None, they are used as vertex and labelled as
    such in a boolean vertex_property 'marginal_labels'.
    """
    labels = neighborhood.keys()
    labelset = set(labels)
    # - Add the 'marginal_labels' to the labelset if any:
    if marginal_labels is not None:
        labelset = labelset | set(marginal_labels)

    # - Initialise an EMPTY PropertyGraph object:
    graph = PropertyGraph()
    # - Add vertex, with relabelling or not:
    if relabel:
        vertex2label = {graph.add_vertex(): l for l in labelset}
    else:
        vertex2label = {graph.add_vertex(l): l for l in labelset}
        # - Save the vertex to label map as a vertex property 'labels'
        graph.add_vertex_property('labels', vertex2label)
    # - Revert the vertex to label dictionary
    label2vertex = {v: k for k, v in vertex2label.items()}

    # - Add structural edges (topological relation):
    # Note that the edges are directed in "PropertyGraph" objects
    # To avoid duplication of topological information we only save for "source_id < target_id"
    for source, targets in neighborhood.items():
        if source in labelset:
            for target in targets:
                if source < target and target in labelset:
                    graph.add_edge(label2vertex[source],
                                   label2vertex[target])

    # - Finally add the 'marginal_labels' vertex property if given as an input parameter:
    if marginal_labels is not None:
        # Add the 'marginal_labels' as a boolean property in the graph:
        bool_ppty = {label2vertex[l]: l in marginal_labels for l in labels}
        graph.add_vertex_property('marginal_labels', bool_ppty)

    return graph


class TissueGraph3DBuilder(TissueGraph3D):
    """
    Class to create TissueGraph3D objects based on a SpatialImage.

    Analyse the labelled 'image' (`SpatialImageAnalysis`) to generate
    a topological graph containing 'labels' vertex property (`TissueGraph3D`).

    USAGE:
    ------
      * 'label': set it to define the list of labels for which to build the
      TissueGraph3D object and compute spatial properties. Possible string
      values are given in LABEL_STR, let it to None to use all labels found in
      the image.

    Notes:
    ------
      * to construct the graph we use the marginal labels, but we do not compute
      spatial feature values for them. We use them in the graph to preserve the
      topological information;
      * self._neighborhood is created from the list of labels (except the
      'marginal_labels' is 'labels' was let to None);
      * wall-based properties are computed for all neighbor-pairs;
      * epidermal wall-based properties are computed for all defined labels
       neighbor to the background;
    """

    def __init__(self, image, labels=None, real_units=True, **kwargs):
        """
        Parameters
        ----------
        images: SpatialImage
            labelled image used to construct the topological graph
        labels: None|list, optional
            if None (default), use all labels found within the 'image', else should
            be a list (of labels) used to limit the vertex included in the graph
            topology
        real_units: bool, optional
            if True (default), the area threshold 'min_area' is in real units, else
            is in voxels
        """
        # - Parse keyword arguments:
        self._parse_kwargs(real_units, **kwargs)

        # - Construct a `SpatialImageAnalysis` object from the labelled `image`:
        self._make_spatial_image_analysis(image)

        # - Detecting the cells at the edge of the 3D stack:
        self._marginal_labels = None
        # --> do NOT compute "cell properties" for those.
        # --> compute "wall properties" for label-pairs ('marginal_label', 'label').
        # --> compute "wall-edges properties" for label-triplet...
        self.get_marginal_labels(labels)
        # - Add them to the list of ignored labels before checking 'labels':
        self.analysis.add2ignoredlabels(self._marginal_labels)

        # - Handle 'labels' to make sure we don't have unknown values (to the image):
        print("\n-- Computing list of labels...")
        self._labels = self.analysis.labels_checker(labels)
        print("Done, found {} labels.".format(len(self._labels)))

        # - Initialise the TissueGraph3D object:
        print("\n-- Initialising the 'TissueGraph3D' object...")
        topo_graph = self.init_tissue_graph()
        try:
            img_name = self.analysis.filepath + self.analysis.filename
        except AttributeError:
            img_name = 'unknown_source'
        TissueGraph3D.__init__(self, topo_graph, image_path=img_name,
                               idgenerator='list', min_area=self.min_area,
                               real_min_area=self.real_min_area)
        # - Add computed vertex properties:
        # -- Add computed 'boundingboxes':
        print("--- Add vertex property 'boundingboxes'...")
        bbox_dict = self.analysis.boundingbox(real=False)
        self.add_vertex_ppty_from_label_dict('boundingboxes', bbox_dict)

        # -- Add computed 'wall_area' (when 'self.min_area' is not None or 0):
        if self.min_area is not None and self.min_area > 0:
            print("--- Add edge property 'wall_area'...")
            if self.real_units:
                lp_area = self.analysis._labelpair_areas
                lp_area_unit = 'µm²'
            else:
                lp_area = self.analysis._labelpair_areas_voxel
                lp_area_unit = 'voxels'

            self.add_edge_ppty_from_labelpair_dict('wall_area', lp_area)
            self.update_graph_property("units", lp_area_unit)

        # - Add important graph properties:
        # -- Add 'BuildDateTime' of the TissueGraph3D:
        self.add_graph_property('BuildDateTime', today_date_time())
        # -- Add 'AcquisitionDateTime' of the original intensity image:
        try:
            acquisition_date = self.analysis.image.metadata['Acquisition Date']
        except KeyError:
            acquisition_date = None
            print("No acquisition date defined!")
        self.add_graph_property('AcquisitionDateTime', acquisition_date)
        # TODO: add git version of code used to generate the graph?
        print("Done initialising the TissueGraph3D object from:\n\t{}\n".format(img_name))

        # - Initialise CELL LISTS attributes for features extraction:
        self._init_cell_list()
        # - Initialise WALL LISTS attributes for features extraction:
        self._init_wall_list()

        # - Extract spatial features if auto_make:
        if self._auto_make:
            self.feature_extraction(self.features2compute)

    def __str__(self):
        """
        """
        print("Initialized a TissueGraph3DBuilder.")
        img_fname = self.analysis.filepath + self.analysis.filename
        print("Used labelled image is named: {}".format(img_fname))

        s = "Generated the following TissueGraph3D:"
        if self.time_point is None:
            s += ":"
        else:
            s += " from image No.{}:".format(self.time_point)
        print(s, "\n", TissueGraph3D.__str__(self))

    def _init_cell_list(self):
        """
        Initialise CELL LISTS attributes for features extraction:
          * self.layer1: labels in the L1 layer (epidermis)
          * self.layer2: labels neighbors to the L1
          * self.labels_w_undef_neigh: labels neighbors to the 'self._no_label_id'
        """
        # -- List of labels neighbors of the background (L1/epidermis)
        self.layer1 = None
        if self.background is not None:
            print("-- Computing list of L1-labels...")
            bak_neigh = self.analysis.cell_first_layer(self.min_area,
                                                       self.real_min_area)
            self.layer1 = list(set(bak_neigh) & set(self._labels))
            print("Found {} L1-labels.".format((len(self.layer1))))
        else:
            self.layer1 = []
            print("Found no L1-labels, since no background label is defined.")
        # -- List of labels neighbors of the L1 (L2/second layer)
        self.layer2 = None
        # -- List of labels neighbors of the undefined value (0)
        no_label_id = self.analysis.image._no_label_id
        print("-- Computing list of labels neighbors to the undefined label {}...".format(no_label_id))
        if no_label_id in self.analysis.image:
            self.labels_w_undef_neigh = label_neighbors(self.analysis,
                                                        no_label_id,
                                                        set(self._labels),
                                                        self.min_area,
                                                        self.real_min_area)
            print("Found {} labels in contact with undefined label (ie. {} value)".format(len(self.labels_w_undef_neigh), no_label_id))
        else:
            self.labels_w_undef_neigh = []
            print("Could not find the undefined label in the labelled image (ie. {} value).".format(no_label_id))

        return

    def _init_wall_list(self):
        """
        Initialise WALL LISTS attributes for features extraction:
          * self.labelpairs: all label-pairs
          * self.epidermis_anticlinal_edge_labelpairs
        """
        self.labelpairs = None
        # -- Get the list of all walls as label-pairs tuple:
        if self.features2compute is not None:
            self.list_all_labelpairs()
        # -- List of epidermal anticlinal walls as label-pairs tuple:
        self.epidermis_anticlinal_edge_labelpairs = None
        if self.features2compute is not None:
            self.list_epidermis_anticlinal_edge_labelpairs()

        return

    def save(self, filename):
        """
        Save the TissueGraph3DBuilder object under 'filename'.

        Parameters
        ----------
        filename: str
            name of the file to load.

        Notes
        -----
        The file is compressed by the LZ4 algorithm
        """
        from tissue_analysis.compression import Compress
        Compress(filename, method=None).write(self)

        return

    @staticmethod
    def load(filename):
        """
        Load a TissueGraph3DBuilder from the given filename.

        Parameters
        ----------
        filename: str
            name of the file to load.
        """
        from tissue_analysis.compression import Compress
        obj = Compress(filename).open()
        if isinstance(obj, TissueGraph3DBuilder):
            return obj
        else:
            print("Object within file '{}' is not a TissueGraph3DBuilder!")
            return

    def get_TissueGraph3D(self):
        """
        Return a TissueGraph3D object from the TissueGraph3DBuilder.

        Returns
        -------
        tissue_graph: TissueGraph3D
            the TissueGraph3D object related to this object
        """
        return TissueGraph3D(self, image_path=self.graph_property('image_path'))

    def get_filename(self, with_path=True):
        """
        Create predefined filename, useful for auto save and load, notably for
        the TissueGraph4DBuilder.

        Parameters
        ----------
        with_path: bool, optional
            add the filepath to the returned filename

        Returns
        -------
        fname: str
            the standardized filename of the object
        """
        if self.fname is None:
            if with_path:
                fname = self.analysis.filepath + self.analysis.filename
            else:
                fname = self.analysis.filename
        else:
            fname = self.fname
        fname += "_TissueGraph3D{}.pkl".format(self.fname_suffix)
        return fname

    def list_all_labelpairs(self):
        """
        List all the known label-pair tuple and return them pair-sorted.
        """
        if self.labelpairs is None:
            print("-- Computing list of wall labelpairs...")
            self.labelpairs = get_labelpairs_from_neighbors(self._neighborhood)
            # TODO: should probably rather use the SpatialImageAnalysis function?
            # self.labelpairs = self.analysis.list_all_walls(min_area=self.min_area, real_area=self.real_min_area)
            print("Got {} labelpairs!".format(len(self.labelpairs)))

        return self.labelpairs

    def list_epidermis_anticlinal_edge_labelpairs(self):
        """
        List all known label-pair tuple defining a wall-edge at the epidermis.
        """
        self.list_all_labelpairs()

        if self.epidermis_anticlinal_edge_labelpairs is None:
            print("-- Computing list of epidermis anticlinal wall edge labelpairs...")
            # - Epidermal edge label-pair have the two non-background neighbors in the L1 (thus neighbors of the background)
            edge_labelpairs = [stuple((lab1, lab2)) for (lab1, lab2) in
                               self.labelpairs if
                               lab1 in self.layer1 and lab2 in self.layer1]
            # TODO: should probably rather use the SpatialImageAnalysis function?
            print("Got {} epidermal edge labelpairs.".format(len(edge_labelpairs)))
            self.epidermis_anticlinal_edge_labelpairs = edge_labelpairs

        return self.epidermis_anticlinal_edge_labelpairs

    def init_tissue_graph(self):
        """
        Build the TissueGraph3D object from the given labelled image.
        """
        # - Get the neighborhood info given 'self._labels' & 'self.min_area':
        print("-- Computing the 'neighborhood' dictionary...")
        neighborhood = self.analysis.neighbors(self._labels, self.min_area,
                                               self.real_units, n_jobs=4)
        self._neighborhood = {k: v for k, v in neighborhood.items() if v != []}
        print("Done, got the 'neighborhood' of {} labels!".format(len(self._neighborhood)))

        # - Make the topological graph from neighborhood dict and marginal labels:
        print("-- Computing the 'topological graph' PropertyGraph...")
        topo_graph = topological_graph_from_neighborhood(self._neighborhood,
                                                         self._marginal_labels,
                                                         relabel=False)

        print("Done, got the following"),
        print(topo_graph.__str__())

        return topo_graph

    def get_spatial_image_analysis(self):
        """
        Returns the SpatialimageAnalysis object.
        """
        return self.analysis

    def _make_spatial_image_analysis(self, image):
        """
        Create the SpatialImageAnalysis from the given 'image'.

        Parameters
        ----------
        image: str|SpatialImage
            filepath as str or SpatialImage
        """
        # If defined, use 'self.time_point' value for fancy printing:
        if self.time_point is None:
            print("\n# - Initialising SpatialImageAnalysis...")
        else:
            print("\n# - Initialising SpatialImageAnalysis No.{}...".format(self.time_point))

        if isinstance(image, str):
            image = imread(image)
            self.analysis = SpatialImageAnalysis(image, return_type=DICT,
                                                 background=self.background)
        elif isinstance(image, SpatialImage):
            self.analysis = SpatialImageAnalysis(image, return_type=DICT,
                                                 background=self.background)
        else:
            raise ValueError(
                "Unknown type for input image, should be a string or a SpatialImage")
        return

    def _check_spatial_feature_type(self):
        """
        Check that self.features2compute is a list of str.
        """
        try:
            assert isinstance(self.features2compute, list)
            assert np.alltrue(
                [isinstance(f, str) for f in self.features2compute])
        except AssertionError:
            raise ValueError(
                "Keyword argument 'spatial_features' should be a list of str")
        return

    def _check_local_curvature_sampling_radius(self):
        """
        Used to manage the list of sampling radius provided when asking for
        principal curvature computation.
        """
        sampling_radius = self.local_curvature_sampling_radius
        try:
            assert isinstance(sampling_radius, list)
        except AssertionError:
            raise TypeError(
                "Parameter `local_curvature_sampling_radius` should be a list!")
        try:
            assert np.alltrue(
                [isinstance(radius, float) or isinstance(radius, int) for radius
                 in sampling_radius])
        except AssertionError:
            raise TypeError(
                "Parameter `local_curvature_sampling_radius` should be a list of float or integer, got {}!".format(
                    [type(radius) for radius in sampling_radius]))
        print("Got an initial list of sampling radius for local curvature estimation: {}".format(sampling_radius))
        return

    def list_cell_spatial_feature(self):
        """
        Return available cell-based properties to be computed by TissueGraph3DBuilder.
        """
        return CELL_SPATIAL_FEATURE

    def list_wall_spatial_feature(self):
        """
        Return available wall-based properties to be computed by TissueGraph3DBuilder.
        """
        return WALL_SPATIAL_FEATURE

    def list_spatial_feature(self):
        """
        Return all available properties to be computed by TissueGraph3DBuilder.
        """
        return SPATIAL_FEATURES

    def _parse_kwargs(self, real_units, **kwargs):
        """
        Use kwargs to define several attributes related to the way the graphs
        are build, the units, registration and other values.

        Parameters
        ----------
        real_units: bool
            if True, the computed spatio-temporal properties are returned in real-world units, else in voxels

        **kwargs
        --------
        # -- Labelled image attributes:
        background: int, (default is None)
            if any, the background id to be found within the
            labelled SpatialImage

        # -- Temporal related attributes:
        time_point: int, (default is None)
            if any, the time-point id of the image within its time series
            (eg. 1 for the second image of a series of 5)
        time_unit: str (default=None)
            indicate the time unit (eg. 'h', 'min', ...) of the time-intervals,
            to be used in later feature plots

        # -- TissueGraph3D creation attributes:
        min_area: float (default=None)
            area threshold to consider two cells as in contact,
        real_min_area: bool (default=real_units)
            define if the `min_area` kwarg should be in real-unit or
            voxels, by default it is set to `real_units`
        detect_marginal_cells: bool (default=False)
            if True (default=False) the cells at the stacks margins will be
            removed before constructing the graph
        auto_make: bool (default=False)
            if True, the TissueGraph3D object is built during initialisation and
            declared `spatial_features` will be computed

        # -- Features computation related attributes:
        spatial_features: list(str) (default=None)
            list of spatial features to compute, use
            self.available_spatial_features() to list those available
        local_curvature_sampling_radius: list(int|float) (default=None)
            list of sampling radius used to performs a local estimation of the
            epidermis' curvature (first layer of voxel in contact with the
            background)
        """
        # - Parsing kwargs and defining attributes:
        # TODO: remove 'real_units' and compute in voxel units!
        # TODO: TissueGraph3D should store ppty in voxel and return the values in real world if required (for example using a 'real=True' param with vertex_property)
        self.real_units = real_units

        # -- Labelled image attributes:
        # TODO: get it from the SpatialImage (should be defined as an attribute)
        self.background = kwargs.get('background', None)

        # -- Temporal related attributes:
        self.time_point = kwargs.get('time_point', None)
        self.time_unit = kwargs.get('time_unit', None)
        self.fname_suffix = kwargs.get('fname_suffix', "")

        # -- TissueGraph3D creation attributes:
        self.min_area = kwargs.get('min_area', None)
        self.real_min_area = kwargs.get('real_min_area', real_units)
        self.detect_marginal_cells = kwargs.get('detect_marginal_cells', False)
        self._auto_make = kwargs.get('auto_make', False)
        if self._auto_make:
            print("Auto build enabled!")
        self.fname = kwargs.get('fname', None)

        # -- Features computation related attributes:
        # Performs feature check:
        self._spatial_features_check(**kwargs)
        # If we have a list of feature but the 'auto_make=False' was not specified we enable auto_make:
        if self.features2compute is not None and not kwargs.has_key(
                'auto_make'):
            self._auto_make = True

        return

    def _spatial_features_check(self, **kwargs):
        """
        Performs all the necessary check for the spatial feature list.

        Returns
        -------
        self.features2compute: list
            the list of features to compute
        """
        self.local_curvature_sampling_radius = kwargs.get(
            'local_curvature_sampling_radius', None)
        # If 'local_curvature_sampling_radius' is defined, we have to check this list
        if self.local_curvature_sampling_radius is not None:
            self._check_local_curvature_sampling_radius()
        self.features2compute = kwargs.get('spatial_features', None)
        # If 'spatial_features' is defined, we have to check this list
        if self.features2compute is not None:
            # -- Computing 'epidermis_local_principal_curvature' requires the definition of a list of sampling radius as kwarg:
            if 'epidermis_local_principal_curvature' in self.features2compute and self.local_curvature_sampling_radius is None:
                print("Found 'epidermis_local_principal_curvature' in list of spatial feature to compute.")
                raise ValueError(
                    "But you did not specify the keyword argument 'local_curvature_sampling_radius' (list).")
            # -- Check the types in the list of spatial feature:
            self._check_spatial_feature_type()
            # -- Check the features requirements are met:
            self._check_feature_requirements()
            # -- Check the features are available to computation:
            self._check_against_available_features()
            # -- Check against potential known features (vertex, egde & graph) in the TissueGraph3D:
            if hasattr(self,
                       '_vertex_property'):  # False if initialisation step!
                self._check_known_graph_features()
        else:
            print("No list of spatial features!")
        return self.features2compute

    def _check_iso_voxelsize(self):
        # - Check we have isometric voxelsize:
        vxs = self.analysis.image.voxelsize
        if not np.allclose(vxs, vxs[0]):
            warnings.warn('Image voxelsize is not isometric !!!')
        return "Done checking voxelsize isometry."

    def is_available_feature(self, feature):
        """
        Check if the given 'feature' is accessible to computation.

        Parameters
        ----------
        feature: str
            feature name to test against those defined in
            'self.available_spatial_feature()'

        Returns
        -------
        known_feature: bool
            True if it can be computed and added to the TissueGraph3D object,
            else False
        """
        return feature in self.list_spatial_feature()

    def _check_known_curvature_sampling(self):
        """
        Handles the local curvature sampling radius given to self.local_curvature_sampling_radius
        against already computed values in the TissueGraph3D object.

        Notes
        -----
        self.local_curvature_sampling_radius: list
            contains list of sampling radius that should be computed by `_sp_add_epidermis_local_pc`
        graph_property('sampling_radius_principal_curvature'): list
            contains list of smapling radius already used to performs local
            estimation of curvature (eg. [70])
        """
        # - Get the list of sampling radiuses to compute:
        radius = set(self.local_curvature_sampling_radius)
        # - Try to find existing 'sampling_radius_principal_curvature' in the graph
        try:
            existing_radius = set(
                self.graph_property(
                    'sampling_radius_principal_curvature'))
        except:
            existing_radius = set([])

        # - Remove existing (already computed) sampling radiuses from the list to compute:
        radius = list(set(radius) - existing_radius)
        # - Update the list of radius to compute (might be empty):
        self.local_curvature_sampling_radius = radius

        # - If the list of radius to compute is empty, remove 'epidermis_local_principal_curvature' from the list of spatial feature to compute:
        if self.local_curvature_sampling_radius == set([]):
            print("Could not detect NEW list of radius for the local estimation of curvature for epidermal walls.")
            self.features2compute.remove('epidermis_local_principal_curvature')
            print("Property 'epidermis_local_principal_curvature' has been removed from the list of spatial features to compute!")
        else:
            print("Got a list of sampling radius for the local estimation of curvature for epidermal walls: {}.".format(radius))
        return

    def set_local_curvature_sampling_radius(self, sampling_radiuses):
        """
        Add the given list of `sampling_radiuses` to the .
        """
        if isinstance(sampling_radiuses, int):
            sampling_radiuses = [sampling_radiuses]

        radius = set(self.local_curvature_sampling_radius)
        radius += set(sampling_radiuses)
        self.local_curvature_sampling_radius = radius
        self._check_known_curvature_sampling()
        return "Done."

    def _check_feature_requirements(self):
        """
        Function used to check the requirements of some of the 'spatial_features'.
        """
        print("\n# - Checking feature requirements:")
        # - Use 'CELL_REQUIREMENTS' & 'WALL_REQUIREMENTS' to check requirements
        # of features to compute:
        n_max = 10000
        n_feat = 0
        n = 0
        while len(
                self.features2compute) != n_feat:  # to allow to check requirements of the requirements themselves!
            n_feat = len(self.features2compute)
            for feat in self.features2compute:
                if CELL_REQUIREMENTS.has_key(feat):
                    self.features2compute.extend(CELL_REQUIREMENTS[feat])
                if WALL_REQUIREMENTS.has_key(feat):
                    self.features2compute.extend(WALL_REQUIREMENTS[feat])
            # - Remove possible duplicates:
            self.features2compute = list(set(self.features2compute))
            if n == n_max - 1:
                print(self.features2compute)
                raise ValueError(
                    "Infinite loop, check 'CELL_REQUIREMENTS' & 'WALL_REQUIREMENTS' definitions!")
            else:
                n += 1

        if n - 1 != 0:
            print("Made {} iterations of feature requirements checks!".format(n - 1))

        # - Print about selected properties to compute:
        feat_list = self.features2compute
        print("List of propert{} to compute:".format("ies" if len(feat_list) > 1 else "y"))
        print('  - ' + '\n  - '.join(sorted(feat_list)))
        # - Print about selected local sampling radius to compute:
        rad = self.local_curvature_sampling_radius
        try:
            print("Selected radius{} to compute: {}".format("es" if len(rad) > 1 else "", rad))
        except:
            pass

        return

    def _check_against_available_features(self):
        """
        Check the list of features to compute is available for computation.

        Returns
        -------
        Nothing, edit the list of features to compute `self.features2compute`
        """
        print("\n# - Checking against list of available features:")
        feat_list = [f for f in self.features2compute if
                     self.is_available_feature(f)]
        if feat_list:
            print("List of propert{} to compute:".format("ies" if len(feat_list) > 1 else "y"))
            print('  - ' + '\n  - '.join(sorted(feat_list)))
        else:
            print("No property left!")
        self.features2compute = feat_list
        return

    def _check_known_graph_features(self):
        """
        Check the list of features to compute is not already defined in the
        TissueGraph

        Returns
        -------
        Nothing, edit the list of features to compute `self.features2compute`
        """
        print("\n# - Checking against known graph features:")
        vtx_ppties = sorted(self.vertex_property_names())
        edge_ppties = sorted(self.edge_property_names())
        graph_ppties = sorted(self.graph_property_names())
        all_ppties = vtx_ppties + edge_ppties + graph_ppties

        ppties2rm = []
        for f in self.features2compute:
            if f in all_ppties:
                ppties2rm.append(f)

        if ppties2rm != []:
            for f in ppties2rm:
                self.features2compute.remove(f)

        return

    def feature_extraction(self, spatial_features,
                           local_curvature_sampling_radius=None):
        """
        Compute and add the listed features in 'spatial_features' to the
        TissueGraph3D object.

        Parameters
        ----------
        spatial_features: str|list
            single or list of name of spatial feature to compute and add to the tissue
            graph object
        local_curvature_sampling_radius: list(int), optional
            list of local sampling radius (in VOXEL), should be specified when
            using 'epidermis_local_principal_curvature'

        Notes
        -----
        The available features are listed by types under:
          * CELL_LIST: list of cell groups
          * CELL_SCALAR: cell-based scalar features
          * CELL_COORDINATE: features related to cells position in the tissue
          * CELL_TENSOR:
          * WALL_SCALAR: wall-based scalar features
          * WALL_COORDINATE: features related to walls position in the tissue
          * WALL_TENSOR:
        ALL available SPATIAL features are listed under: SPATIAL_FEATURES
        """
        if isinstance(spatial_features, str):
            spatial_features = list(spatial_features)
        # - Define the list of feature(s) to compute in `self.features2compute`
        spatial_features = self._spatial_features_check(
            spatial_features=spatial_features,
            local_curvature_sampling_radius=local_curvature_sampling_radius)

        print("\n# - Spatial feature extraction:")
        #  - Now compute and add features in 'spatial_features' to the TissueGraph 'self.graph'
        ## - CELL LEVEL:
        ## -- CELL_COORDINATE:
        if 'boundingbox' in spatial_features:
            print("\n- Computing 'boundingbox' property...")
            self.add_boundingbox()
            self.features2compute.remove('boundingbox')
        if 'barycenter' in spatial_features:
            print("\n- Computing 'barycenter' property...")
            self.add_barycenter()
            self.features2compute.remove('barycenter')

        ## -- CELL_LIST:
        if 'marginal_labels' in spatial_features:
            print("\n- Computing 'marginal_labels' property...")
            self.add_marginal_labels()
            self.features2compute.remove('marginal_labels')
        if 'L1' in spatial_features:
            print("\n- Computing 'L1' property...")
            self.add_L1()
            self.features2compute.remove('L1')
        if 'L2' in spatial_features:
            print("\n- Computing 'L2' property...")
            self.add_L2()
            self.features2compute.remove('L2')

        ## -- CELL_SCALAR:
        if 'area' in spatial_features:
            print("\n- Computing 'area' property...")
            print(NotImplementedError("But please, be my guest!"))
        if 'volume' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'volume' property...")
            self.add_volume()
            self.features2compute.remove('volume')
        if 'neighbors_count' in spatial_features:
            print("\n- Computing 'neighbors_count' property...")
            print(NotImplementedError("But please, be my guest!"))
        if 'shape_anisotropy_3D' in spatial_features:
            print("\n- Computing 'shape_anisotropy_3D' property...")
            print(NotImplementedError("But please, be my guest!"))

        ## -- CELL_TENSOR:
        if 'inertia_tensor_3D' in spatial_features:
            print("\n- Computing 'inertia_tensor_3D' property...")
            self.add_inertia_tensor_3d()
            self.features2compute.remove('inertia_tensor_3D')

        ## - WALL LEVEL:
        ## -- WALL_COORDINATE:
        if 'wall_median_voxel' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'wall_median_voxel' property...")
            self.add_wall_median_voxel()
            self.features2compute.remove('wall_median_voxel')
        if 'wall_median' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'wall_median' property...")
            self.add_wall_median()
            self.features2compute.remove('wall_median')
        if 'wall_edge_median' in spatial_features:
            print("\n- Computing 'wall_edge_median' property...")
            print(NotImplementedError("But please, be my guest!"))
        if 'wall_edge_median_voxel' in spatial_features:
            print("\n- Computing 'wall_edge_median_voxel' property...")
            print(NotImplementedError("But please, be my guest!"))
        if 'epidermis_median_voxel' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'epidermis_median_voxel' property...")
            self.add_epidermis_median_voxel()
            self.features2compute.remove('epidermis_median_voxel')
        if 'epidermis_median' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'epidermis_median' property...")
            self.add_epidermis_median()
            self.features2compute.remove('epidermis_median')
        if 'epidermis_edge_median' in spatial_features:
            print("\n- Computing 'epidermis_edge_median' property...")
            print(NotImplementedError("But please, be my guest!"))
        if 'epidermis_edge_median_voxel' in spatial_features:
            print("\n- Computing 'epidermis_edge_median_voxel' property...")
            print(NotImplementedError("But please, be my guest!"))

        ## -- WALL_SCALAR:
        if 'wall_area' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'wall_area' property...")
            self.add_wall_area()
            self.features2compute.remove('wall_area')
        if 'epidermis_area' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'epidermis_area' property...")
            self.add_epidermis_area()
            self.features2compute.remove('epidermis_area')

        ## -- WALL_TENSOR:
        if 'wall_orientation' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'wall_orientation' property...")
            self.add_wall_orientation()
            self.features2compute.remove('wall_orientation')
        if 'rank2_projection_matrix' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'rank2_projection_matrix' property...")
            self.add_rank_2_projection_matrix()
            self.features2compute.remove('rank2_projection_matrix')
        if 'epidermis_rank2_projection_matrix' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'epidermis_rank2_projection_matrix' property...")
            self.add_epidermis_rank_2_projection_matrix()
            self.features2compute.remove('epidermis_rank2_projection_matrix')
        if 'wall_principal_curvatures' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'wall_principal_curvatures' property...")
            try:
                self.add_wall_principal_curvatures()
            except:
                print("WARNING: Error during 'wall_principal_curvatures' computation...\n")
            else:
                self.features2compute.remove('wall_principal_curvatures')
        if 'wall_principal_curvatures' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'wall_principal_curvatures' property...")
            try:
                self.add_epidermis_principal_curvatures()
            except:
                print("WARNING: Error during 'wall_principal_curvatures' computation...\n")
            else:
                self.features2compute.remove('wall_principal_curvatures')
        # TODO: move gaussian, ratio & mean curvature from ExtraFeature here

        if 'epidermis_local_principal_curvature' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'epidermis_local_principal_curvature' property...")
            self.add_epidermis_local_pc()
            # TODO: move gaussian, ratio & mean curvature from ExtraFeature here
            self.features2compute.remove('epidermis_local_principal_curvature')

        ## - WALL-EDGE LEVEL:
        if 'epidermis_edge_median_voxel' in spatial_features and self.analysis.is3D():
            print("\n- Computing 'epidermis_edge_median_voxel' property...")
            self.add_epidermis_edge_median_voxel()
            self.features2compute.remove('epidermis_edge_median_voxel')

    ##------------------------------------------------------------------------------
    ## SUB-FUNCTIONS FOR 'SPATIAL_PROPERTIES':
    ##------------------------------------------------------------------------------

    ## LABEL BASED PROPERTIES:
    ##------------------------------------------------------------------------------
    def add_boundingbox(self):
        """
        Add boundingboxes to the TG, as detected by SpatialImageAnalysis.

        Returns
        -------
        Nothing, add to the object
        """
        bbox = self.analysis.boundingbox(self._labels, real=False)
        unit = {"boundingbox": 'voxels'}
        self.extend_vertex_ppty_from_label_dict('boundingbox', bbox)
        self.update_graph_property("units", unit)
        print("Done adding 'boundingbox' as vertex property.")
        return

    def add_volume(self):
        """
        Add volume to the TG, as detected by SpatialImageAnalysis.

        Returns
        -------
        Nothing, add to the object
        """
        volume = self.analysis.volume(self._labels, real=self.real_units)
        unit = {"volume": (u'\u03BCm\u00B3' if self.real_units else 'voxels')}
        self.extend_vertex_ppty_from_label_dict('volume', volume)
        self.update_graph_property("units", unit)
        return

    def add_barycenter(self):
        """
        Add barycenter to the TG, as detected by SpatialImageAnalysis.
        VOXEL units.
        """
        bary = self.analysis.center_of_mass(self._labels, real=False)
        unit = {"barycenter": 'voxels'}
        self.extend_vertex_ppty_from_label_dict('barycenter', bary)
        self.update_graph_property("units", unit)
        return

    def get_L1(self):
        """
        Get the list of cells belonging to the first layer (epidermis) of the
        biological object.

        Returns
        -------
        self.layer1: list
            the list of cell in contact with the background label
        """
        if self.layer1 is None:
            self.layer1 = self.analysis.cell_first_layer(self.min_area,
                                                         self.real_min_area)
        return self.layer1

    def add_L1(self):
        """
        Add the list of vertex belonging to the first layer to the TG, as detected
        by SpatialImageAnalysis.
        """
        layer1 = self.get_L1()
        layer1 = {l: l in layer1 for l in self._labels}
        self.extend_vertex_ppty_from_label_dict('L1', layer1)
        return

    def add_L2(self):
        """
        Add the list of vertex belonging to the second layer to the TG, as detected
        by SpatialImageAnalysis.
        
        """
        if self.layer2 is None:
            self.layer2 = self.analysis.cell_second_layer(self.min_area,
                                                          self.real_min_area)

        l2 = {l: l in self.layer2 for l in self._labels}
        self.extend_vertex_ppty_from_label_dict('L2', l2)
        return

    def get_marginal_labels(self, labels=None):
        """
        Compute the list of marginal labels.

        Parameters
        ----------
        labels: None|list|str, optional
            list of labels around which the margin should be defined

        Returns
        -------
        marginal_labels: list
            list of marginal labels
        """
        if self._marginal_labels is None:
            print("-- Computing list of 'marginal_labels'"),
            if labels is not None:
                tmp_labels = self.analysis.labels_checker(labels)
                print("around a group of {} labels...".format(len(tmp_labels)))
                tmp_nei = self.analysis.neighbors(tmp_labels,
                                                  min_area=self.min_area,
                                                  real_area=self.real_min_area)
                # - Marginal labels are the neigbhors of 'labels' iunfound in this list:
                self._marginal_labels = [nei for nei_list in tmp_nei.values()
                                         for nei in nei_list if
                                         nei not in labels]
            else:
                print("at the edges of the stack...")
                self._marginal_labels = self.analysis.labels_at_stack_margins()
            print("Done, found {} 'marginal_labels'.".format(len(self._marginal_labels)))
        return self._marginal_labels

    def add_marginal_labels(self):
        """
        Add cells in contact with the edges of the image.
        """
        self.get_marginal_labels()
        border_cells = {l: l in self._marginal_labels for l in self._labels}
        self.extend_vertex_ppty_from_label_dict('marginal_labels',
                                                border_cells)
        return

    def add_inertia_tensor_3d(self):

        """
        Add cells' inertia_tensor_3D, as detected by SpatialImageAnalysis.
        """
        inertia_tensor_3d, inertia_values_3d = self.analysis.inertia_tensor_3d(
            self._labels, real=self.real_units)
        # TODO: Check the 'inertia_tensor_3d' function returns 'inertia_values' in units requested by `real_ppty`!
        self.extend_vertex_ppty_from_label_dict('inertia_tensor_3D',
                                                inertia_tensor_3d)
        self.extend_vertex_ppty_from_label_dict('inertia_values_3D',
                                                inertia_values_3d)
        # TODO: Add units for inertia tensor norms?
        return

    ## WALL BASED PROPERTIES:
    ##------------------------------------------------------------------------------
    def get_wall_area(self, labelpairs=None):
        """
        Compute the wall area for defined 'labelpairs' if not None, else for
        'self.labelspairs'.

        Parameters
        ----------
        labelpairs: list(tuple), optional
            if None (default), use 'self.labelspairs', else should be a list of
            length-2 tuples defining pair of labels

        Returns
        -------
        wall_area: dict
            the labelpair dictionary containing the wall area
        """
        if labelpairs is None:
            labelpairs = self.labelpairs

        print("Got {} wall areas to compute...".format(len(labelpairs)))
        wall_areas = self.analysis.wall_area_from_labelpairs(labelpairs,
                                                             real=self.real_units)
        print("Done.")
        return wall_areas

    def add_wall_area(self):
        """
        Add the wall area to the TissueGraph3D object:
          * edge_property: 'wall_area'
          * vertex_property: 'epidermis_area' & unlabelled_wall_area
        """
        wall_areas = self.get_wall_area()
        epidermis_area = {lab2: v for (lab1, lab2), v in wall_areas.items() if
                          lab1 == self.background}
        unlabelled_wall_area = {lab2: v for (lab1, lab2), v in
                                wall_areas.items() if
                                lab1 == self.analysis._no_label_id}

        # - Add those values to the TissueGraph3D:
        unit = {
            "wall_area": (u'\u03BCm\u00B2' if self.real_units else 'voxels')}
        self.extend_edge_ppty_from_labelpair_dict('wall_area', wall_areas)
        self.update_graph_property("units", unit)

        # - Take care of the 'background' neighbors:
        if epidermis_area:
            unit = {"epidermis_area": (
                u'\u03BCm\u00B2' if self.real_units else 'voxels')}
            self.extend_vertex_ppty_from_label_dict('epidermis_area',
                                                    epidermis_area)
            self.update_graph_property("units", unit)

        # - Take care of the 'undefined values' neighbors:
        if unlabelled_wall_area:
            unit = {"unlabelled_wall_area": (
                u'\u03BCm\u00B2' if self.real_units else 'voxels')}
            self.extend_vertex_ppty_from_label_dict('unlabelled_wall_area',
                                                    unlabelled_wall_area)
            self.update_graph_property("units", unit)

        return

    def add_epidermis_area(self):
        """
        Add the epidermal wall area to the TissueGraph3D object:
          * vertex_property: 'epidermis_area'
        """
        wall_areas = self.get_wall_area(
            labelpairs=self.analysis.list_epidermal_walls(
                min_area=self.min_area, real_area=self.real_min_area))
        epidermis_area = {lab2: v for (lab1, lab2), v in wall_areas.items() if
                          lab1 == self.background}

        # - Take care of the 'background' neighbors:
        if epidermis_area:
            unit = {"epidermis_area": (
                u'\u03BCm\u00B2' if self.real_units else 'voxels')}
            self.extend_vertex_ppty_from_label_dict('epidermis_area',
                                                    epidermis_area)
            self.update_graph_property("units", unit)

        return

    def _compute_wall_median_voxel(self, labelpairs=None, real=False):
        """
        Compute the coordinate in voxel units (real=False) of the wall median,
        based on a list of wall defining 'labelpairs'.

        Parameters
        ----------
        labelpairs: list, optional
            list of wall-defining labelpairs for which to compute the wall
            median coordinate;
        real: bool, optional
            if False (default), return the coordinates in voxels, else in real
            units;

        Returns
        -------
        wall_median_voxel: dict
            labelpair dictionary of the walls median coordinate;
        epidermis_median_voxel: dict
            labelpair dictionary of the epidermal wall median coordinate;
        unlabelled_wall_median_voxel: dict
            labelpair dictionary of the marginal wall median coordinate;
        """
        if labelpairs is None:
            labelpairs = self.labelpairs
        print("-- Searching for {} wall median voxel coordinates".format(len(labelpairs)))
        # We separate the wall medians into 3 different 'type of wall':
        #  - "regular": 2 cells in contact
        #  - "epidermal": one cell in contact with the 'background' value
        #  - "unlabelled": one cell in contact with the 'erase' value (deleted cells)
        wall_median_voxel = {}
        epidermis_median_voxel = {}
        unlabelled_wall_median_voxel = {}
        no_wmv = []
        for label_1, label_2 in labelpairs:
            # label_1, label_2 = stuple((label_1, label_2))
            slp = stuple((label_1, label_2))
            if slp in wall_median_voxel or slp[
                1] in epidermis_median_voxel or \
                    slp[1] in unlabelled_wall_median_voxel:
                continue  # already computed!
            wmw = self.analysis.wall_median_from_labelpair(label_1, label_2,
                                                           real=real,
                                                           min_area=self.min_area,
                                                           real_area=self.real_min_area)
            if wmw is None:  # from min_area filtering!
                no_wmv.append(slp)
                continue
            if slp[0] == self.analysis.image._no_label_id:
                # no need to check `label_2` because labels are sorted
                unlabelled_wall_median_voxel[slp[1]] = wmw
            elif slp[0] == self.background:
                # no need to check `label_2` because labels are sorted
                epidermis_median_voxel[slp[1]] = wmw
            else:
                wall_median_voxel[slp] = wmw

        if len(no_wmv) != 0:
            print("Could not compute the wall median voxel for {} labelpairs:\n{}".format(len(no_wmv), no_wmv))

        return wall_median_voxel, epidermis_median_voxel, unlabelled_wall_median_voxel

    def add_wall_median(self):
        """
        Add/update the real coordinates of the wall medians to/of the
        TissueGraph3D object:
          * vertex_property: 'epidermis_median', 'unlabelled_wall_median'
          * edge_property: 'wall_median'
        """
        wmv, epidermal_wmv, unlabelled_wmv = self._compute_wall_median_voxel(
            real=True)
        self.extend_edge_ppty_from_labelpair_dict('wall_median', wmv)
        if epidermal_wmv:
            self.extend_vertex_ppty_from_label_dict('epidermis_median',
                                                    epidermal_wmv)
        if unlabelled_wmv:
            self.extend_vertex_ppty_from_label_dict('unlabelled_wall_median',
                                                    unlabelled_wmv)
        return

    def add_wall_median_voxel(self):
        """
        Add/update the voxel coordinates of the wall medians to/of the
        TissueGraph3D object:
          * vertex_property: 'epidermis_median_voxel', 'unlabelled_wall_median_voxel'
          * edge_property: 'wall_median_voxel'
        """
        wmv, epidermal_wmv, unlabelled_wmv = self._compute_wall_median_voxel()
        self.extend_edge_ppty_from_labelpair_dict('wall_median_voxel', wmv)
        if epidermal_wmv:
            self.extend_vertex_ppty_from_label_dict('epidermis_median_voxel',
                                                    epidermal_wmv)
        if unlabelled_wmv:
            self.extend_vertex_ppty_from_label_dict(
                'unlabelled_wall_median_voxel', unlabelled_wmv)
        return

    def add_epidermis_median(self):
        """
        Add/update the real coordinates of the epidermal wall medians to/of the
        TissueGraph3D object:
          * vertex_property: 'epidermis_median'
        """
        l1_labelpairs = self.analysis.list_epidermal_walls(
            min_area=self.min_area, real_area=self.real_min_area)
        wmv, epidermal_wmv, unlabelled_wmv = self._compute_wall_median_voxel(
            labelpairs=l1_labelpairs, real=True)
        self.extend_vertex_ppty_from_label_dict('epidermis_median',
                                                epidermal_wmv)
        return

    def add_epidermis_median_voxel(self):
        """
        Add/update the voxel coordinates of the epidermal wall medians to/of the
        TissueGraph3D object:
          * vertex_property: 'epidermis_median_voxel'
        """
        l1_labelpairs = self.analysis.list_epidermal_walls(
            min_area=self.min_area, real_area=self.real_min_area)
        wmv, epidermal_wmv, unlabelled_wmv = self._compute_wall_median_voxel(
            labelpairs=l1_labelpairs)
        self.extend_vertex_ppty_from_label_dict('epidermis_median_voxel',
                                                epidermal_wmv)
        return

    def add_rank_2_projection_matrix(self):
        """
        Compute and add (to the graph) the rank-2 projection matrix of each cell
        wall defined by a label-pairs in the dictionary of wall voxel coordinates.
        """
        wall_voxels_dict = self.get_wall_voxels_dict()
        print("-- Computing 'rank2_projection_matrix'...")
        vxs = self.analysis._voxelsize
        proj_mat = {}
        epidermis_proj_mat = {}
        for (label_1, label_2), wvc in wall_voxels_dict.items():
            label_1, label_2 = stuple((label_1, label_2))
            if label_1 == self.analysis._no_label_id:  # no need to check label_2 because labels are sorted
                continue
            elif label_1 == self.background:  # no need to check label_2 because labels are sorted
                epidermis_proj_mat[label_2] = projection_matrix(
                    np.multiply(wvc, vxs), 2)
            else:
                proj_mat[(label_1, label_2)] = projection_matrix(
                    np.multiply(wvc, vxs), 2)

        self.extend_edge_ppty_from_labelpair_dict(
            'wall_rank2_projection_matrix', proj_mat)
        if epidermis_proj_mat:
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_rank2_projection_matrix', epidermis_proj_mat)

        return

    def add_epidermis_rank_2_projection_matrix(self):
        """
        Compute and add (to the graph) the rank-2 projection matrix of each cell
        wall defined by a label-pairs in the dictionary of wall voxel coordinates.
        """
        l1_labelpairs = self.analysis.list_epidermal_walls(
            min_area=self.min_area, real_area=self.real_min_area)
        wall_voxels_dict = self.get_wall_voxels_dict(labelpairs=l1_labelpairs)
        print("-- Computing 'epidermis_rank2_projection_matrix'...")
        vxs = self.analysis._voxelsize
        epidermis_proj_mat = {}
        for (label_1, label_2), wvc in wall_voxels_dict.items():
            label_1, label_2 = stuple((label_1, label_2))
            if label_1 == self.background:  # no need to check label_2 because labels are sorted
                epidermis_proj_mat[label_2] = projection_matrix(
                    np.multiply(wvc, vxs), 2)
            else:
                pass
        return

    def get_wall_plane_info(self, labelpairs=None):
        """
        Compute the principal curvatures of the wall for defined labelpairs.

        Parameters
        ----------
        labelpairs: None|list(tuple)
            if None (default), use 'self.labelspairs', else should be a list of
            length-2 tuples defining pair of labels

        Returns
        -------
        dict
            the labelpair dictionary containing the wall area
        """
        wall_voxels_dict = self.get_wall_voxels_dict(labelpairs)
        print("-- Computing 'wall_orientation'...")
        # - Need to get the dictionary of ALL wall medians (EXCEPT unlabelled_wall):
        # -- starts with 'wall_median', need to convert eid into label-pairs:
        e2l_map = self.get_edge2labelpair_map()
        origins = {e2l_map[k]: v for k, v in
                   self.edge_property('wall_median').items() if
                   k in e2l_map}
        # -- add the 'epidermis_median', need to convert vid into label-pairs (1,label(vid)):
        v2l = self.get_vertex2label_map()
        try:
            ep_wall_median = self.vertex_property("epidermis_median")
        except:
            print("No 'epidermis_median' vertex property found!")
        else:
            origins.update({(self.background, v2l[k]): v for k, v in
                            ep_wall_median.items()})

        # - We can now compute the orientation of the wall:
        pc_norms, pc_normal, pc_directions, pc_origin = self.analysis.wall_orientation(
            wall_voxels_dict, origin_coords=origins)

        # - Separate 'epidermal', 'unlabelled' & 'inner' walls:
        lp_pc_norms, lp_pc_normal, lp_pc_directions, lp_pc_origin = {}, {}, {}, {}
        epi_pc_norms, epi_pc_normal, epi_pc_directions, epi_pc_origin = {}, {}, {}, {}
        unlab_pc_norms, unlab_pc_normal, unlab_pc_directions, unlab_pc_origin = {}, {}, {}, {}
        for label_1, label_2 in wall_voxels_dict.keys():
            # Separate "unlabelled walls"...
            if (label_1 == self.analysis._no_label_id) and (
                    label_2 in self.vertices()):
                # no need to check `label_2` because `wall_voxels_per_cells_pairs`
                # returns label-sorted keys!
                unlab_pc_norms[label_2] = pc_norms[(label_1, label_2)]
                unlab_pc_normal[label_2] = pc_normal[(label_1, label_2)]
                unlab_pc_directions[label_2] = pc_directions[
                    (label_1, label_2)]
                unlab_pc_origin[label_2] = pc_origin[(label_1, label_2)]
            # Separate "epidermis walls"...
            if (label_1 == self.background) and (label_2 in self.vertices()):
                # no need to check `label_2` because `wall_voxels_per_cells_pairs`
                # returns label-sorted keys!
                epi_pc_norms[label_2] = pc_norms[(label_1, label_2)]
                epi_pc_normal[label_2] = pc_normal[(label_1, label_2)]
                epi_pc_directions[label_2] = pc_directions[
                    (label_1, label_2)]
                epi_pc_origin[label_2] = pc_origin[(label_1, label_2)]
            elif (label_1 in self.vertices()) and (
                    label_2 in self.vertices()):
                lp_pc_norms[(label_1, label_2)] = pc_norms[(label_1, label_2)]
                lp_pc_normal[(label_1, label_2)] = pc_normal[
                    (label_1, label_2)]
                lp_pc_directions[(label_1, label_2)] = pc_directions[
                    (label_1, label_2)]
                lp_pc_origin[(label_1, label_2)] = pc_origin[
                    (label_1, label_2)]

        lp_pc = lp_pc_norms, lp_pc_normal, lp_pc_directions, lp_pc_origin
        epi_pc = epi_pc_norms, epi_pc_normal, epi_pc_directions, epi_pc_origin
        return lp_pc, epi_pc

    def add_wall_orientation(self):
        """
        Compute and add the wall plane normal, direction & origin to the `graph`.
        """
        lp_pc, epi_pc = self.get_wall_plane_info()
        lp_pc_norms, lp_pc_normal, lp_pc_directions, lp_pc_origin = lp_pc
        epi_pc_norms, epi_pc_normal, epi_pc_directions, epi_pc_origin = epi_pc

        # - Finally add the computed & sorted values tho the TG:
        self.extend_edge_ppty_from_labelpair_dict(
            'wall_plane_norms', lp_pc_norms)
        self.extend_edge_ppty_from_labelpair_dict(
            'wall_plane_normal', lp_pc_normal)
        self.extend_edge_ppty_from_labelpair_dict(
            'wall_plane_directions', lp_pc_directions)
        self.extend_edge_ppty_from_labelpair_dict(
            'wall_plane_origin', lp_pc_origin)
        if epi_pc_norms != {}:
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_plane_norms', epi_pc_norms)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_plane_normal', epi_pc_normal)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_plane_directions', epi_pc_directions)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_plane_origin', epi_pc_origin)
        return

    def add_epidermis_orientation(self):
        """
        Compute and add the wall plane normal, direction & origin to the `graph`.
        """
        lp_pc, epi_pc = self.get_wall_plane_info(
            labelpairs=self.analysis.list_epidermal_walls(
                min_area=self.min_area, real_area=self.real_min_area))
        epi_pc_norms, epi_pc_normal, epi_pc_directions, epi_pc_origin = epi_pc

        if epi_pc_norms != {}:
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_plane_norms', epi_pc_norms)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_plane_normal', epi_pc_normal)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_plane_directions', epi_pc_directions)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_plane_origin', epi_pc_origin)
        return

    def add_wall_principal_curvatures(self):
        """
        Add the wall principal curvatures to a TissueGraph object.
        """
        lp_pc, epi_pc = self.get_wall_plane_info()
        lp_pc_norms, lp_pc_normal, lp_pc_directions, lp_pc_origin = lp_pc
        epi_pc_norms, epi_pc_normal, epi_pc_directions, epi_pc_origin = epi_pc

        # - Finally add the computed & sorted values tho the TG:
        self.extend_edge_ppty_from_labelpair_dict(
            'wall_principal_curvature_norms', lp_pc_norms)
        self.extend_edge_ppty_from_labelpair_dict(
            'wall_principal_curvature_normal', lp_pc_normal)
        self.extend_edge_ppty_from_labelpair_dict(
            'wall_principal_curvature_directions', lp_pc_directions)
        self.extend_edge_ppty_from_labelpair_dict(
            'wall_principal_curvature_origin', lp_pc_origin)
        if epi_pc_norms != {}:
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_principal_curvature_norms', epi_pc_norms)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_principal_curvature_normal',
                epi_pc_normal)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_principal_curvature_directions',
                epi_pc_directions)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_principal_curvature_origin',
                epi_pc_origin)
        return

    def add_epidermis_principal_curvatures(self):
        """
        Add the wall principal curvatures to a TissueGraph object.
        """
        lp_pc, epi_pc = self.get_wall_plane_info(
            labelpairs=self.analysis.list_epidermal_walls(
                min_area=self.min_area, real_area=self.real_min_area))
        epi_pc_norms, epi_pc_normal, epi_pc_directions, epi_pc_origin = epi_pc

        if epi_pc_norms != {}:
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_principal_curvature_norms', epi_pc_norms)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_principal_curvature_normal',
                epi_pc_normal)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_principal_curvature_directions',
                epi_pc_directions)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_principal_curvature_origin',
                epi_pc_origin)
        return

    def add_epidermis_local_pc(self):
        """
        Add 'epidermis_local_principal_curvatures' to the TG.
        This is different from 'epidermis_principal_curvatures'.
        'epidermis_principal_curvatures' use only the voxels of the epidermal wall,
        when 'epidermis_local_principal_curvatures' use a sampling radius.
        """
        for radius in self.local_curvature_sampling_radius:
            print("-- Computing 'local_principal_curvature' property with radius = {}voxels...".format(radius))
            vx = self.analysis._voxelsize
            print(u"This represent a local curvature estimation area of {}\u03BCm\u00B2".format(
                round(math.pi * (radius * vx[0]) * (radius * vx[1]))))
            # We reset the dictionaries (to empty) since it may contain the values computed with a different radius!
            self.analysis._principal_curvatures_norms = {}
            self.analysis._principal_curvatures_normal = {}
            self.analysis._principal_curvatures_origin = {}
            self.analysis._principal_curvatures_directions = {}
            # Now we can call the computation of local principal curvature:
            self.analysis.local_principal_curvature(labels=self._labels,
                                                    sampling_radius=radius,
                                                    verbose=True)
            # Add computed principal curvature values to the TG:
            origin = {lab: val for (lab, rad), val in
                      self.analysis._principal_curvatures_origin.items()}
            directions = {lab: val for (lab, rad), val in
                          self.analysis._principal_curvatures_directions.items()}
            norms = {lab: val for (lab, rad), val in
                     self.analysis._principal_curvatures_norms.items()}
            normals = {lab: val for (lab, rad), val in
                       self.analysis._principal_curvatures_normal.items()}
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_local_principal_curvature_norms_r' + str(radius),
                norms)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_local_principal_curvature_normal_r' + str(radius),
                normals)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_local_principal_curvature_directions_r' + str(
                    radius), directions)
            self.extend_vertex_ppty_from_label_dict(
                'epidermis_local_principal_curvature_origin_r' + str(radius),
                origin)
        return

    ## WALL-EDGE BASED PROPERTIES:
    ##------------------------------------------------------------------------------
    def _compute_epidermis_edge_median_voxel(self):
        """
        Compute & add the median voxel of the L1-cell wall edges.
        Used to compute the 'epidermis_3DS_landmarks'.
        """
        epidermal_med_vox = self.analysis.epidermis_edges_median(
            self.list_epidermis_anticlinal_edge_labelpairs(), real=False,
            verbose=True)

        # - Separate wall defined with 'unlabelled' id:
        unlab_epidermal_med_vox = {}
        for (label1, label2), ewem in epidermal_med_vox.items():
            if ewem is None:
                epidermal_med_vox.pop((label1, label2))
                continue
            if label1 == self.analysis._no_label_id:  # (label1, label2) is a sorted tuple
                unlab_epidermal_med_vox[label2] = epidermal_med_vox[
                    (label1, label2)]
                # remove unlabelled walls so only true walls remains
                epidermal_med_vox.pop((label1, label2))

        return epidermal_med_vox, unlab_epidermal_med_vox

    def add_epidermis_edge_median_voxel(self):
        """
        Add the wall edge median voxel coordinate for the epidermal walls.
        """
        epidermal_med_vox, unlabelled_epidermal_med_vox = self._compute_epidermis_edge_median_voxel()
        # Saving all this within the graph:
        self.extend_edge_ppty_from_labelpair_dict('epidermis_edge_median_voxel',
                                                  epidermal_med_vox)
        if unlabelled_epidermal_med_vox:
            self.extend_vertex_ppty_from_label_dict(
                'unlabelled_epidermis_edge_median_voxel',
                unlabelled_epidermal_med_vox)
        return

    def get_wall_voxels_dict(self, labelpairs=None):
        """
        Returns a dictionary of wall voxels coordinates using at least one of the three
        neighborhood input method: 'neighborhood', 'bak_neigh' and 'labels_w_undef_neigh'.

        Parameters
        ----------
        labelpairs: list(tuple), optional
            list of label-pair tuple for which to get the wall voxels coordinates

        Returns
        -------
        wall_voxels_dict: dict
            a sorted label-pair tuple dictionary with wall coordinates values
            {(label_i, label_j): wall_coords_ij}, with i < j and np.array as values
        """
        # -- We start by defining all (pairs of cells defining) walls we want to extract:
        if labelpairs is None:
            labelpairs = self.labelpairs
        else:
            # Make sure the unicity of the label-pairs list:
            labelpairs = [stuple(lp) for lp in labelpairs]
        nb_pairs = len(labelpairs)

        # - Try to initialise with 'self.wall_voxels_dict':
        try:
            wall_voxels_dict = self.wall_voxels_dict
        except:
            wall_voxels_dict = {}

        get_wall_voxels = self.analysis.wall_voxels_between_two_cells
        no_bbox = set([])
        for n, (label_1, label_2) in enumerate(labelpairs):
            if self.analysis.boundingbox(label_1) is None:
                no_bbox.update(label_1)
            if self.analysis.boundingbox(label_2) is None:
                no_bbox.update(label_2)
            slp = stuple([label_1, label_2])
            if slp not in wall_voxels_dict.keys() and label_1 not in no_bbox and label_2 not in no_bbox:
                wv = get_wall_voxels(label_1, label_2)
                wall_voxels_dict[slp] = wv

        # Print some information about extraction success:
        p = round(len(wall_voxels_dict) * 100 / float(nb_pairs), 1)
        print("Got {}/{} wall voxels coordinates ({}% of input labelpairs)".format(len(wall_voxels_dict), nb_pairs, p))
        # Print if bounding boxes could not be found for som labels:
        if no_bbox != set([]):
            print("No bounding boxes could be found for {} labels!".format(len(no_bbox)))
            if len(no_bbox) < 10:
                print(no_bbox)

        # - Save/update it as an object attribute:
        self.wall_voxels_dict = wall_voxels_dict
        return self.wall_voxels_dict


class TissueGraph4DBuilder(TissueGraph4D):
    """
    Class to create TissueGraph4D objects based on a list of SpatialImages
    and a list of lineage.
    """

    def __init__(self, images, lineages, time_steps, real_units=True, **kwargs):
        """
        Parameters
        ----------
        images: list(str|SpatialImage)
            list of image paths (strings) or SpatialImages
        lineages: list(str|dict)
            list of lineage paths (strings) or mapping dictionaries
        time_steps: list
            time steps of each image (usually start a 0)
        real_units: bool, optional
            if True (default), the computed spatio-temporal properties are
            returned in real-world units, else in voxels
    
        Notes
        -----
        * The temporary backup allow to resume the computation of the TissueGraph4D
        * It is possible to force its uses with `force_tmp_tgfi_load=True`
        * Please make sure the following assertions are true:
          ** nb_lineages == nb_images-1
          ** nb_time_steps == nb_images
          ** if background is a list: nb_background == nb_images
        """
        # - Parse keyword arguments:
        self._parse_kwargs(real_units, **kwargs)
        # - Usual paranoia to minimise useless computation:
        self._check_images_lineages(images, lineages)
        self._check_time_steps(time_steps)
        # -- Check 'self.background':
        self._check_background_list()

        # - Load the cell lineages:
        self.load_lineages()

        # - Initialise the TissueGraph3D dictionary:
        self.TissueGraph3D = {}
        """
        keys are (time_point, rank), where:
          * if rank == 0: TissueGraph3D from the original SpatialImage
          * if rank == n: TissueGraph3D from the rank-n fused sibling SpatialImage
        """
        # - Load/Create the dictionary of TissueGraph3D objects:
        self.build_TissueGraph3D_dict(max_rank=0)
        # - Initialise the TissueGraph4D object:
        print("\n-- Initialising the 'TissueGraph4D' object...")
        tg3d = [self.TissueGraph3D[(tp, 0)] for tp in self.time_points]
        TissueGraph4D.__init__(self, tg3d, mappings=self.lineages)

        if self._auto_make:
            pass

        self.__str__()

    def __str__(self):
        to_str = lambda l: map(str, l)
        print("\nTissueGraph4DBuilder from:")

        if self._same_path_images:
            print("   - the image path is: '{}'".format(self.path_images[0]))
        else:
            print("   - the image paths are:\n - {}".format(' - '.join(to_str(self.path_images))))

        print("   - the following {} images:".format(self.nb_images))
        print('\n'.join(["\t* t{} ({}{}): {}".format(n, self.time_steps[n],
                                                     self.time_unit,
                                                     self.images_nopath[n]) for
                         n in range(self.nb_images)]))

        if self._same_path_lineages:
            print("   - the lineage path is: '{}'".format(self.path_lineages[0]))
        else:
            print("   - the lineage paths are:\n - {}".format(' - '.join(to_str(self.path_lineages))))

        print("   - the following {} lineages:".format(self.nb_lineages))
        print('\t* ' + '\n\t* '.join(self.lineages_nopath))

    def load_lineages(self):
        """
        Load the lineages from the file according to the specified format.
        """
        print("\n-- Loading lineages from files, in '{}' format...".format(self.lineage_fmt_type)),
        lin_fnames = [self.path_lineages[n] + lin_fname for n, lin_fname in
                      enumerate(self.lineages_nopath)]
        # -- Getting cell lineages contained within the lineage files:
        if self.lineage_fmt_type == 'basic':
            self.lineages = [Lineage(lin).read() for lin in lin_fnames]
        elif self.lineage_fmt_type == 'marsalt':
            self.lineages = [lineage_from_file(lin) for lin in lin_fnames]
        else:
            print("Unknown lineage type '{}', available types are:\n{}".format(self.lineage_fmt_type, AVAILABLES_LINEAGE_FMT))
        print("Done.")
        return

    def get_lineaged_labels(self):
        """
        Returns a time sorted dictionary of 'lineaged labels':
         - as ancestors from t_0 to t_n-1, {n: [ancestors_lineaged_labels]}
         - as descendants (t_n), {n: [descendants_lineaged_labels]}

        Returns
        -------
        a dictionary
        """
        # -- Extract 'lineaged labels': ancestors (t_0 to t_n-1) and descendants (t_n):
        self.lineaged_labels = {}
        if self.lineaged_labels == {}:
            for n, lin in enumerate(self.lineages):
                if n <= self.nb_lineages - 1:
                    self.lineaged_labels[n] = lin.keys()
                if n == self.nb_lineages - 1:
                    desc_lab = []
                    for d in lin.values():
                        desc_lab.extend(d)
                    self.lineaged_labels[n + 1] = desc_lab

        return self.lineaged_labels

    def list_temporal_features(self):
        """
        Return available temporal properties to be computed by TissueGraph4DBuilder.
        """
        return TEMPORAL_FEATURES

    def list_spatial_feature(self):
        """
        Return all available properties to be computed by TissueGraph3DBuilder.
        """
        return SPATIAL_FEATURES

    @staticmethod
    def load(filename):
        """
        Load a TissueGraph4DBuilder from the given filename.

        Parameters
        ----------
        filename: str
            name of the file to load.
        """
        from tissue_analysis.compression import Compress
        obj = Compress(filename).open()

        if isinstance(obj, TissueGraph3DBuilder):
            return obj
        else:
            print("Object within file '{}' is not a TissueGraph3DBuilder!")
        return

    def relabel_to_ancestor(self, time_point, rank=1):
        """
        Fuse the labels of a given time point to their ancestor at given rank.

        Parameters
        ----------
        time_point: int
            descendant time-point
        rank: int, optional
            backward distance from time point at which to select the ancestors
            value to use during relabelling

        Returns
        -------
        SpatialImageAnalysis
            labelled image from 'time_point' relabelled with ancestors
            labels from 'time_point - rank'
        """
        try:
            assert rank > 0
        except AssertionError:
            raise ValueError("Parameter 'rank' should be strictly positive!")
        try:
            assert time_point - rank >= 0
        except AssertionError:
            msg = "Given parameters 'rank' ({})".format(rank)
            msg += "and 'time_point' ({})".format(time_point)
            msg += "do not allow to relabel toward ancestors ids!"
            raise ValueError(msg)

        im2fuse = self.images_nopath[time_point]
        print("Relabelling image '{}' with rank-{} ancestors labels.".format(im2fuse, rank))
        lineage = self.get_lineage(time_point - rank, rank=rank, as_labels=True)
        mapping = {desc: anc for anc, desc_list in lineage.items() for desc in
                   desc_list}
        analysis = self.TissueGraph3D[(time_point, 0)].analysis

        return analysis.image.relabel_from_mapping(mapping, clear_unmapped=True,
                                                   verbose=True)

    def make_TissueGraph3D(self, time_point, rank=0):
        """
        Create a TissueGraph3D object for given time_point and rank.

        Parameters
        ----------
        time_point: int
            temporal index of the SpatialImage to use for building a
            TissueGraph3D object
        rank: int, optional
            if 0 (default), the original SpatialImage is used, else a "sibling
            fused" version for the given rank is used

        Returns
        -------
        tg: TissueGraph3D
            the TissueGraph3D obtained from the TissueGraph3DBuilder

        """
        print("\n--- Creating TissueGraph3D for time-point {} ({}{})".format(time_point, self._time_point2steps[time_point], self.time_unit)),
        if rank != 0:
            print("with rank-{} descendants-fused image...".format(rank))
        else:
            print("...")
        back_id = self.background[time_point]
        path = self.path_images[time_point]
        im_fname = self.images_nopath[time_point]
        # - if rank > 0, we need to fuse the sibling labels in the image:
        if rank != 0:
            img = self.relabel_to_ancestor(time_point, rank)
            tgb = TissueGraph3DBuilder(img, labels=None, background=back_id)
        else:
            tgb = TissueGraph3DBuilder(path + im_fname, labels=None,
                                       background=back_id)
        return tgb

    def build_TissueGraph3D_dict(self, max_rank=1, compression='lz'):
        """
        Build the dictionary of TissueGraph3D for all time points and for
        relabelled to rank ancestors.

        Parameters
        ----------
        max_rank: int, optional
            maximum depth of "ancestor relabelling" to performs
        compression: str, optional
            extension (default = 'lz') to use when saving compressed pickled
            TissueGraph3DBuilder after initialisation;
            use None to avoid compression
        """
        dot = "." if not compression.startswith('.') else ""
        print("\n-- Building dictionary of TissueGraph3D objects")
        # - Build the list of TissueGraph3D file names:
        TissueGraph3D_fnames = {}
        for tp, im in enumerate(self.images_nopath):
            filepath = self.path_images[tp]
            fname = filepath + fname_NoExt(im)
            for rank in range(0, max_rank + 1):
                if rank != 0 and tp < rank:
                    # - Can not "relabel to ancestors" the first time-point if rank-1,
                    # - Can not "relabel to ancestors" the first two time-point if rank-2,
                    # ...
                    continue
                if rank > 0:
                    fname += "_TissueGraph3D_rank{}.pkl".format(rank)
                else:
                    fname += "_TissueGraph3D.pkl"
                TissueGraph3D_fnames[(tp, rank)] = fname + dot + compression

        # - Look for existing TissueGraph3D on drive, or create it:
        for (tp, rank), fname in TissueGraph3D_fnames.items():
            tg3d = None
            if os.path.exists(fname):
                print("Found existing TissueGraph3DBuilder...")
                # test_TissueGraph3D return None if could not load it
                # tg3d = Compress(fname, method=None).open()  # FIXME!
                # tg3d = self.TissueGraph3D[(tp, rank)].test_TissueGraph3D(fname)
                if tg3d is None:
                    print("UNUSABLE!")
            else:
                print("Did not find existing TissueGraph3DBuilder: '{}'!".format(fname))

            if tg3d is None:
                tg3d = self.make_TissueGraph3D(tp, rank)
                # tg3d.save(fname)  # FIXME!
            self.TissueGraph3D[(tp, rank)] = tg3d

        return

    def build_tissue_graph_4D(self):
        """
        Build the TissueGraph4D object.
        """
        missing_tg3d = [self.TissueGraph3D.has_key((tp, 0)) for tp in
                        range(self.nb_time_steps)]
        if sum(missing_tg3d) == 0:
            # - NO TissueGraph3D exist:
            self.build_TissueGraph3D_dict(0)
        else:
            # - some TissueGraph3D are missing:
            for tp in range(self.nb_time_steps):
                if not self.TissueGraph3D.has_key((tp, 0)):
                    self.make_TissueGraph3D(tp, 0)

        ### ----- TissueGraph4D initialisation ----- ###
        print("\n-- TissueGraph4D initialisation...")
        # -- Now construct a "property-free" TissueGraph4D based on tp*TissueGraph3D:
        graphs = [self.TissueGraph3D[(tp, 0)] for tp in
                  range(self.nb_time_steps)]
        tg = TissueGraph4D(graphs, self.lineages,
                           time_steps=self.time_steps,
                           check_mapping=self.check_mapping,
                           min_area=self.min_area,
                           real_min_area=self.real_min_area)
        print("Done!\n")
        print("Generated a TissueGraph4D object containing:")
        print(tg.__str__())

    def _check_images_lineages(self, images, lineages):
        """
        Check:
         - components of list 'images' & 'lineages' are str.
         - N_lineage +1 = N_image  

        Parameters
        ----------
        images: list
            list of str instances indicating labelled images location
        lineages: list
            list of str instances indicating lineages location
        """
        # - Check components of list 'image' & 'lineage' are str.
        try:
            assert np.alltrue([isinstance(i, str) for i in images])
        except AssertionError:
            raise TypeError("Parameter 'images' should be a list of str")
        try:
            assert np.alltrue([isinstance(l, str) for l in lineages])
        except AssertionError:
            raise TypeError("Parameter 'lineages' should be a list of str")

        # - Check N_lineage = N_image - 1
        self.nb_images = len(images)
        self.nb_lineages = len(lineages)
        try:
            assert self.nb_lineages == self.nb_images - 1
        except AssertionError:
            raise ValueError(
                "Got {} lineage and {} images (should be n, n-1)".format(
                    self.nb_lineages, self.nb_images))

        # - Handling file names & paths for images:
        # if isinstance(images[0], str):
        self.images_nopath = [im.split('/')[-1] for im in images]
        self.path_images = ['/'.join(im
                                     .split('/')[:-1]) + "/" for im in images]
        # elif isinstance(images[0], SpatialImage):
        #     try:
        #         self.images_nopath = [im.metadata['filename'] for im in images]
        #     except:
        #         self.images_nopath = ["" for im in images]
        #     self.path_images = ['' for im in images]
        # else:
        #     raise TypeError("Parameter 'images' should be a list of str")
        if sum([self.path_images[0] == p_im for p_im in
                self.path_images[1:]]) == (len(self.path_images) - 1):
            self._same_path_images = True
        else:
            self._same_path_images = False

        # - Handling file names & paths for lineages:
        # if isinstance(lineages[0], str):
        self.lineages_nopath = [lin.split('/')[-1] for lin in lineages]
        self.path_lineages = ['/'.join(lin.split('/')[:-1]) + "/" for lin in
                              lineages]
        # elif isinstance(lineages[0], dict):
        #     self.lineages_nopath = ["" for lin in lineages]
        #     self.path_lineages = ["" for lin in lineages]
        #     self.lineage_fmt_type = 'dict'
        # else:
        #     raise TypeError("Parameter 'lineages' should be a list of str")
        if sum([self.path_lineages[0] == p_lin for p_lin in
                self.path_lineages[1:]]) == (len(self.path_lineages) - 1):
            self._same_path_lineages = True
        else:
            self._same_path_lineages = False

        return

    def _check_time_steps(self, time_steps):
        """
        Check:
         - N_time_steps = N_image

        Parameters
        ----------
        time_steps: list
            list of int instances indicating time steps
        """
        # - Check N_time_steps = N_image
        self.nb_time_steps = len(time_steps)
        try:
            assert self.nb_time_steps == self.nb_images
        except:
            raise ValueError(
                "Got {} time steps and {} images (should be equal)".format(
                    self.nb_time_steps, self.nb_images))
        else:
            self.time_steps = time_steps
            self.time_points = range(self.nb_images)
            self._time_point2steps = {n: t for n, t in enumerate(time_steps)}
            self._time_steps2points = {t: n for n, t in enumerate(time_steps)}

        return

    def _check_background_list(self):
        """
        Check if self.background is correctly defined.
        """
        if isinstance(self.background, int) or self.background is None:
            self.background = [self.background] * self.nb_images
        try:
            assert len(self.background) == self.nb_images
        except AssertionError:
            raise ValueError(
                "Parameter 'background' should be, if not None or an integer, a list of same length than the number of images")
        return

    def _check_fname(self, fname):
        """
        Remove the extension from 'fname' str, if any.

        Parameters
        ----------
        fname: str
            a filename from which the extension will be removed

        Returns
        -------
        an str without the extension
        """
        splitted_fname = fname.rsplit(".")
        fname = ".".join(splitted_fname[:-1]) if (
                (splitted_fname[-1] == 'pkl') or (splitted_fname[
                                                      -1] == 'pkz')) else fname  # remove the extension (last dot-separated piece of the string, if any)
        return fname

    def _parse_kwargs(self, real_units, **kwargs):
        """
        Use kwargs to define several attributes related to the way the graphs
        are build, the units, registration and other values.

        Parameters
        ----------
        real_units: bool, optional
            if True (default), the computed spatio-temporal properties are
            returned in real-world units, else in voxels

        **kwargs
        --------
        # -- Labelled image attributes:
        background: int (default=None)
            if any, the background id to be found within the
            labelled SpatialImage

        # -- Temporal related attributes:
        time_unit: str (default=None)
            indicate the time unit (eg. 'h', 'min', ...) of the time-intervals,
            to be used in later feature plots

        # -- TissueGraph3D creation attributes:
        min_area: float (default=None)
            area threshold to consider two cells as in contact
        real_min_area: bool (default=real_units)
            define if the `min_area` kwarg should be in real-unit or voxels
        detect_marginal_cells: bool (default=False)
            if True, the cells at the stacks margins will be removed before
            constructing the graph
        auto_make: bool (default=False)
            if True, the TissueGraph3D object is built during initialisation and
            declared `spatial_features` will be computed

        # -- TissueGraph4D creation attributes:
        lineaged_labels_only: bool (default=False)
            if True, only lineaged labels will be used to create topological
            graphs and will have their feature computed
        check_mapping: bool (default=True)
            if True, check that a cell does not have two mothers in the mapping
        lineage_fmt_type: str (default=DEFAULT_LINEAGE_FMT)
            indicate the format of the lineage file, available formats are given
            in `AVAILABLES_LINEAGE_FMT` by default it is set to 'basic'
        force_tmp_tgfi_load: bool (default=False)
            if True, the temporary backup will not be check for consistency with
            the input images. Uses this for development purposes ONLY!
        tmp_fname: str (default="tmp_tgfi_process")
            filename used during the TissueGraph4D creation process to create
            temporary backup
        tg_fname: str (default="", ie. empty)
            filename used to save the TissueGraph4D at the end of the process

        # -- SPATIAL features computation related attributes:
        spatial_features: list(str) (default=None)
            list of spatial features to compute, use
            self.available_spatial_features() to list those available

        # -- TEMPORAL features computation related attributes:
        temporal_features: list(str) (default=None)
            list of temporal features to compute, use
            self.available_temporal_features() to list those available

        TODO:
        time_serie_id: str (default=None)
            name of the time series represented by the list images and lineages
        register_images: bool (default=False)
            boolean conditioning the registration of the images using the
            lineaged cells barycenter
        reference_image: int (default=nb_images, i.e last one!)
            index of the images used as reference during image registration
            process, by default uses the last one
        """
        # - Parsing kwargs and defining attributes:
        # -- Labelled image attributes:
        # TODO: get it from the SpatialImage (should be defined as an attribute)
        self.background = kwargs.get('background', None)

        # -- Temporal related attributes:
        self.time_unit = kwargs.get('time_unit', None)

        # -- TissueGraph3D creation attributes:
        self.min_area = kwargs.get('min_area', None)
        self.real_min_area = kwargs.get('real_min_area', real_units)

        # -- TissueGraph4D creation attributes:
        self.lineaged_labels_only = kwargs.get('lineaged_labels_only', False)
        # TODO: define a list of marginal_labels around this group of cells?
        if self.lineaged_labels_only:
            # TODO: add a 'lineage_rank' kwargs to check for other rank than rank=1 ?
            pass
        self.check_mapping = kwargs.get('check_mapping', True)
        self.lineage_fmt_type = kwargs.get('lineage_fmt_type',
                                           DEFAULT_LINEAGE_FMT)
        self._auto_make = kwargs.get('auto_make', False)
        if self._auto_make:
            print("Auto build enabled!")

        # -- Features computation related attributes:
        # Performs feature check:
        self._temporal_features_check(**kwargs)
        # If we have a list of feature but the 'auto_make=False' was not specified we enable auto_make:
        if self.features2compute is not None and 'auto_make' not in kwargs:
            self._auto_make = True

        # -- Temporary and final name to give to the TissueGraph4D:
        self.tmp_fname = kwargs.get('tmp_fname', DEFAULT_TMP_FNAME)
        if self.tmp_fname != DEFAULT_TMP_FNAME:
            self.tmp_fname = self._check_fname(self.tmp_fname)
        self.tg_fname = kwargs.get('tg_fname', "")
        if self.tg_fname:
            self.tg_fname = self._check_fname(self.tg_fname)

        # -- Used by image registration process:
        # self.register_images = kwargs.get('register_images', False)
        # self.reference_image = kwargs.get('reference_image', self.nb_images)
        return

    def _temporal_features_check(self, **kwargs):
        """
        Performs all the necessary check for the spatial feature list.

        Returns
        -------
        self.features2compute: list
            the list of features to compute
        """
        self.features2compute = kwargs.get('temporal_features', None)

        if self.features2compute is not None:
            # -- Check the types in the list of spatial feature:
            self._check_temporal_feature_type()
            # -- Check the features requirements are met:
            self._check_tp_feature_requirements()
            # -- Check against potential known features (vertex, egde & graph) in the TissueGraph3D:
            self._check_against_available_features()
        return self.features2compute

    def _check_temporal_feature_type(self):
        """
        Check that self.features2compute is a list of str.
        """
        try:
            assert isinstance(self.features2compute, list)
            assert np.alltrue(
                [isinstance(f, str) for f in self.features2compute])
        except AssertionError:
            msg = "Keyword argument 'temporal_features' should be a list of str"
            raise ValueError(msg)
        print("Got an initial list of temporal feature to compute:")
        print("  - " + "\n  - ".join(sorted(self.features2compute)))
        return

    def _check_tp_feature_requirements(self):
        """
        Function used to check the requirements of some of the 'spatial_features'.
        """
        print("\n# - Checking TEMPORAL feature requirements:")
        # - Use 'CELL_TP_REQUIREMENTS' & 'WALL_TP_REQUIREMENTS' to check requirements
        # of features to compute:
        n_max = 10000
        n_feat = 0
        n = 0
        while len(
                self.features2compute) != n_feat:  # to allow to check requirements of the requirements themselves!
            n_feat = len(self.features2compute)
            for feat in self.features2compute:
                if feat in CELL_TP_REQUIREMENTS:
                    self.features2compute.extend(CELL_TP_REQUIREMENTS[feat])
                if feat in WALL_TP_REQUIREMENTS:
                    self.features2compute.extend(WALL_TP_REQUIREMENTS[feat])
            # - Remove possible duplicates:
            self.features2compute = list(set(self.features2compute))
            if n == n_max - 1:
                print(self.features2compute)
                raise ValueError(
                    "Infinite loop, check 'CELL_TP_REQUIREMENTS' & 'WALL_TP_REQUIREMENTS' definitions!")
            else:
                n += 1

        if n - 1 != 0:
            print("Made {} iterations of feature requirements checks!".format(n - 1))

        # - Print about selected properties to compute:
        feat_list = self.features2compute
        print("List of propert{} to compute:".format("ies" if len(feat_list) > 1 else "y"))
        print('  - ' + '\n  - '.join(sorted(feat_list)))

        return self.features2compute

    def _check_against_available_features(self):
        """
        Check the list of features to compute is available for computation.

        Returns
        -------
        Nothing, edit the list of features to compute `self.features2compute`
        """
        self.features2compute = [f for f in self.features2compute if
                                 self.is_available_feature(f)]
        return self.features2compute

    def is_available_feature(self, feature):
        """
        Check if the given 'feature' is accessible to computation.

        Parameters
        ----------
        feature: str
            feature name to test against those defined in 'TEMPORAL_FEATURES'
            and 'SPATIAL_FEATURES'

        Returns
        -------
        known_feature: bool
            True if it can be computed and added to the TissueGraph4D object,
            else False
        """
        return feature in self.list_temporal_features() or feature in self.list_spatial_feature()
