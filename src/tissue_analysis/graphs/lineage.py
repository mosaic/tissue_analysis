#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#       TissueAnalysis.graphs
#
#       Copyright 2017 INRIA - CIRAD - INRA - ENS Lyon
#
#       File author(s): Vincent Mirabet <vincent.mirabet@ens-lyon.fr>
#                       Jonathan LEGRAND <jonathan.legrand@ens-lyon.fr>
#
###############################################################################

"""
This module implement tools to read/write lineages.
"""

import sys
import warnings

import numpy as np
from tissue_analysis.util import flatten_list

def lineage_resampling(lineage_list, resampling_index):
    """Allow to get new list of lineages from temporal down-sampling.

    Parameters
    ----------
    lineage_list: list
        list of lineages (filenames or dictionaries);
    resampling_index: list
        indexes of lineages to keep, extend in between (if necessary).

    Returns
    -------
    list

    Example
    -------
    >>> from tissue_analysis.graphs.lineage import lineage_resampling
    >>> lineage_resampling([lin01, lin12, lin23, lin34, lin45], [0, 2, 5])
    >>> [lin02, lin25]
    """
    N_lin = len(lineage_list)
    # Check all required index (`resampling_index`) are accessible in the `lineage_list`:
    assert np.all([idx <= N_lin for idx in resampling_index])

    # Handle Lineage objects creation from `lineage_list`:
    for n, lin in enumerate(lineage_list):
        if isinstance(lin, str):
            lineage_list[n] = Lineage.read(lin)
        elif isinstance(lin, Lineage):
            lineage_list[n] = lin
        else:
            try:
                lineage_list[n] = Lineage.read(lin)
            except:
                print("Error loading the lineage {}/{}, ABORTING!".format(n+1, N_lin))
                return None

    resampled_lin = []
    for b,e in zip(resampling_index[:-1], resampling_index[1:]):
        print("Concatenation of lineages index {} to {}...".format(b,e))
        for i in range(b,e):
            if i == b:
                tmp_lin = lineage_list[i]
            else:
                tmp_lin = extend_successive_lineage(tmp_lin, lineage_list[i])
        resampled_lin.append(tmp_lin)

    return resampled_lin


def extend_successive_lineage(lin_ab, lin_bc, flatten=True, verbose=True):
    """
    Use this to extend a lineage A-B with a lineage B-C, thus returning a lineage A-C.
    To keep sub-lineage change to flatten = False. TODO !!!

    Parameters
    ----------
    lin_ab: Lineage
        a lineage object (not an str);
    lin_bc: Lineage
        a lineage object (not an str);
    flatten: bool
        if True, the extended lineage is flattenned, else wub-lineage is kept (not-working yet!);
    verbose: bool
        determine whether or not missing lineages info will be printed;

    Returns
    -------
    lin_ac: Lineage
        a lineage object.
    """
    lin_ac = {}; missing_lineage = 0
    for a_cell in lin_ab:
        try:
            if flatten:
                lin_ac[a_cell] = list(flatten_list([lin_bc[b_cell] for b_cell in lin_ab[a_cell]]))
            else:
                lin_ac[a_cell] = [lin_bc[b_cell] for b_cell in lin_ab[a_cell]]
        except:
            missing_lineage += 1

    if verbose:
        print("Missing {}/{} lineage extensions !".format(missing_lineage, len(lin_ab)))
    return lin_ac

DEFAULT_LINEAGE_FMT = 'basic'
AVAILABLES_LINEAGE_FMT = ['basic', 'marsalt']

class Lineage(object):
    """
    Class Lineage define temporal relations between labels of two
     temporally related segmented images.

    Allow to import lineage from {ancestor: descendants} dictionary or
     '.txt' files (saved from ALT or handmade).
    """
    def __init__(self, input_lineage=None, lineage_fmt=DEFAULT_LINEAGE_FMT):
        if isinstance(input_lineage, str):
            self.filename = input_lineage
            try:
                self.lineage_dict = self.read(input_lineage, lineage_fmt)
            except IOError:
                 msg = "Could not find input file: '{}'".format(input_lineage)
                 raise ValueError(msg)
        elif isinstance(input_lineage, dict):
            self.lineage_dict = input_lineage
        else:
            msg = "Could not make sense of the `input_lineage` provided!"
            raise TypeError(msg)
        self.ancestor_of = self._invert_lineage_dict()

    def ancestors_list(self):
        """Returns the list of ancestors found in the lineage object. """
        return list(self.lineage_dict.keys())

    def descendants_list(self):
        """Returns the list of descendants found in the lineage object. """
        return list(self.ancestor_of.keys())

    def read(self, filename, lineage_fmt=DEFAULT_LINEAGE_FMT):
        """Read a lineage depending on its format 'lineage_fmt'.

        Parameter
        ---------
        filename: str
            path to the lineage file to

        Returns
        -------
        dict
            lineage dictionary, with ancestors ids as keys associated to a list of
            descendants ids.
        """
        if lineage_fmt == "basic":
            lineage_dict = self._read_basic(filename)
        else:
            lineage_dict = self._read_marsalt(filename)
        return lineage_dict

    def _read_basic(self, filename):
        """Create lineage dictionary {ancestor: descendants} from a basic lineage file.

        Parameter
        ---------
        filename: str
            path to the lineage file to

        Returns
        -------
        dict
            lineage dictionary, with ancestors ids as keys associated to a list of
            descendants ids.
        """
        f = open(filename,'r')
        lines = f.readlines()
        lineage_dict = {}
        for line in lines:
            numbers = line.split()
            # - We make sure of the unicity of the lineage: each mother has been associated ONCE!
            m_id = int(numbers[0])
            c_id = [int(i) for i in numbers[1:]]
            if m_id in lineage_dict:
                warnings.warn( "Mother cell #{} has already been associated with:\n{}".format(m_id, lineage_dict[m_id]) )
                warnings.warn( "You are trying to associate it again with:\n{}".format(c_id) )
                print(" -- Skip this!")
            lineage_dict[m_id]=c_id
        f.close()
        return lineage_dict

    def _read_marsalt(self, filename):
        """Create lineage dictionary {ancestor: descendants} from a marsalt lineage file.

        Parameter
        ---------
        filename: str
            path to the lineage file to

        Returns
        -------
        dict
            lineage dictionary, with ancestors ids as keys associated to a list of
            descendants ids.
        """
        return lineage_from_file(filename)

    def _invert_lineage_dict(self):
        """ Invert the lineage dictionary to access
        """
        inv_lineage_dict={}
        for m_id in self.lineage_dict.keys():
            for c_id in self.lineage_dict[m_id]:
                # - We make sure of the unicity of the lineage: each daughter has been associated ONCE!
                if c_id in inv_lineage_dict:
                    warnings.warn( "Daughter cell #{} has already been associated with:\n{}".format(c_id, inv_lineage_dict[c_id]) )
                    warnings.warn( "You are trying to associate it again with:\n{}".format(m_id) )
                    print(" -- Skip this!")
                inv_lineage_dict[c_id]=m_id
        return inv_lineage_dict


########################################################################
# SOME OLD MARS-ALT I/O FUNCTIONS
########################################################################
def lineage_to_file(filename, lineage):
    """Writes a lineage dictionnary to a file in RomainFernandez format"""
    text = "NombreDeCellules="+str(len(lineage))+"\n"
    for m, daugh in lineage.items():
        daugh.sort()
        text += str(m) + ": " + "nbFilles=" + str(len(daugh)) + " -> ["
        for d in daugh:
            if isinstance(d, tuple): #we have a daughter+distance tuple
                text += "(%i, %.3f),"%d
            else:
                text += str(d)+","
        text += "-1,]\n"
    with open(filename, "w+t") as expertFile:
        expertFile.write(text)
    return lineage


def lineage_from_file(filename):
    """Reads a lineage definition file and outputs a dict"""
    d = {}
    with open(filename, "r") as f:

        # -- in case the file is a python module that encodes the mapping
        # as a dictionnary. We simply evaluate the second part of the assignment
        # operator ( thus the split("=")[1] ) --
        try:
            d = eval(f.read().split("=")[1])
            check_lineage_input(d)
        except:
            # --rewind and try classical read. --
            f.seek(0)

        lines = f.readlines()
        # -- Find out if we are looking at a mapping in the Romain Fernandez format --
        rf_style = False
        for l in lines:
            if "NombreDeCellules" in l:
                rf_style = True
                break

        if rf_style: # is RomainFernandez format
            for l in lines[1:]:
                if not "->" in l:
                    continue
                colon = l.index(":")
                arrow = l.index(">")
                d[ eval( l[:colon] ) ] = list(eval( l[arrow+1:] )[:-1])
            check_lineage_input(d)
        else: # brute text (Mirabet Style)
            for l in lines:
                if (l == "\n") or (l.startswith("#")): continue
                # - Avoid sub_lineage and iterative list creation
                if "[" in l:
                    l=l.replace("[", "")
                    l=l.replace("]", "")
                # - Avoid commentary marked with "#":
                l = l.rsplit("#")[0]
                # - Avoid SyntaxError if '*' marker in the lineage file.
                try:
                    values = list(eval(l.replace(" ", ",")))
                except SyntaxError:
                    l=l.replace("*", "")
                    values = list(eval(l.replace(" ", ",")))
                if len(values) <= 1: continue
                d[values[0]] = values[1:]
    return d


def sub_lineage_from_file(filename):
    """Reads a lineage definition file and outputs a dict"""
    d = {}
    with open(filename, "r") as f:

        # -- in case the file is a python module that encodes the mapping
        # as a dictionnary. We simply evaluate the second part of the assignment
        # operator ( thus the split("=")[1] ) --
        try:
            d = eval(f.read().split("=")[1])
            check_lineage_input(d)
        except:
            # --rewind and try classical read. --
            f.seek(0)

        lines = f.readlines()
        # -- Find out if we are looking at a mapping in the Romain Fernandez format --
        rf_style = False
        for l in lines:
            if "NombreDeCellules" in l:
                rf_style = True
                break

        if rf_style: # is RomainFernandez format
            for l in lines[1:]:
                if not "->" in l:
                    continue
                colon = l.index(":")
                arrow = l.index(">")
                d[ eval( l[:colon] ) ] = list(eval( l[arrow+1:] )[:-1])
            check_lineage_input(d)
        else: # brute text (Mirabet Style)
            for l in lines:
                if l == "\n": continue
                # - Avoid commentary marked with "#":
                l = l.rsplit("#")[0]
                # - Avoid SyntaxError if '*' marker in the lineage file.
                try:
                    values = list(eval(l.replace(" ", ",")))
                except SyntaxError:
                    l=l.replace("*", "")
                    values = list(eval(l.replace(" ", ",")))
                if len(values) <= 1: continue
                d[values[0]] = values[1:]
    return d


def check_lineage_input(mapping):
    for mom, kids in mapping.items():
        if not isinstance(mom, int):
            raise Exception("Badly formatted mapping, mom %s not int"%str(mom))
        for kid in kids:
            if isinstance(kid, tuple):
                if not isinstance(kid[0], int):
                    raise Exception("Badly formatted mapping, kid %s of mom %d not int"%(str(kid), mom))
                if not isinstance(kid[1], float):
                    raise Exception("Badly formatted mapping, kid distance or score %s of mom %d not float"%(str(kid), mom))
            elif not isinstance(kid, int):
                raise Exception("Badly formatted mapping, kid %s of mom %d not int"%(str(kid), mom))

########################################################################
# End
########################################################################


def main():
    """ Allow to read lineage straight from this script file without importing the class first. """
    l = Lineage(sys.argv[1:])

if __name__ == '__main__':
    main()
