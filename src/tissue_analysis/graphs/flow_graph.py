# -*- python -*-
# -*- coding: utf-8 -*-
#
#       TissueAnalysis.graphs
#
#       Copyright 2017 INRIA - CIRAD - INRA - ENS Lyon
#
#       File author(s):
#           Jonathan LEGRAND <jonathan.legrand@ens-lyon.fr>
#
# ##############################################################################

"""
This module implement flow graphs methods.
"""

import time
import numpy as np
import networkx as nx

from tissue_analysis.util import elapsed_time


def flow_graph_from_coordinates(x, y, n_edges=15):
    """Match topological element coordinates x with y.

    Parameters
    ----------
    x : dict
        dictionary of length-3 numpy arrays of coordinates
    y : dict
        dictionary of length-3 numpy arrays of coordinates
    n_edges : int
        number of edge to draw per 'real' node, ie. limit to the first n_edges
        distance between Xi and Yi

    Returns
    -------
    G : networkx.DiGraph
        flow graph created from the dictionaries
    """
    from numpy.linalg import norm

    assert isinstance(x, dict)
    assert isinstance(y, dict)
    nb_x = len(x)
    nb_y = len(y)
    n_edges = min([n_edges, nb_y])
    # - Size is the index of the target node in G:
    size = 4 + nb_x + nb_y

    print("Initialising Directed Graph...")
    # - Initialise the matching graph as a Directed Graph:
    G = nx.DiGraph()
    # -- Add the SOURCE node:
    source_id = 1
    G.add_node(source_id, group=None, label='source', x=0, y=nb_x / 2,
               coords=(None, None))
    # -- Add the TARGET node:
    target_id = size
    G.add_node(target_id, group=None, label='target', x=3, y=nb_y / 2,
               coords=(None, None))

    # - Add the list of node of the first dict of coordinates:
    print(" -- Adding the Xi nodes and edges with source node...")
    xi_map = {}
    start_id_x = 2
    node_id = start_id_x
    for n, (ki, xi) in enumerate(x.items()):
        node_id = n + start_id_x
        G.add_node(node_id, group="Xi", label=ki, x=1, y=n, coords=xi)
        xi_map[ki] = node_id
        G.add_edge(source_id, node_id, capacity=1, weight=0)

    # - Add the "virtual node" to the first set:
    node_id += 1
    G.add_node(node_id, group="Xi", label='fake_x', x=1, y=n + 1,
               coords=(None, None))
    fake_x_id = xi_map['fake_x'] = node_id
    # - Add the edge between the "source node" and the "virtual node":
    G.add_edge(source_id, fake_x_id, capacity=np.abs(nb_x - nb_y), weight=0)

    # - Add the list of node of the second dict of coordinates:
    print(" -- Adding the Yi nodes and edges with target node...")
    yi_map = {}
    start_id_y = node_id + 1
    for n, (ki, yi) in enumerate(y.items()):
        node_id = n + start_id_y
        G.add_node(node_id, group="Yi", label=ki, x=2, y=n, coords=yi)
        yi_map[ki] = node_id
        G.add_edge(node_id, target_id, capacity=1, weight=0)

    # - Add the "virtual node" to the second set:
    node_id += 1
    G.add_node(node_id, group="Yi", label='fake_y', x=2, y=n + 1,
               coords=(None, None))
    fake_y_id = yi_map['fake_y'] = node_id
    G.add_edge(fake_y_id, target_id, capacity=np.abs(nb_x - nb_y), weight=0)

    # - Add the edges between the two sets of coordinates Xi & Yi:
    arr = np.array([[x[k], y[k]] for k in x.keys()])
    x_arr = arr[:, 0, :]
    y_arr = arr[:, 1, :]
    # -- Computing the pairwise euclidean distance matrix:
    print(" -- Computing the pairwise euclidean distance matrix...")
    t_start = time.time()
    dist_xy = norm(y_arr[np.newaxis, :] - x_arr[:, np.newaxis], axis=2)
    dist_xy = np.round(dist_xy * 10000, 0).astype(np.uint64)
    elapsed_time(t_start)
    # -- Get the maximum distance value:
    max_dist = np.max(dist_xy)
    # -- Reducing the Xi / Yi pairing to the smallest distances only:
    print(" -- Sorting the euclidean distance matrix...")
    dist_argsort = np.argsort(dist_xy)
    # -- Adding the Xi / Yi edges and euclidean distance as weight
    print(" -- Adding 'Xi'-'Yi' edges with the euclidean distance as weight...")
    t_start = time.time()
    for n in range(0, n_edges):
        xi, yi = np.where(dist_argsort == n)
        node_id_x = xi + start_id_x
        node_id_y = yi + start_id_y
        for i, (id_x, id_y) in enumerate(zip(node_id_x, node_id_y)):
            G.add_edge(id_x, id_y, capacity=1, weight=dist_xy[xi[i], yi[i]])

    elapsed_time(t_start)

    # - Add the edges between 'fake_x' & 'Yi:
    print(" -- Adding the 'fake node X' to 'Yi' edges...")
    for kyi, yi in y.items():
        G.add_edge(fake_x_id, yi_map[kyi], capacity=1, weight=int(max_dist + 1))
    print(" -- Adding the 'Xi' to 'fake node Y' edges...")
    # - Add the edges between 'Xi' & 'fake_y':
    for kxi, xi in x.items():
        G.add_edge(xi_map[kxi], fake_y_id, capacity=1, weight=int(max_dist + 1))

    # - Add the edges between the two fake nodes:
    print(" -- Adding the 'fake node X' to 'fake node Y' edge...")
    G.add_edge(fake_x_id, fake_y_id, capacity=min([nb_x, nb_y]),
               weight=int(max_dist + 1))

    return G


def labels_pairing_from_flow_graph(G):
    """Compute the max flow min cost function for the graph G and return paired
    labels (keys of the two dictionaries).

    Parameters
    ----------
    G : networkx.DiGraph
        flow graph created from the dictionaries of coordinates

    Returns
    -------
    list
        list of label pairs (tuples)
    """
    source_id = 1
    target_id = G.number_of_nodes()
    # - Compute the max flow min cost on the flow graph:
    mfmc = nx.algorithms.flow.max_flow_min_cost(G, source_id, target_id,
                                                capacity='capacity',
                                                weight='weight')
    # - Get the labels pairing:
    label_pairs = []
    for source, targets in mfmc.items():
        for target in targets:
            has_flow = mfmc[source][target] == 1
            not_source = G.node[source]['label'] != 'source'
            not_target = G.node[target]['label'] != 'target'
            if has_flow and not_source and not_target:
                source_label = G.node[source]['label']
                target_label = G.node[target]['label']
                label_pairs.append((source_label, target_label))

    return label_pairs


def draw_flow_graph(G, h_spacing=1., v_spacing=1.5, **kwargs):
    """Draw the flow graph G constructed using 'flow_graph_from_coordinates'.

    Parameters
    ----------
    G : nx.DiGraph
        directec flow graph
    h_spacing : float, optional
        horizontal spacing between the nodes, default=1.
    v_spacing : float, optional
        vertical spacing between the nodes, default=1.5
    kwargs : dict, optional
        optional keyword arguments passed to 'draw_networkx'
    """
    import matplotlib.pylab as plt

    # - Get the target id:
    target_id = G.number_of_nodes()
    # - Get x & y coordinates from the node dictionaries:
    coords = {}
    node_labels = {}
    for n in range(1, target_id + 1):
        coords[n] = [G.node[n]['x'] * h_spacing, G.node[n]['y'] * v_spacing]
        label = G.node[n]['label']
        # node_labels[n] = label if isinstance(label, str) else str(label)[1:-1]
        node_labels[n] = str(label)

    # - Make a list of node sizes:
    default_node_size = 200
    node_size = [1 * default_node_size for n in range(1, target_id + 1)]
    # -- Make "source" and "traget" nodes much bigger:
    node_size[0] = node_size[target_id - 1] = 5 * default_node_size

    # - Edge color:
    grey = (0.3, 0.3, 0.3)
    black = (0., 0., 0.)
    edge_color = []
    for e in G.edges():
        source_node = e[0]
        target_node = e[1]
        if G.node[source_node]['label'] == 'fake_x':
            edge_color.append(grey)
        elif G.node[target_node]['label'] == 'fake_y':
            edge_color.append(grey)
        else:
            edge_color.append(black)

    nx.drawing.nx_pylab.draw_networkx(G, coords, labels=node_labels,
                                      node_shape='s', node_size=node_size,
                                      edge_color=edge_color, **kwargs)
    plt.show()
