# -*- python -*-
# -*- coding: utf-8 -*-
#
#       TissueAnalysis.graphs
#
#       Copyright 2017 INRIA - CIRAD - INRA - ENS Lyon
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Frederic Boudon <frederic.boudon@cirad.fr>
#
################################################################################
"""This module helps to create TissueGraph from SpatialImages and cell lineages."""

__license__ = "Private/UnderDev"
__revision__ = ' $Id$ '

import warnings
import numpy as np
import pickle

from cellcomplex.graph import Graph
from cellcomplex.graph.property_graph import PropertyGraph,PropertyError
from cellcomplex.graph.temporal_property_graph import TemporalPropertyGraph

from tissue_analysis.util import stuple


# - SpatialImage <-> TissueGraph translation functions:
########################################################################
def translate_vid2label(graph, vids, time_point=None):
    """
    Translate `TissueGraph` ids `vids` to `SpatialImage` ids.

    Parameters
    ----------
    graph : TissueGraph
        TG containing the "translation" dictionary 'labels' mapping
        `SpatialImage`-ids to `TissueGraph`-ids
    vids : list
        list of vertex ids to translate
    time_point : int, optional
        temporal index of the SpatialImage in the TissueGraph, use it to
        filter the intermediary 'vertex2label' dict
    """
    if isinstance(vids, int):
        try:
            return graph.vertex_property('labels')[vids]
        except:
            if time_point is not None:
                prefix = "at time-point {}!".format(time_point)
            else:
                prefix = "!"
            print("vid '{}' is not linked to a label{}".format(vids, prefix))
    if isinstance(vids, set):
        vids = list(vids)
    if not isinstance(vids, list):
        raise ValueError('This is not an "int" or a "list" type variable.')

    vertex2label = graph.get_vertex2label_map(time_point)
    # - Check for missing translation keys:
    missing_vids = list(set(vids) - set(vertex2label.keys()))
    nb_missing_vids = len(missing_vids)
    if nb_missing_vids != 0:
        nb_vids = len(vids)
        p_missing = nb_missing_vids * 100 / nb_vids
        print("Some vids (n={}) had no corresponding label ({}% of 'vids')".format(
            nb_missing_vids, p_missing))
        print("Input list contains {} vids, against {} labels at time-point {}!".format(
            nb_vids, len(vertex2label.keys()), time_point))

    known_vids = list(set(vids) & set(vertex2label.keys()))
    return [graph.vertex_property('labels')[k] for k in known_vids]


def translate_label2vid(graph, labels, time_point):
    """
    Translate `SpatialImage` ids `labels` to `TissueGraph` vertex-ids.

    Parameters
    ----------
    graph : TissueGraph
        TG containing the "translation" dictionary 'labels' mapping
        `SpatialImage`-ids to `TissueGraph`-ids
    labels : int|list
        label or list of labels (cell-ids) to translate
    time_point : int
        temporal index of the SpatialImage in the TissueGraph

    Notes
    -----
    * `time_point` numbering starts at '0'.
    * `time_point` is mandatory since `SpatialImage` can contain similar ids.
    """
    label2vertex = graph.get_label2vertex_map(time_point)
    if isinstance(labels, int):
        try:
            return graph.vertex_property('labels')[label2vertex(labels)]
        except:
            print("Label '{}' is not linked to a vertex at time-point {}!".format(
                labels, time_point))
    if isinstance(labels, set):
        labels = list(labels)
    if (not isinstance(labels, list)) and (not isinstance(labels, int)):
        raise ValueError('This is not an "int" or a "list" type variable.')
    # - Check for missing translation keys:
    known_labels = list(set(labels) & set(label2vertex.keys()))
    missing_labels = list(set(labels) - set(label2vertex.keys()))
    nb_missing_labels = len(missing_labels)
    nb_labels = len(labels)
    if nb_missing_labels != 0:
        p_missing = nb_missing_labels * 100 / nb_labels
        print("Some label (n={}) had no corresponding vertex id ({}% of 'labels')".format(
            nb_missing_labels, p_missing))
        print("Input list contains {} labels, against {} vertices at time-point {}!".format(
            nb_labels, len(label2vertex.keys()), time_point))

    return [graph.vertex_property('labels')[label2vertex(k)] for k in
            known_labels]


def translate_keys_vid2label(graph, vid_dict, time_point=None):
    """
    Translate `vid_dict` keys of type `TissueGraph4D`-ids to `SpatialImage`-ids.

    Parameters
    ----------
    graph : TissueGraph4D
        TG containing the "translation" dictionary 'labels' mapping
        `SpatialImage`-ids to `TissueGraph`-ids
    vid_dict : dict
        keys are `TissueGraph`-ids type
    time_point : int, optional
        temporal index of the SpatialImage in the TissueGraph

    Notes
    -----
    * `time_point` numbering starts at '0'.
    * `time_point` is mandatory since `SpatialImage` can contain similar ids.
    """
    if not isinstance(vid_dict, dict):
        raise ValueError('This is not a "dict" type variable.')
    vertex2label = graph.get_vertex2label_map(time_point)
    vids = vid_dict.keys()
    # - Check for missing translation keys:
    known_vids = list(set(vids) & set(vertex2label.keys()))
    missing_vids = list(set(vids) - set(vertex2label.keys()))
    nb_missing_vids = len(missing_vids)
    nb_vids = len(vids)
    if nb_missing_vids != 0:
        p_missing = nb_missing_vids * 100 / nb_vids
        print("Some vids (n={}) had no corresponding label ({}% of 'vids')".format(
            nb_missing_vids, p_missing))
        print("Input list contains {} vids, against {} labels at time-point {}!".format(
            nb_vids, len(vertex2label.keys()), time_point))

    return {vertex2label[k]: vid_dict[k] for k in known_vids}


def translate_keys_label2vid(graph, label_dict, time_point):
    """
    Translate `label_dict` keys of type `SpatialImage`-ids to `TissueGraph`-ids.

    Parameters
    ----------
    graph : TissueGraph
        TG containing the "translation" dictionary 'labels' mapping
        `SpatialImage`-ids to `TissueGraph`-ids
    label_dict : dict
        keys are `SpatialImage`-ids type
    time_point : int
        temporal index of the SpatialImage in the TissueGraph

    Notes
    -----
    * `time_point` numbering starts at '0'.
    * `time_point` is mandatory since `SpatialImage` can contain similar ids.
    """
    if not isinstance(label_dict, dict):
        raise ValueError('This is not a "dict" type variable.')
    label2vertex = graph.get_label2vertex_map(time_point)
    labels = label_dict.keys()
    # - Check for missing translation keys:
    known_labels = list(set(labels) & set(label2vertex.keys()))
    missing_labels = list(set(labels) - set(label2vertex.keys()))
    nb_missing_labels = len(missing_labels)
    nb_labels = len(labels)
    if nb_missing_labels != 0:
        p_missing = nb_missing_labels * 100 / nb_labels
        print("Some label (n={}) had no corresponding vertex id ({}% of 'labels')".format(
            nb_missing_labels, p_missing))
        print("Input list contains {} labels, against {} vertices at time-point {}!".format(
            nb_labels, len(label2vertex.keys()), time_point))

    return dict(
        [(graph.vertex_property('labels')[label2vertex(k)], label_dict[k])
         for k in known_labels])


# -- Adding or extending graph property from SpatialImage labels:
########################################################################
def add_bool_atlas_data(graph, time_sorted_exp_patterns_fnames,
                        exp_patterns_time_steps, exp_pattern_as_cid=True,
                        patterns_path=""):
    """
    Add ATLAS data, "boolean" data giving all cells (values;cids/vids) expressing a given gene (keys).

    Parameters
    ----------
    graph : TissueGraph
        graph to complete
    time_sorted_exp_patterns_fnames : list
        list of strings, defining filenames of expression patterns
    exp_patterns_time_steps : list
        list of int, defining to which time-step the filenames of expression patterns relates to
    exp_pattern_as_cid : bool, optional
        says whether the cell ids are given as cids (image) or vids (tg)
    patterns_path : str, optional
        path where to find the filenames of expression patterns
    """
    # We list the time-steps to manually associate the correct time-point ID to each ATLAS:
    time_steps = graph.graph_property('time_steps')
    atlas_tp = [time_steps.index(ts) for ts in exp_patterns_time_steps]
    print("Detected 'time-steps' association with 'patterns files':\n{}".format(
        zip([time_steps[ts] for ts in atlas_tp],
            time_sorted_exp_patterns_fnames)))

    ATLAS_genes = []
    # Loop over the ATLAS to transfer genes expression levels to the TG:
    for tp, patterns_name in zip(atlas_tp, time_sorted_exp_patterns_fnames):
        print("\n")
        gene_dict = pickle.load(open(patterns_path + patterns_name,'r'))
        print("Detected the following list of genes: {}.".format(
            gene_dict.keys()))
        ATLAS_genes += gene_dict.keys()
        for gene_name, cids in gene_dict.iteritems():
            gene_name = str(gene_name)
            print("Exporting '{}' pattern at time point {}...".format(gene_name,
                                                                      tp))
            print("Found {} initial cell-ids".format(len(cids)))
            if exp_pattern_as_cid:
                vids = translate_label2vid(graph, cids, tp)
                ncids = len(cids)
                nvids = len(vids)
                print("and {}% ({}/{}) could be translated into vertex-ids!".format(
                    round(float(nvids) / ncids, 3) * 100, nvids, ncids))
            else:
                vids = cids
            graph.add_vertex_to_domain(vids, gene_name)

    return graph


class AbstractTissueGraph(object):
    """
    Object used to add SpatialImage / PropertyGraph|TemporalPropertyGraph
    relation of both static and dynamic (time-series) of images representing
    biological tissues.
    """

    def __init__(self, graph):
        """
        Constructor, check if 'labels' is defined as vertex property.
        This is mandatory to access the label <-> vertex-id translation dictionary
        linking SpatialImage labels to PropertyGraph|TemporalPropertyGraph vids.

        Parameters
        ----------
        graph : PropertyGraph|TemporalPropertyGraph
            a spatial (3D) or temporal (4D) graph build from SpatialImage(s)
        """
        try:
            assert 'labels' in graph.vertex_properties()
        except:
            raise AssertionError(
                "The provided graph should contain a vertex_property 'labels'")

    def domain_vids(self, domain_name):
        """
        Return a list of vids (TG vertex id type) that belong to the domain `domain_name` according to graph.

        Parameters
        ----------
        domain_name : str
            the name of a domain (e.g. from `self.add_vertex_to_domain())
        """
        # TODO: create 'self._domains' and use 'self.metavidtypepropertyname' to save list of vids as domains (as a graph_property)
        if 'domains' in list(self.vertex_properties()):
            return sorted(list(set(
                [k for k, v in self.vertex_property('domains').iteritems() for r
                 in v if r == domain_name])))
        else:
            print("No property 'domains' added to the graph yet!")
            return None

    def _add_vertex_to_domain(self, vids, domain_name):
        """
        Add a set of vertices 'vids' to a domain 'domain_name'.

        Parameters
        ----------
        vids : list
            list of vids to add to domain 'domain_name'
        domain_name : str
            the name of the domain

        Notes
        -----
        Saved in two places:
          * self.graph_property[domain_name]: returns the list of all vertices
            belonging to 'domain_name';
          * self.vertex_property["domains"][vid]: returns the list of all
            domains 'vid' belong to.
        """
        if "domains" not in self._vertex_property:
            self._vertex_property["domains"] = {}

        for vid in vids:
            # Adding domain_name to the "domain" property of each 'vid':
            if vid in self._vertex_property["domains"]:
                self._vertex_property["domains"][vid].append(domain_name)
            else:
                self._vertex_property["domains"][vid] = [domain_name]
            # Adding 'vid' to the 'domain_name' (graph_property) it belong to:
            self._graph_property[domain_name].append(vid)
        return

    def add_vertex_to_domain(self, vids, domain_name):
        """
        Add a list of vertex 'vids' to a domain 'domain_name'.

        Parameters
        ----------
        vids : list
            list of vids to add to domain 'domain_name'
        domain_name : str
            the name of the domain

        Notes
        -----
        * self.graph_property["domains"]: returns the list of saved domains;
        * self.graph_property['domain_name']: returns the list of all vertices
        belonging to 'domain_name'.
        """
        # TODO: make a function returning 'self.vertex_property["domains"][vid]' instead of saving it!
        # Add the 'domains' graph_property if missing:
        if "domains" not in self._graph_property:
            print("Initialisation of the 'domains' dictionary...")
            self.add_graph_property("domains", domain_name)
        # Add the 'vids' to graph_property 'domain_name':
        self.add_graph_property(domain_name, vids)

        self._add_vertex_to_domain(self._to_set(vids), domain_name)
        return

        # TODO: make a function returning boolean dict of domain from given 'domain_name'

    def _remove_vertex_from_domain(self, vids, domain_name):
        """
        Remove a set of vertices 'vids' from a domain 'domain_name'.

        Parameters
        ----------
        vids : list
            list of vids to remove from domain 'domain_name'
        domain_name : str
            the name of the domain

        Note
        ----
        'domain_name' should be defined in the object.
        """
        for vid in vids:
            self._vertex_property["domains"][vid].remove(domain_name)
            if self._vertex_property["domains"][vid] == []:
                self._vertex_property["domains"].pop(vid)

            self._graph_property[domain_name].remove(vid)
        return

    def remove_vertex_from_domain(self, vids, domain_name):
        """
        Remove a set of vertices 'vids' from a domain 'domain_name'.

        Parameters
        ----------
        vids : list
            list of vids to remove from domain 'domain_name'
        domain_name : str
            the name of the domain

        Note
        ----
        'domain_name' should be defined in the object.
        """
        if domain_name not in self._graph_property:
            raise PropertyError(
                "Property {} is not defined on graph".format(domain_name))
        self._remove_vertex_from_domain(self._to_set(vids), domain_name)
        return

    def add_domain_from_func(self, func, domain_name):
        """
        Create a domain 'domain_name' of vertices according to a function 'func'.

        Parameters
        ----------
        func : func
            the function to make the domain (might return True or False)
        domain_name : str
            the name of the domain

        Note
        ----
        'domain_name' should not be already defined in the object.
        """
        if domain_name in self._graph_property:
            raise PropertyError(
                "Property {} is already defined on graph".format(domain_name))
        self._graph_property[domain_name] = []
        if "domains" not in self._vertex_property.keys():
            self.add_vertex_property("domains")
        for vid in self._vertices.keys():
            if func(self, vid):
                self._add_vertex_to_domain({vid}, domain_name)
        return

    def add_domains_from_dict(self, dict_domains, domain_names):
        """
        If one already posses a dict indicating for a list of vertex which domain
        they belong to, it can be given to the graph directly.

        Parameters
        ----------
        dict_domains : dict
            dict {vid: domain_id}, where domain_id is the domain name index in
            domain_names
        domain_names : list
            a list containing the name of the domain(s)

        Note
        ----
        Each 'domain_name' in 'domain_names' should not be already defined in
        the object.
        """
        list_domains = set(dict_domains.values())
        if len(domain_names) != len(list_domains):
            warnings.warn(
                "You didn't provided the same number of domains and domain names.")
            pass

        if "domains" not in self._vertex_property.keys():
            self.add_vertex_property("domains")

        for domain, domain_name in enumerate(domain_names):
            if domain_name in self._graph_property:
                raise PropertyError(
                    "Property {} is already defined on graph".format(
                        domain_name))
            self._graph_property[domain_name] = []
            for vid in dict_domains:
                if dict_domains[vid] == list_domains[domain]:
                    self._add_vertex_to_domain({vid}, domain_name)
        return

    def iter_domain(self, domain_name):
        """
        Returns an iterator on the domain 'domain_name'

        Parameters
        ----------
        domain_name : str
            the name of the domain

        Note
        ----
        'domain_name' should be defined in the object.
        """
        if domain_name not in self._graph_property:
            raise PropertyError(
                "Property {} is not defined on graph".format(domain_name))
        return iter(self._graph_property[domain_name])

    def remove_domain(self, domain_name):
        """
        Remove a domain 'domain_name' from self._vertex_property and
        self._graph_property.

        Parameters
        ----------
        domain_name : str
            the name of the domain

        Note
        ----
        'domain_name' should be defined in the object.
        """
        # TODO: move to TissueGraph? class.

        if domain_name not in self._graph_property:
            raise PropertyError(
                "Property {} is not defined on graph".format(domain_name))

        for vid in self.iter_domain(domain_name):
            self._vertex_property["domains"][vid].remove(domain_name)
            if self._vertex_property["domains"][vid] == []:
                self._vertex_property["domains"].pop(vid)

        self._graph_property.pop(domain_name)
        return

    def is_connected_domain(self, domain_name, edge_type=None):
        """
        Return True if a domain 'domain_name'is connected, meaning if all vids
        belonging to 'domain_name' are connected by edges, else False.
        Edges can be restricted to a given type.

        Parameters
        ----------
        domain_name : str
            the name of the domain
        edge_type : str|set|None, optional
            edge type or set of edge types to consider, if None (default) uses all types

        Note
        ----
        'domain_name' should be defined in the object.
        """
        # TODO: move to TissueGraph? class.

        if domain_name not in self._graph_property:
            raise PropertyError("Property %s is not defined on graph"
                                % domain_name)
        domain_sub_graph = Graph.sub_graph(self,
                                           self._graph_property[domain_name])
        distances = domain_sub_graph.topological_distance(
            domain_sub_graph._vertices.keys()[0], edge_type=edge_type)
        return not float('inf') in distances.values()


def TissueGraph(graph, **kwargs):
    """
    TissueGraph Factory.
    Create a 'TissueGraph3D' or a 'TissueGraph4D' object if a 'PropertyGraph'
    or a 'TemporalPropertyGraph' is given as 'graph'.

    Parameters
    ----------
    graph : PropertyGraph|TemporalPropertyGraph
        spatial (3D) or temporal (4D) graph.
    kwargs : optional
        keywords arguments are given to TissueGraph*D constructor.

    Returns
    -------
    TissueGraph3D if 'graph' is a 'PropertyGraph';
    TissueGraph4D if 'graph' is a 'TemporalPropertyGraph'.
    """
    if isinstance(graph, PropertyGraph):
        return TissueGraph3D(graph, **kwargs)
    elif isinstance(graph, TemporalPropertyGraph):
        return TissueGraph4D(graph, **kwargs)
    else:
        raise TypeError(
            "Unknown type '{}' as 'graph' input!".format(type(graph)))


def existing_ids(ref_ids, ids):
    """
    Compare a list of ids to a reference ('ref_ids')

    Parameters
    ----------
    ref_ids : list
        list of reference ids
    ids : list
        list of ids that should be in ref_ids

    Returns
    -------
    ids_in, the list of 'ids' matching 'ref_ids'
    """
    ids_off = list(set(ids) - set(ref_ids))
    if ids_off != []:
        print("Some ids could not be found in the graph...")
    return list(set(ids) & set(ref_ids))


class TissueGraph3D(PropertyGraph):
    """
    TissueGraph3D allows for the following abstract representation:
      - A vertex represent a cell.
      - A 'structural' edge ('s') linking two vertices, represent a contact
        (wall/membrane) between them (topology).
    
    Use "vertex property" dictionaries to save cellular properties.
    Use "edge property" dictionaries to save wall properties.
    """

    # TODO: add 'units' parameter when adding edge or vertex ppty!
    def __init__(self, graph=None, image_path=None, **kwargs):
        """
        Constructor, check if 'labels' is defined as vertex property.
        This is mandatory to access the label <-> vertex-id translation
        dictionary linking SpatialImage labels to PropertyGraph vids.

        Parameters
        ----------
        graph : None|PropertyGraph
            a spatial (3D) graph build from SpatialImage, representing a fixed
            tissue. Contains a label <-> vertex-id translation dictionary saved
            under the vertex property 'labels'
        image_path : None|str
            file path to the labelled image used to generate the object

        Note
        ----
        if 'graph' is None, construct an EMPTY object.
        """
        min_area = kwargs.get('min_area', None)
        real_min_area = kwargs.get('real_min_area', None)
        verbose = kwargs.get('verbose', False)

        PropertyGraph.__init__(self, graph, **kwargs)
        if isinstance(graph, PropertyGraph):
            # - Check the presence of 'labels' vertex property in each `graph`:
            try:
                assert 'labels' in graph.vertex_properties()
            except:
                raise AssertionError(
                    "Input PropertyGraph should contain a 'labels' vertex property!")
            if verbose:
                print("Provided 'graph' object: {}".format(graph.__str__()))
                print("  - {} vertex / label dict elements".format(
                    len(graph._vertex_property['labels'])))
            # - Add the "vids to labels" mapping dictionary as an attribute:
            self._vids_to_labels = self._vertex_property['labels']
            # - Add the "labels to vids" mapping dictionary as an attribute:
            self._labels_to_vids = {v: k for k, v in
                                    self._vids_to_labels.iteritems()}
            # - Add the path of the image used to create the object:
            try:
                self.add_graph_property('image_path', image_path)
            except PropertyError as error:
                print(error)
            # - Add kwargs 'min_area' & 'real_min_area' as graph properties:
            if min_area is not None:
                try:
                    self.add_graph_property('min_area', min_area)
                except PropertyError as error:
                    print(error)
            if real_min_area is not None:
                try:
                    self.add_graph_property('real_min_area', real_min_area)
                except PropertyError as error:
                    print(error)
        elif graph is None:
            # - Initialise EMPTY TissueGraph graph properties:
            self.add_vertex_property('labels')
            # - Create TissueGraph attributes:
            self._vids_to_labels = self._vertex_property['labels']
            self._labels_to_vids = None
        else:
            raise TypeError(
                "Unknown initialisation method with input 'graph' type '{}'".format(
                    type(graph)))

    def __str__(self):
        """
        Format returned object information.
        """
        s = "'TissueGraph3D' object containing:\n"
        s += "  - {} vertices\n".format(len(self._vertices))
        s += "  - {} edges\n".format(len(self._edges))
        s += "  - {} vertex properties\n".format(len(self._vertex_property))
        s += "  - {} edge properties\n".format(len(self._edge_property))
        s += "  - {} graph properties\n".format(len(self._graph_property))
        s += "  - {} vertex / label translation dict entries\n".format(
            len(self._vertex_property['labels']))
        return s

    # ##########################################################################
    #
    # Overridden methods of PropertyGraph object:
    #
    # ##########################################################################
    def vertex_property(self, ppty_name, vids=None, labels=None):
        """
        Returns a vertex property dictionary.
        Filter with list of 'vids' or 'labels'.

        Parameters
        ----------
        ppty_name : str
            the name of an existing vertex property
        vids : list|None, optional
            if None (default) do not filter, else filter for these vids
        labels : list|None, optional
            if None (default) do not filter, else filter for these labels

        Returns
        -------
        a dictionary {vid: vid_ppty_value}
        """
        if vids is None:
            vids = set(self._vertices)
        elif isinstance(vids, int):
            vids = [vids]

        vids = set(vids)
        # - Filter by ppty keys:
        ppty = self._vertex_property[ppty_name]
        vids = vids & set(ppty)
        # - Filter by 'labels':
        if labels is not None:
            vids = vids & set(self.label2vertex(labels))
        return {k: ppty[k] for k in vids}

    # ##########################################################################
    #
    # New methods to return cell or wall features:
    #
    # ##########################################################################
    def label_property(self, ppty_name, vids=None, labels=None):
        """
        Returns a vertex property dictionary with SpatialImage labels as keys.

        Parameters
        ----------
        ppty_name : str
            a string referring to an existing vertex property 'ppty_name'
        vids : list|None
            if None (default), return it for all vertices
        labels : list|None, optional
            if None (default) do not filter, else filter for these labels

        Returns
        -------
        dictionary {label_i: 'ppty_name'_i}
        """
        v2l = self.get_vertex2label_map(vids=vids, labels=labels)
        ppty = self.vertex_property(ppty_name)
        return {v: ppty[k] for k, v in v2l}

    def edge_property_with_vidpairs(self, ppty_name, eids=None,
                                    edge_type=None):
        """
        Returns an edge property dictionary with vid-pairs as keys.

        Parameters
        ----------
        ppty_name : str
            a string referring to an existing edge property to extract

        Returns
        -------
        dict {(vid_i, vid_j): ppty_name(eid_ij)}
        """
        eid2vidpair = self.get_edge2vertexpair_map()
        ppty = self.edge_property(ppty_name, eids=eids)
        return {eid2vidpair[k]: ppty[k] for k in ppty}

    def edge_property_with_labelpairs(self, ppty_name):
        """
        Returns an edge property dictionary with label-pairs as keys.

        Parameters
        ----------
        ppty_name : str
            a string referring to an existing edge property to extract;

        Returns
        -------
        dict {(vid_i, vid_j): ppty_name(eid_ij)}
        """
        eid2labelpair = self.get_edge2labelpair_map()
        ppty = self.edge_property(ppty_name)
        return {eid2labelpair[k]: ppty[k] for k in ppty}

    # ##########################################################################
    #
    # New TRANSLATION methods:
    #
    # ##########################################################################
    def vertex2labels(self, vids):
        """
        Convert a list of 'vids' into labels, ignoring unknown values.

        Parameters
        ----------
        vids : list
            list of vids to transform into labels

        Returns
        -------
        labels : list
            list of vertex-ids converted into labels
        """
        return [self._vids_to_labels[i] for i in vids if i in self._vids_to_labels]

    def label2vertex(self, labels):
        """
        Convert a list of 'labels' into vids, ignoring unknown values.

        Parameters
        ----------
        labels : list
            list of labels to transform into vids

        Returns
        -------
        vertex : list
            list of labels converted into vertex-ids
        """
        return [self._labels_to_vids[l] for l in labels if l in self._labels_to_vids]

    # ##########################################################################
    #
    # New ID-MAPPING methods:
    #
    # ##########################################################################
    def get_vertex2label_map(self, vids=None, labels=None):
        """
        Returns a dictionary that map label to vertex id.

        Parameters
        ----------
        vids : list|None, optional
            if None (default) do not filter, else filter for vids with this
            temporal value
        labels : list|None, optional
            if None (default) do not filter, else filter for labels with this
            temporal value, make 'time_point' mandatory!

        Returns
        -------
        id_mapping : dict
            dictionary {vid_i: label_i} mapping vertex to label ids
        """
        return self.vertex_property('labels', vids=vids, labels=labels)

    def get_label2vertex_map(self, vids=None, labels=None):
        """
        Returns a dictionary that map label to vertex id.

        Parameters
        ----------
        vids : list|None, optional
            if None (default) do not filter, else filter for these vids
        labels : list|None, optional
            if None (default) do not filter, else filter for these labels

        Returns
        -------
        id_mapping : dict
            dictionary {label_i: vid_i} mapping label to vertex ids
        """
        v2l = self.get_vertex2label_map(vids=vids, labels=labels)
        return {j: i for i, j in v2l.iteritems()}

    def get_edge2labelpair_map(self, labelpairs=None):
        """
        Returns a dictionary mapping edge-ids to label-pairs.

        Parameters
        ----------
        labelpairs : list|None, optional
            if None (default) do not filter, else filter for these label-pairs

        Returns
        -------
        id_mapping : dict
            dictionary {eid_ij: (label_i, label_j)} mapping edge ids to tuples
            of label_pairs
        """
        from tissue_analysis.util import flatten_list
        labels = list(set(flatten_list(labelpairs)))
        v2l = self.get_vertex2label_map(labels=labels)
        s = self.source
        t = self.target
        edges = self.edges()
        return {eid: (v2l[s(eid)], v2l[t(eid)]) for eid in edges if (
                s(eid) in v2l and t(eid)) in v2l}

    def get_labelpair2edge_map(self, labelpairs=None):
        """
        Returns a dictionary mapping label-pairs to edge-ids.

        Parameters
        ----------
        labelpairs : list|None, optional
            if None (default) do not filter, else filter for these label-pairs

        Returns
        -------
        id_mapping : dict
            dictionary {(label_i, label_j): eid} mapping tuples of label_pairs
            to edge-ids
        """
        e2lp = self.get_edge2labelpair_map(labelpairs)
        return {j: i for i, j in e2lp.iteritems()}

    # ##########################################################################
    #
    # New methods:
    #
    # ##########################################################################
    def labels(self, vids=None, labels=None):
        """
        List the labels, can be filtered by 'vids' and 'labels.

        Parameters
        ----------
         vids : list|None, optional
            if None (default) do not filter, else filter for these vids
         labels : list|None, optional
            if None (default) do not filter, else filter for these labels

        Returns
        -------
        labels : list
            list of labels found in the object
        """
        return self.vertex_property('labels', vids=vids, labels=labels).values()

    # ##########################################################################
    #
    # Add vertex features from label ids:
    #
    # ##########################################################################
    def add_vertex_ppty_from_label_dict(self, name, label_dict,
                                        try_erase_first=False):
        """
        Add a vertex property of the object using SpatialImage labels as keys
        in 'label_dict'.

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        label_dict : dict
            dictionary {label: label_value} to add
        try_erase_first : bool, optional
            if True (default False) try to erase the property before adding it
        """
        lab2vtx = self.get_label2vertex_map()
        # - If 'name' already exist abort:
        if name in self.vertex_properties() and not try_erase_first:
            raise ValueError("Existing vertex property '{}'".format(name))
        # - Try to erase property first if required:
        if try_erase_first:
            print("You asked to try to erase property '{}'...".format(name))
            try:
                self.remove_vertex_property[name]
            except:
                print("It is not defined!".format(name))
            else:
                print("Done!")
        # - Check for missing translation keys:
        missing_vertex = list(set(label_dict.keys()) - set(lab2vtx.keys()))
        nb_missing_vertex = len(missing_vertex)
        nb_labels = len(label_dict)
        # - Translate the dict and check for unknown labels:
        vid_dict = {}
        for k, v in label_dict.iteritems():
            if k in lab2vtx.keys():
                vid_dict[lab2vtx[k]] = v
            else:
                continue  # already taken care of missing translation keys

        # - Add the property 'name' to the graph:
        self.add_vertex_property(name, vid_dict)

        # - Print about that:
        print("Added {} values to vertex property '{}'".format(
            nb_labels - nb_missing_vertex, name))

        if nb_missing_vertex != 0:
            p_missing = nb_missing_vertex * 100 / nb_labels
            print("-> Some label (n={}) had no corresponding vertex id ({}% of 'label_dict')".format(
                nb_missing_vertex, p_missing))
            print("-> Input dict contains {} label, against {} vertices in the object!".format(
                nb_labels, len(lab2vtx.keys())))
        return

    def add_vertex_ppty_from_label_and_value(self, name, labels, values,
                                             try_erase_first=False):
        """
        Add a vertex property to the object using lists of SpatialImage
        labels and values.

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        labels : list
            list of labels to add
        values : list
            list of values to add
        try_erase_first : bool, optional
            if True (default False) try to erase the property before adding it
        """
        label_dict = dict(zip(labels, values))
        return self.add_vertex_ppty_from_label_dict(name, label_dict,
                                                    try_erase_first)

    # ##########################################################################
    #
    # Add edge features from label-pairs:
    #
    # ##########################################################################
    def add_edge_ppty_from_labelpair_dict(self, name, labelpair_dict,
                                          try_erase_first=False):
        """
        Add an edge property to the object using a dict of SpatialImage
        label-pairs and values.

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        labelpair_dict : dict
            dictionary {(label_i, label_j): ppty_ij} to add
        try_erase_first : bool, optional
            if True (default False) try to erase the property before adding it
        """
        labp2eid = self.get_labelpair2edge_map(labelpair_dict.keys())
        # - If 'name' already exist abort:
        if name in self.edge_properties() and not try_erase_first:
            raise ValueError("Existing edge property '{}', ABORT!".format(name))
        # - Try to erase property first if required:
        if try_erase_first:
            print("You asked to try to erase property '{}'...".format(name))
            try:
                self.remove_edge_property[name]
            except:
                print("It is not defined!".format(name))
            else:
                print("Done!")
        # - Check for missing translation keys:
        missing_edge = list(
            set(labelpair_dict.keys()) - set(labp2eid.keys()))
        nb_missing_edge = len(missing_edge)
        nb_labelspair = len(labelpair_dict)
        # - Translate the dict and check for unknown labelpair and eid with values:
        eid_dict = {}
        for k, v in labelpair_dict.iteritems():
            # make sure the keys of the `labelpair_dict` are sorted tuples
            sk = tuple(sorted(k))
            if sk in labp2eid.keys():
                eid_dict[labp2eid[sk]] = v
            else:
                continue  # already taken care of missing translation keys

        # - Add the property 'name' to the graph:
        self.add_edge_property(name, eid_dict)

        # - Print about that:
        print("Added {} values to edge property '{}'!".format(
            nb_labelspair - nb_missing_edge, name))

        if nb_missing_edge != 0:
            p_missing = nb_missing_edge * 100 / nb_labelspair
            print("-> Some labelpair (n={}) had no corresponding edge id ({}% of 'labelpair_dict')".format(
                nb_missing_edge, p_missing))
            print("-> Input dict contains {} labelpair, against {} edges in the object!".format(
                nb_labelspair, len(labp2eid.keys())))
        return

    def add_edge_ppty_from_labelpair_and_value(self, name, label_pairs, values,
                                               try_erase_first=False):
        """
        Add an edge property to the object using lists of SpatialImage
        label-pairs and values.

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        label_pairs : list
            list of label-pairs to add
        values : list
            list of values to add
        try_erase_first : bool, optional
            if True (default False) try to erase the property before adding it
        """
        labelpairs_dict = dict(zip(label_pairs, values))
        return self.add_edge_ppty_from_labelpair_dict(name, labelpairs_dict,
                                                      try_erase_first)

    # ##########################################################################
    #
    # EXTEND vertex features from label-ids:
    #
    # ##########################################################################
    def extend_vertex_ppty_from_label_dict(self, name, label_dict):
        """
        Extend a vertex property of the object using a dict with
        SpatialImage labels as keys.
        If a label in 'labels' is not associated to a vid it is ignored.
        If a vid associated to a given label from 'labels' already has a
        value, it will not be updated!

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        label_dict : dict
            dictionary {label: label_value} to add
        """
        lab2vtx = self.get_label2vertex_map()
        # - If 'name' does not exist, add it:
        if name not in self.vertex_properties():
            self.add_vertex_ppty_from_label_dict(name, label_dict)
            return
        # - Check for missing translation keys:
        missing_vertex = list(set(label_dict.keys()) - set(lab2vtx.keys()))
        nb_missing_vertex = len(missing_vertex)
        nb_labels = len(label_dict)

        # - Translate the dict and check for unknown label and vid with values:
        vid_dict = {}
        vids_taken = []
        for k, v in label_dict.iteritems():
            if k in lab2vtx:
                if lab2vtx[k] in self.vertex_property(name):
                    vids_taken.append(lab2vtx[k])
                else:
                    vid_dict[lab2vtx[k]] = v
            else:
                continue  # already taken care of missing translation keys
        nb_taken = len(vids_taken)

        # - Add the property 'name' to the graph:
        self._vertex_property[name].update(vid_dict)

        # - Print about that:
        print("Added {} values to vertex property '{}'".format(
            nb_labels - nb_missing_vertex - nb_taken, name))

        # - Print those info, if any:
        if nb_taken != 0:
            p_taken = nb_taken * 100 / nb_labels
            print("-> {} vertex-ids were already attached to values ({}% of 'label_dict')".format(
                nb_taken, p_taken))

        if nb_missing_vertex != 0:
            p_missing = nb_missing_vertex * 100 / nb_labels
            print("-> Some label (n={}) had no corresponding vertex id ({}% of 'label_dict')".format(
                nb_missing_vertex, p_missing))
            print("-> Input dict contains {} label, against {} vertices in the object!".format(
                nb_labels, len(lab2vtx.keys())))
        return

    def extend_vertex_ppty_from_label_and_value(self, name, labels, values):
        """
        Extend a vertex property of the object using lists of SpatialImage
        labels and values.
        If a label in 'labels' is not associated to a vid it is ignored.
        If a vid associated to a given label from 'labels' already has a
        value, it will not be updated!

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        labels : list
            list of labels to add, sorted as values
        values : list
            list of values to add, sorted as labels
        """
        label_dict = dict(zip(labels, values))
        return self.extend_vertex_ppty_from_label_dict(name, label_dict)

    # ##########################################################################
    #
    # EXTEND edge features from label-pairs:
    #
    # ##########################################################################
    def extend_edge_ppty_from_labelpair_dict(self, name, labelpair_dict):
        """
        Extend an edge property of the object using a dict of SpatialImage
        label-pairs and values.
        If some of the edges already have a value, they will not be updated!

        Parameters
        ----------
        name : str
            the name of the edge property to add
        labelpair_dict : dict
            dictionary {(label_i, label_j): ppty_ij} to add
        """
        labp2eid = self.get_labelpair2edge_map()
        # - If 'name' does not exist add it:
        if name not in self.edge_properties():
            self.add_edge_ppty_from_labelpair_dict(name, labelpair_dict)
            return
        # - Check for missing translation keys:
        missing_edge = list(set(labelpair_dict.keys()) - set(labp2eid.keys()))
        nb_missing_edge = len(missing_edge)
        nb_labelspair = len(labelpair_dict)

        # - Translate the dict and check for unknown labelpair and eid with values:
        eid_dict = {}
        eids_taken = []
        for k, v in labelpair_dict.iteritems():
            # make sure the keys of the `labelpair_dict` are sorted tuples
            sk = tuple(sorted(k))
            if sk in labp2eid:
                if labp2eid[sk] in self.edge_property(name):
                    eids_taken.append(labp2eid[sk])
                else:
                    eid_dict[labp2eid[sk]] = v
            else:
                continue  # already taken care of missing translation keys
        nb_taken = len(eids_taken)

        # - Add the property values to the graph:
        self._edge_property[name].update(eid_dict)

        # - Print those information, if any:
        print("Added {} values to edge property '{}'".format(
            nb_labelspair - nb_missing_edge - nb_taken, name))

        if nb_taken != 0:
            p_taken = nb_taken * 100 / nb_labelspair
            print("-> {} edge-ids were already attached to values ({}% of 'labelpair_dict')".format(
                nb_taken, p_taken))

        if nb_missing_edge != 0:
            p_missing = nb_missing_edge * 100 / nb_labelspair
            print("-> Some labelpair (n={}) had no corresponding edge id ({}% of 'labelpair_dict')".format(
                nb_missing_edge, p_missing))
            print("-> Input dict contains {} labelpair, against {} edges in the object!".format(
                nb_labelspair, len(labp2eid.keys())))

        return

    def extend_edge_ppty_from_labelpair_and_value(self, name, label_pairs,
                                                  values):
        """
        Extend an edge property of the object using lists of SpatialImage
        label-pairs and values.
        If a label-pair in 'label_pairs' is not associated to an eid it is
        ignored.
        If an eid associated to a given label-pair from 'label_pairs' already has a
        value, it will not be updated!

        Parameters
        ----------
        name : str
            the name of the edge property to add
        label_pairs : list
            list of label-pairs to add, sorted as values
        values : list
            list of values to add, sorted as label_pairs
        """
        labelpair_dict = dict(zip(label_pairs, values))
        return self.extend_edge_ppty_from_labelpair_dict(name, labelpair_dict)


# class TissueGraph4D(AbstractTissueGraph, TemporalPropertyGraph):
class TissueGraph4D(TemporalPropertyGraph):
    """
    TissueGraph4D allows for the following abstract representation:
      - A vertex represent a cell.
      - A 'structural' edge ('s') linking two vertices, represent a contact
        (wall/membrane) between them (topology).
      - A 'temporal' edge ('s') linking two vertices, represent a lineage
        between them (ancestors/descendants).
    
    Use "vertex property" dictionaries to save cellular properties.
    Use "structural edge property" dictionaries to save wall properties.
    Use "temporal edge property" dictionaries to save time derivative
    properties.
    """

    def __init__(self, graphs=None, mappings=None, images_paths=None, **kwargs):
        """
        Constructor, check if 'labels' is defined as vertex property.
        This is mandatory to access the label <-> vertex-id translation
        dictionary linking SpatialImage labels to TemporalPropertyGraph vids.

        Parameters
        ----------
        graphs : None|list(PropertyGraph)|TemporalPropertyGraph|list(TissueGraph3D)
            a temporal (4D) property graph build from SpatialImage(s),
            representing a time series of tissue. Contains a label <-> vertex-id
            translation dictionary saved under the vertex property 'labels'
        mappings : None|list(dict)
            list of "temporal mapping" (dict), linking labels from different 
            time-points
        images_paths : None|list(str)
            list of file paths to the labelled images used to generate the
            object

        Note
        ----
        if 'graphs' is None, construct an EMPTY object.
        """
        # TODO: use a 'label_mapping' param, mapping label values, requires 'labels' as vertex property in the given graphs
        # TODO: use a 'vertex_mapping' param, mapping vertex ids
        min_area = kwargs.get('min_area', None)
        real_min_area = kwargs.get('real_min_area', None)

        TemporalPropertyGraph.__init__(self, graphs, mappings, **kwargs)
        if graphs is not None:
            # Check the presence of 'labels' vertex property in each `graphs`:
            try:
                assert all(['labels' in g.vertex_properties() for g in graphs])
            except:
                err = "Input 'graphs' should contain vertex_property 'labels'!"
                raise AssertionError(err)

            self._vids_to_labels = self._vertex_property['labels']
            # Can not create 'self._labels_to_vids' since labels are coming from
            # multiple images and may not be unique
            self.add_graph_property('images_paths', images_paths)
            # - Add kwargs graphs properties
            if min_area is not None:
                if 'min_area' not in self.graph_property_names():
                    self.add_graph_property('min_area', min_area)
                else:
                    assert np.alltrue(
                        [g.graph_property('min_area') == min_area for g in
                         graphs])
            if real_min_area is not None:
                if 'real_min_area' not in self.graph_property_names():
                    self.add_graph_property('real_min_area', real_min_area)
                else:
                    assert np.alltrue(
                        [g.graph_property('real_min_area') == real_min_area for
                         g in graphs])
        else:
            # - Initialise EMPTY TissueGraph graphs properties:
            self.add_vertex_property('labels')
            self.add_vertex_property('images_paths')
            # - Create TissueGraph attributes:
            self._vids_to_labels = self._vertex_property['labels']

    def __str__(self):
        """
        Format returned object information.
        """
        s = "Object 'TissueGraph4D' containing:\n"
        s += "  - {} time-points\n".format(self.nb_time_points)
        s += "  - {} vertices\n".format(len(self._vertices))
        s += "  - {} edges\n".format(len(self._edges))
        s += "  - {} vertex properties:\n".format(len(self._vertex_property))
        s += "    {}\n".format(self._vertex_property.keys())
        s += "  - {} edge properties:\n".format(len(self._edge_property))
        s += "    {}\n".format(self._edge_property.keys())
        s += "  - {} graph properties:\n".format(len(self._graph_property))
        s += "    {}\n".format(self._graph_property.keys())
        s += "  - {} vertex <--> label translation dict entries\n".format(
            len(self._vertex_property['labels']))
        return s

    def add_vertex_ppty_from_label_dict(self, name, label_dict,
                                        time_point, try_erase_first=False):
        """
        Add a vertex property of the object using SpatialImage labels as keys
        in 'label_dict'.

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        label_dict : dict
            dictionary {label: label_value} to add
        time_point : int
            temporal index of the labels
        try_erase_first : bool, optional
            try to erase the property before adding it
        """
        lab2vtx = self.get_label2vertex_map(time_point)
        # - If 'name' already exist abort:
        if name in self.vertex_properties() and not try_erase_first:
            raise ValueError("Existing vertex property '{}'".format(name))
        # - Try to erase property first if required:
        if try_erase_first:
            print("You asked to try to erase property '{}'...".format(name))
            try:
                self.remove_vertex_property[name]
            except:
                print("It is not defined!".format(name))
            else:
                print("Done!")
        # - Check for missing translation keys:
        missing_vertex = list(
            set(label_dict.keys()) - set(lab2vtx.keys()))
        nb_missing_vertex = len(missing_vertex)
        nb_labels = len(label_dict)
        if nb_missing_vertex != 0:
            p_missing = round(nb_missing_vertex * 100 / float(nb_labels), 1)
            print("Some label (n={}) had no corresponding vertex id ({}% of 'label_dict')".format(
                nb_missing_vertex, p_missing))
            print("Input dict contains {} label, against {} vertices at time-point {}!".format(
                len(label_dict.keys()), len(lab2vtx.keys()), time_point))
        # - Translate the dict and check for unknown labels:
        vid_dict = {}
        for k, v in label_dict.iteritems():
            if k in lab2vtx.keys():
                vid_dict[lab2vtx[k]] = v
            else:
                continue  # already taken care of missing translation keys
        # - Add the property 'name' to the graph:
        self.add_vertex_property(name, vid_dict)
        # - Print about that:
        print("Added '{}' values to vertex property '{}'".format(
            len(label_dict) - len(missing_vertex), name))
        if time_point is not None:
            print("(tp={})".format(time_point))
        else:
            print("")
        return

    def add_vertex_ppty_from_label_and_value(self, name, labels, values,
                                             time_point,
                                             try_erase_first=False):
        """
        Add a vertex property of the object using lists of SpatialImage
        labels and values.

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        labels : list
            list of labels to add
        values : list
            list of values to add
        time_point : int
            temporal index of the labels
        try_erase_first : bool, optional
            try to erase the property before adding it
        """
        label_dict = dict(zip(labels, values))
        return self.add_vertex_ppty_from_label_dict(name, label_dict,
                                                    time_point, try_erase_first)

    def add_edge_ppty_from_labelpair_dict(self, name, labelpair_dict,
                                          time_point, try_erase_first=False):
        """
        Add an edge property of the object using a dict of SpatialImage
        label-pairs and values.

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        labelpair_dict : dict
            dictionary {(label_i, label_j): ppty_ij} to add
        time_point : int
            temporal index of the labels
        try_erase_first : bool, optional
            try to erase the property before adding it
        """
        labp2eid = self.get_labelpair2edge_map(time_point)
        # - If 'name' already exist abort:
        if name in self.edge_properties() and not try_erase_first:
            raise ValueError("Existing edge property '{}', ABORT!".format(name))
        # - Try to erase property first if required:
        if try_erase_first:
            print("You asked to try to erase property '{}'...".format(name))
            try:
                self.remove_edge_property[name]
            except:
                print("It is not defined!".format(name))
            else:
                print("Done!")
        # - Check for missing translation keys:
        missing_edge = list(
            set(labelpair_dict.keys()) - set(labp2eid.keys()))
        nb_missing_edge = len(missing_edge)
        nb_labelspair = len(labelpair_dict)
        if nb_missing_edge != 0:
            p_missing = nb_missing_edge * 100 / nb_labelspair
            print("Some labelpair (n={}) had no corresponding edge id ({}% of 'labelpair_dict')".format(
                nb_missing_edge, p_missing))
            print("Input dict contains {} labelpair, against {} edges at time-point {}!".format(
                len(labelpair_dict.keys()), len(labp2eid.keys()),
                time_point))
        # - Translate the dict and check for unknown labelpair and eid with values:
        eid_dict = {}
        for k, v in labelpair_dict.iteritems():
            # make sure the keys of the `labelpair_dict` are sorted tuples
            sk = stuple(k)
            if sk in labp2eid.keys():
                eid_dict[labp2eid[sk]] = v
            else:
                continue  # already taken care of missing translation keys
        # - Add the property 'name' to the graph:
        self.add_edge_property(name, eid_dict)
        # - Print about that:
        print("Added '{}' values to edge property '{}'".format(
            len(labelpair_dict) - len(missing_edge), name))
        if time_point is not None:
            print("(tp={})".format(time_point))
        else:
            print("")
        return

    def add_edge_ppty_from_labelpair_and_value(self, name, label_pairs, values,
                                               time_point=None,
                                               try_erase_first=False):
        """
        Add an edge property of the object using lists of SpatialImage
        label-pairs and values.

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        label_pairs : list
            list of label-pairs to add
        values : list
            list of values to add
        time_point : int, optional
            temporal index of the label-pairs
        try_erase_first : bool, optional
            try to erase the property before adding it

        Note
        ----
        if you don't provide the 'labp2eid', you need to give 'time_point'
        """
        labelpairs_dict = dict(zip(label_pairs, values))
        return self.add_edge_ppty_from_labelpair_dict(name, labelpairs_dict,
                                                      time_point,
                                                      try_erase_first)

    def extend_vertex_ppty_from_label_dict(self, name, label_dict, time_point):
        """
        Extend a vertex property of the object using a dict with
        SpatialImage labels as keys.
        If a label in 'label_dict' has no matched vid ('labels') it is ignored.
        If a vid corresponding to a given label in 'label_dict' already has a
        value, it is not updated!

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        label_dict : dict
            dictionary {label: label_value} to add
        time_point : int
            temporal index of the labels
        """
        lab2vtx = self.get_label2vertex_map(time_point)
        # - If 'name' does not exist, add it:
        if name not in self.vertex_properties():
            self.add_vertex_ppty_from_label_dict(name, label_dict, time_point)
            return
        # - Check for missing translation keys:
        missing_vertex = list(
            set(label_dict.keys()) - set(lab2vtx.keys()))
        nb_missing_vertex = len(missing_vertex)
        nb_labels = len(label_dict)
        if nb_missing_vertex != 0:
            p_missing = nb_missing_vertex * 100 / nb_labels
            print("Some label (n={}) had no corresponding vertex id ({}% of 'label_dict')".format(
                nb_missing_vertex, p_missing))
            print("Input dict contains {} label, against {} vertices at time-point {}!".format(
                len(label_dict.keys()), len(lab2vtx.keys()), time_point))
        # - Translate the dict and check for unknown label and vid with values:
        vid_dict = {}
        vids_taken = []
        for k, v in label_dict.iteritems():
            if k in lab2vtx:
                if lab2vtx[k] in self.vertex_property(name):
                    vids_taken.append(lab2vtx[k])
                else:
                    vid_dict[lab2vtx[k]] = v
            else:
                continue  # already taken care of missing translation keys
        # - Print those info, if any:
        if vids_taken:
            print("{} vertex were already attached to values, they were not updated:\n{}".format(
                len(vids_taken), vids_taken))
        # - Add the property 'name' to the graph:
        self._vertex_property[name].update(vid_dict)
        # - Print about that:
        print("Added '{}' values to vertex property '{}'".format(
            len(label_dict) - nb_missing_vertex, name))
        if time_point is not None:
            print("(tp={})".format(time_point))
        else:
            print("")
        return

    def extend_vertex_ppty_from_label_and_value(self, name, label, values,
                                                time_point=None):
        """
        Extend a vertex property of the object using list of SpatialImage
        labels and values.
        If a label in 'label_dict' has no matched vid ('labels') it is ignored.
        If a vid corresponding to a given label in 'label_dict' already has a
        value, it is not updated!

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        label : list
            list of labels to add, sorted as values
        values : list
            list of values to add, sorted as labels
        time_point : int
            temporal index of the labels
        """
        label_dict = dict(zip(label, values))
        return self.extend_vertex_ppty_from_label_dict(name, label_dict,
                                                       time_point)

    def extend_edge_ppty_from_labelpair_dict(self, name, labelpair_dict,
                                             time_point=None):
        """
        Extend an edge property of the object using a dict of SpatialImage
        label-pairs and values.
        If some of the edges already have a value, they will not be updated!

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        labelpair_dict : dict
            dictionary {(label_i, label_j): ppty_ij} to add
        time_point : int
            temporal index of the label-pairs
        """
        labp2eid = self.get_labelpair2edge_map(time_point)
        # - If 'name' does not exist add it:
        if name not in self.edge_properties():
            self.add_edge_ppty_from_labelpair_dict(name, labelpair_dict,
                                                   time_point)
            return
        # - Check for missing translation keys:
        missing_edge = list(
            set(labelpair_dict.keys()) - set(labp2eid.keys()))
        nb_missing_edge = len(missing_edge)
        nb_labelspair = len(labelpair_dict)
        if nb_missing_edge != 0:
            p_missing = nb_missing_edge * 100 / nb_labelspair
            print("Some labelpair (n={}) had no corresponding edge id ({}% of 'labelpair_dict')".format(
                nb_missing_edge, p_missing))
            print("Input dict contains {} labelpair, against {} edges at time-point {}!".format(
                len(labelpair_dict.keys()), len(labp2eid.keys()),
                time_point))
        # - Translate the dict and check for unknown labelpair and eid with values:
        eid_dict = {}
        eids_taken = []
        for k, v in labelpair_dict.iteritems():
            sk = tuple(sorted(
                k))  # make sure the keys of the `labelpair_dict` are sorted tuples
            if sk in labp2eid:
                if labp2eid[sk] in self.edge_property(name):
                    eids_taken.append(labp2eid[sk])
                else:
                    eid_dict[labp2eid[sk]] = v
            else:
                continue  # already taken care of missing translation keys
        # - Print those information, if any:
        if eids_taken:
            print("{} edges were already attached to values, they were not updated:\n{}".format(
                len(eids_taken), eids_taken))
        # - Add the property values to the graph:
        self._edge_property[name].update(eid_dict)
        print("Added '{}' values to edge property '{}'".format(
            len(labelpair_dict) - nb_missing_edge, name))
        if time_point is not None:
            print("(tp={})".format(time_point))
        else:
            print("")

        return

    def extend_edge_ppty_from_labelpair_and_value(self, name, label_pairs,
                                                  values, time_point):
        """
        Extend an edge property of the object using a dict of SpatialImage
        label-pairs and values.
        If some of the edges already have a value, they will not be updated!

        Parameters
        ----------
        name : str
            the name of the vertex property to add
        label_pairs : list
            list of label-pairs to add, sorted as values
        values : list
            list of values to add, sorted as label_pairs
        time_point : int
            temporal index of the label-pairs
        """
        labelpair_dict = dict(zip(label_pairs, values))
        return self.extend_edge_ppty_from_labelpair_dict(name, labelpair_dict,
                                                         time_point)

    def labels_at_time(self, time_point, vids=None, labels=None):
        """
        List the labels of a given time-point, can be filtered by 'vids' and
         'labels.

        Parameters
        ----------
        time_point : int
            restrict returned list to this time-point
        vids : list|None, optional
            if None (default) do not filter, else filter for these vids
        labels : list|None, optional
            if None (default) do not filter, else filter for these labels
            Warning using it make definition of 'time_point' mandatory!

        Returns
        -------
        lis of labels
        """
        return self.vertex_property('labels', vids=vids, time_point=time_point,
                                    labels=labels).values()

    def get_vertex2label_map(self, time_point=None, vids=None, labels=None):
        """
        Compute a dictionary that map label to vertex id.

        Parameters
        ----------
        time_point : int, optional
            if None (default) do not filter, else filter for vids/labels with
            this temporal value
        vids : list|None, optional
            if None (default) do not filter, else filter for these vids
        labels : list|None, optional
            if None (default) do not filter, else filter for these labels
            Warning using it make definition of 'time_point' mandatory!

        Returns
        -------
        dictionary {vid_i: label_i}

        Note
        ----
        'time_point' is mandatory when defining 'labels' since they are not
        uniques
        """
        return self.vertex_property('labels', vids=vids, time_point=time_point,
                                    labels=labels)

    def get_label2vertex_map(self, time_point, vids=None, labels=None):
        """
        Compute a dictionary that map label to vertex id.
        This is performed for a given time point, since 'labels' are not unique
        and could result in several values for the same key... which is wrong!

        Parameters
        ----------
        time_point : int
            filter for labels with this temporal value
        vids : list|None, optional
            if None (default) do not filter, else filter for these vids
        labels : list|None, optional
            if None (default) do not filter, else filter for these labels

        Returns
        -------
        dictionary {label_i: vid_i}

        Note
        ----
        'time_point' is mandatory since SpatialImage labels are not uniques
        """
        v2l = self.get_vertex2label_map(time_point=time_point, vids=vids,
                                        labels=labels)
        return {j: i for i, j in v2l.iteritems()}

    def get_edge2labelpair_map(self, time_point=None,
                               edge_type=None, labelpairs=None):
        """
        Returns a dictionary mapping edge-ids to label-pairs.

        Parameters
        ----------
        time_point : int|None, optional
            restrict returned dictionary to this time-point
        edge_type : str|None, optional
            type of edge to use, by default (None) use them all, but can also be
            limited to 's' or 't'
        labelpairs : list|None, optional
            if None (default) do not filter, else filter for these label-pairs

        Returns
        -------
        dictionary {eid_ij: (label_i, label_j)}
        """
        from tissue_analysis.util import flatten_list
        labels = list(set(flatten_list(labelpairs)))
        v2l = self.get_vertex2label_map(time_point, labels=labels)
        s = self.source
        t = self.target
        edges = self.edges(edge_type=edge_type)
        return {eid: (v2l[s(eid)], v2l[t(eid)]) for eid in edges if
                s(eid) in v2l and t(eid) in v2l}

    def get_labelpair2edge_map(self, time_point, edge_type=None,
                               labelpairs=None):
        """
        Returns a dictionary mapping label-pairs, of a given 'time-point', to
        edge-ids.

        Parameters
        ----------
        time_point : int
            restrict returned dictionary to this time-point
        edge_type : str|None, optional
            type of edge to use, by default (None) use them all, but can also be
            limited to 's' or 't'
        labelpairs : list|None, optional
            if None (default) do not filter, else filter for these label-pairs
            Warning using it make definition of 'time_point' mandatory!

        Returns
        -------
        dictionary {(label_i, label_j): eid}

        Note
        ----
        'time_point' is mandatory since SpatialImage labels are not uniques
        """
        e2lp = self.get_edge2labelpair_map(time_point, edge_type, labelpairs)
        return {j: i for i, j in e2lp.iteritems()}

    def vertex2labels(self, vids, time_point=None):
        """
        Convert a list of 'vids' into labels, ignoring unknown values.

        Parameters
        ----------
        vids : list
            list of vids to transform into vid
        time_point : int, optional
            labels temporal index in the object

        Returns
        -------
        list

        Note
        ----
        'time_point' is mandatory since SpatialImage labels are not uniques
        """
        if time_point is None:
            return [self._vids_to_labels[i] for i in vids]
        else:
            tp_index = self.vertex_temporal_index
            return [self._vids_to_labels[i] for i in vids if
                    tp_index[i] == time_point]

    def label2vertex(self, labels, time_point):
        """
        Convert a list of 'labels' into vids, ignoring unknown values.

        Parameters
        ----------
        labels : list
            list of labels to transform into vids
        time_point : int
            labels temporal index in the object

        Returns
        -------
        list

        Note
        ----
        'time_point' is mandatory since SpatialImage labels are not uniques
        """
        if isinstance(labels, int):
            return self._old_to_new_vids[time_point][labels]
        else:
            return [self._old_to_new_vids[time_point][l] for l in labels]

    def vertex_property(self, ppty_name, vids=None, time_point=None,
                        labels=None, **kwargs):
        """
        Returns a vertex property dictionary.
        Filter with list of 'vids' or 'labels'.

        Parameters
        ----------
        ppty_name : str
            the name of an existing vertex property
        vids : list|None, optional
            if None (default) do not filter, else filter for these vids
        time_point : int, optional
            if None (default) do not filter, else filter for vids/labels with
            this temporal value
        labels : list|None, optional
            if None (default) do not filter, else filter for these labels
            Warning using it make definition of 'time_point' mandatory!

        Returns
        -------
        a dictionary {vid: vid_ppty_value}

        **kwargs
        --------
        lineaged : default=False,
            returned vids are lineaged over 'lineage_rank'
        fully_lineaged : default=False,
            returned vids are fully lineaged as ancestors over 'lineage_rank'
        as_ancestor : default=False,
            returned vids are lineaged as ancestors over 'lineage_rank'
        as_descendant : default=False,
            returned vids are lineaged as descendants over 'lineage_rank'
        lineage_rank : default=1,
            use to change temporal rank when checking for all other kwargs

        Note
        ----
        'time_point' is mandatory when defining 'labels' since they are not
        uniques
        """
        lineaged = kwargs.get('lineaged', False)
        fully_lineaged = kwargs.get('fully_lineaged', False)
        as_ancestor = kwargs.get('as_ancestor', False)
        as_descendant = kwargs.get('as_descendant', False)
        lineage_rank = kwargs.get('lineage_rank', 1)

        # - Get the property dictionary:
        ppty = self._vertex_property[ppty_name]
        # - Check the vids:
        if vids is None:
            vids = set(self._vertices)
        elif isinstance(vids, int):
            vids = [vids]
        else:
            vids = set(vids)
            # Filter by ppty keys
            vids_in = vids & set(ppty.keys())
            vids_off = vids - vids_in
            if vids_off:
                n_off = len(vids_off)
                # warnings.formatwarning()
                msg = "Property '{}' is missing '{}' vertex-id values!".format(ppty_name, n_off)
                warnings.warn(msg)
            vids = vids_in

        # Filter by 'time-point' or time-point and kwargs
        if time_point is not None:
            vids_f = self.vertex_at_time(time_point, lineaged, fully_lineaged,
                                         as_ancestor, as_descendant,
                                         lineage_rank)
            vids = vids & set(vids_f)
        elif lineaged:
            vids_f = self.lineaged_vertex(fully_lineaged, as_ancestor,
                                          as_descendant, lineage_rank)
            vids = vids & set(vids_f)
        # Filter by 'labels'
        if labels is not None:
            try:
                assert time_point is not None
            except AssertionError:
                raise ValueError(
                    "Parameter 'time_point' has to be defined if using 'labels'")
            else:
                vids = vids & set(self.label2vertex(labels, time_point))

        return {k: ppty[k] for k in vids}

    def label_property(self, ppty_name, time_point, vids=None,
                       labels=None, **kwargs):
        """
        Returns a vertex property dictionary with SpatialImage labels as keys.
        This is performed for a given time point, since 'labels' are not unique 
        and could result in several values for the same key... which is wrong!

        Parameters
        ----------
        ppty_name : str
            the name of an existing vertex property
        time_point : int
            filter for labels with this temporal value
        vids : list|None, optional
            if None (default) do not filter, else filter for these vids
        labels : list|None, optional
            if None (default) do not filter, else filter for these labels

        Returns
        -------
        dictionary {label_i: 'ppty_name'_i}

        **kwargs
        --------
        lineaged : default=False,
            returned vids are lineaged over 'lineage_rank'
        fully_lineaged : default=False,
            returned vids are fully lineaged as ancestors over 'lineage_rank'
        as_ancestor : default=False,
            returned vids are lineaged as ancestors over 'lineage_rank'
        as_descendant : default=False,
            returned vids are lineaged as descendants over 'lineage_rank'
        lineage_rank : default=1,
            use to change temporal rank when checking for all other kwargs

        Note
        ----
        'time_point' is mandatory when defining 'labels' since they are not
        uniques
        """
        lineaged = kwargs.get('lineaged', False)
        fully_lineaged = kwargs.get('fully_lineaged', False)
        as_ancestor = kwargs.get('as_ancestor', False)
        as_descendant = kwargs.get('as_descendant', False)
        lineage_rank = kwargs.get('lineage_rank', 1)

        v2l = self.get_vertex2label_map(time_point, vids, labels)
        ppty = self.vertex_property(ppty_name, vids, time_point, labels,
                                    lineaged=lineaged,
                                    fully_lineaged=fully_lineaged,
                                    as_ancestor=as_ancestor,
                                    as_descendant=as_descendant,
                                    lineage_rank=lineage_rank)
        return {v: ppty[k] for k, v in v2l.iteritems() if k in ppty}

    def edge_property_with_vidpairs(self, ppty_name, time_point, vids=None,
                                    edge_type=None):
        """
        Returns an edge property dictionary with vid-pairs as keys.

        Parameters
        ----------
        ppty_name : str
            a string referring to an existing edge property to extract
        time_point : int
            time-point for which to return the 'edge_property'
        vids : list|None, optional
            if None (default) do not filter, else filter for vids with this
            temporal value
        edge_type : str, optional
            type of edge to use, by default (None) use them all, but can also be
            limited to 's' or 't'

        Returns
        -------
        dict {(vid_i, vid_j): ppty_name(eid_ij)}
        """
        eid2vp = self.get_edge2vertexpair_map(vids, time_point, edge_type)
        ppty = self.edge_property(ppty_name)
        return {v: ppty[k] for k, v in eid2vp.iteritems() if k in ppty}

    def edge_property_with_labelpairs(self, ppty_name, time_point,
                                      edge_type=None, labelpairs=None):
        """
        Returns an edge property dictionary with label-pairs as keys.

        Parameters
        ----------
        ppty_name : str
            a string referring to an existing edge property to extract
        time_point : int
            time-point for which to return the 'edge_property'
        edge_type : str, optional
            type of edge to use, by default (None) use them all, but can also be
            limited to 's' or 't'

        Returns
        -------
        dict {(vid_i, vid_j): ppty_name(eid_ij)}
        """
        eid2lp = self.get_edge2labelpair_map(time_point, edge_type, labelpairs)
        ppty = self.edge_property(ppty_name)
        return {v: ppty[k] for k, v in eid2lp.iteritems() if k in ppty}

    def get_lineage(self, time_point, rank=1, as_labels=False):
        """
        Get the lineage between given 'time_point' and 'rank'.
        Lineage can be returned with label ids using 'as_labels=True'.

        Parameters
        ----------
        time_point : int
            temporal index of the vertex (or label) ids returned as key of the
            mapping dict
        rank : int, optional
            temporal distance, from 'time_point', of the descendants to return
        as_labels : bool, optional
            if False (default) return the mapping dict with vertex ids else with
            label ids

        Returns
        -------
        lineage : dict
            the lineage dictionary with keys from temporal index 'time_point'
            and values from 'time_point + rank'
        """
        assert self.exists_time_point(time_point)
        assert self.exists_time_point(time_point + rank)
        # - Get vids for given 'time_point' with lineage at 'rank':
        vids = self.vertex_at_time(time_point, lineage_rank=rank)
        # - Get the mapping dict for those vids:
        lineage = self.mapping_rank_descendants(vids, rank=rank)
        # - Translate to label ids if required:
        if as_labels:
            v2l = self.get_vertex2label_map()
            lineage = {int(v2l[k]): [int(v2l[d]) for d in v] for k, v in lineage.items()}

        return lineage

    def get_vertex_lineage(self, vids, rank=1):
        """
        Get the lineage dictionary for the given list of vertex ids and temporal
        distance 'rank'.

        Parameters
        ----------
        vids: list
            list of vid for which to extract the lineage
        rank : int, optional
            temporal distance, from each vids 'time_point', of the descendants
            to return

        Returns
        -------
        lineage : dict
            the lineage dictionary with 'vids' as keys and the list of their
            descendants at 'rank' as values
        """
        lineage = self.mapping_rank_descendants(vids, rank=rank)
        return lineage

    def get_label_lineage(self, labels, time_point, rank=1):
        """
        Get the lineage dictionary for the given list of labels and temporal
        distance 'rank'.

        Parameters
        ----------
        labels: list
            list of label for which to extract the lineage
        time_point : int
            temporal index of the vertex (or label) ids returned as key of the
            mapping dict
        rank : int, optional
            temporal distance, from each vids 'time_point', of the descendants
            to return

        Returns
        -------
        lineage : dict
            the lineage dictionary with 'vids' as keys and the list of their
            descendants at 'rank' as values
        """
        vids = self.label2vertex(labels, time_point=time_point)
        lineage = self.mapping_rank_descendants(vids, rank=rank)
        # - Translate to label ids:
        v2l = self.get_vertex2label_map()
        lineage = {v2l[k]: [v2l[d] for d in v] for k, v in lineage.items()}
        return lineage
