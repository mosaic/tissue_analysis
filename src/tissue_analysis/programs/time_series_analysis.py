# -*- python -*-
# -*- coding: utf-8 -*-
#
#       TissueAnalysis
#
#       Copyright 2018 CNRS - ENS Lyon
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# -----------------------------------------------------------------------------

"""
Regroup functions dedicated to the analysis of a time-series of images.
"""
import numpy as np
from scipy.linalg import svd
from timagetk.components import TissueImage
from timagetk.components.labelled_image import relabel_from_mapping
from tissue_analysis.array_tools import affine_deformation_tensor, \
    fractional_anisotropy_eigenval, projection_matrix, strain_anisotropy_2d
from tissue_analysis import TissueAnalysis


def landmark_pairing(pts0, pts1):
    """ Defines landmarks per cell using two dictionaries of coordinates.

    Parameters
    ----------
    pts0 : dict
        dictionary {poi_id: coordinate}
    pts1 : dict
        dictionary {poi_id: coordinate}

    Returns
    -------
    dict
        cell based dictionary {cell_id: [coordinate_tn, coordinate_tn+1]}

    """
    ldmk = {}
    for k, coord0 in pts0.items():
        try:
            coord1 = pts1[k]
        except:
            continue
        if isinstance(k, int):
            if k == 0 or k == 1:
                continue
            if k in ldmk:
                ldmk[k].append([coord0, coord1])
            else:
                ldmk[k] = [[coord0, coord1]]
        else:
            for kk in k:
                if kk == 0 or kk == 1:
                    continue
                if kk in ldmk:
                    ldmk[kk].append([coord0, coord1])
                else:
                    ldmk[kk] = [[coord0, coord1]]
    # - Checking for keys defined @ T_n and not @ T_n+1 or the other way around:
    missing_float_tp_keys = list(set(pts1.keys()) - set(pts0.keys()))
    missing_ref_keys = list(set(pts0.keys()) - set(pts1.keys()))
    if missing_float_tp_keys:
        print("Missing keys @ T_n wrt T_n+1: {}".format(missing_float_tp_keys))
    if missing_ref_keys:
        print("Missing keys @ T_n+1 wrt T_n: {}".format(missing_ref_keys))

    return ldmk


def epidermal_landmark_pairing(pts0, pts1, bkgd_id=1):
    """ Defines landmarks per epidermal wall using two dictionaries of coordinates.

    Parameters
    ----------
    pts0 : dict
        dictionary {poi_id: coordinate}
    pts1 : dict
        dictionary {poi_id: coordinate}
    bkgd_id : int, optional
        background label, default is 1

    Returns
    -------
    dict
        cell based dictionary {cell_id: [coordinate_tn, coordinate_tn+1]}

    """
    ldmk = {}
    for k, coord0 in pts0.items():
        try:
            coord1 = pts1[k]
        except:
            continue
        if isinstance(k, int):
            continue
        else:
            if bkgd_id not in k:
                continue
            for kk in k:
                if kk == 0 or kk == bkgd_id:
                    continue
                if (bkgd_id, kk) in ldmk:
                    ldmk[(bkgd_id, kk)].append([coord0, coord1])
                else:
                    ldmk[(bkgd_id, kk)] = [[coord0, coord1]]

    return ldmk


def landmark_scoring(pts0):
    """ Compute landmark "undefinition score" as the percentage of landmarks
    defined with the 'not a label' ids.

    Parameters
    ----------
    pts0 :  dict
        dictionary {poi_id: coordinate}

    Returns
    -------
    dict
        the undefinition score per cell
    """
    # - Counting number of landmarks per cell:
    n_wall, n_edge, n_vertex = {}, {}, {}
    for k, coord0 in pts0.items():
        if isinstance(k, int):
            n_wall[k] = 0
            n_edge[k] = 0
            n_vertex[k] = 0
        elif len(k) == 2:
            for kk in k:
                if kk != 0 and kk != 1:
                    n_wall[kk] += 1
        elif len(k) == 3:
            for kk in k:
                if kk != 0 and kk != 1:
                    n_edge[kk] += 1
        elif len(k) == 4:
            for kk in k:
                if kk != 0 and kk != 1:
                    n_vertex[kk] += 1
        else:
            pass

    # - Checking for "undefined landmarks" (ie. with 0 as neighbors):
    undef_wall, undef_edge, undef_vertex = {}, {}, {}
    for k, coord0 in pts0.items():
        if isinstance(k, int):
            undef_wall[k] = 0
            undef_edge[k] = 0
            undef_vertex[k] = 0
        elif len(k) == 2 and 0 in k:
            for kk in k:
                if kk != 0 and kk != 1:
                    undef_wall[kk] += 1
        elif len(k) == 3 and 0 in k:
            for kk in k:
                if kk != 0 and kk != 1:
                    undef_edge[kk] += 1
        elif len(k) == 4 and 0 in k:
            for kk in k:
                if kk != 0 and kk != 1:
                    undef_vertex[kk] += 1
        else:
            pass

    # - Compute the score:
    ldmk_score = {}
    for k, coord0 in pts0.items():
        if isinstance(k, int):
            n_ldmk = n_wall[k] + n_edge[k] + n_vertex[k]
            n_undef_ldmk = undef_wall[k] + undef_edge[k] + undef_vertex[k]
            ldmk_score[k] = (1 - (n_ldmk - n_undef_ldmk) / n_ldmk) * 100

    return ldmk_score

def combine_dictionaries(dicts, na=np.nan):
    """ Combine a list of dictionaries by their keys.

    Parameters
    ----------
    dicts : list(dict)
        list of dictionaries to combine
    na : value
        value used when a key is missing in a dictionary

    Returns
    -------
    dict
        a dictionary with all the unique keys and values as a list of dicts values
    """
    unique_keys = set([])
    for d in dicts:
        unique_keys = unique_keys | set(d.keys())

    out_dict = {}
    for d in dicts:
        for k in unique_keys:
            try:
                v = d[k]
            except KeyError:
                v = na
            if k in out_dict:
                out_dict[k].append(v)
            else:
                out_dict[k] = [v]

    return out_dict

if __name__ == "__main__":
    """
    For help, in a terminal: `$ python time_series_analysis.py -h`
    
    Notes
    -----
    If the images are not in the same reference frame, you should provide the list
    of associated intensity images to performs sequence registration prior to any
    quantification. To do so, use the `--intensity_image` optional arguments.
    
    The output image format is used only for sequence registered images.
    """
    import os
    import time
    import argparse
    import numpy as np
    import pandas as pd

    from timagetk.io import imread
    from timagetk.util import elapsed_time, absolute_path
    from timagetk.io.util import assert_exists, get_pne

    from tissue_analysis import AVAILABLES_LINEAGE_FMT, \
    DEFAULT_LINEAGE_FMT, Lineage

    parser = argparse.ArgumentParser(
        description='Extract spatio-temporal information from a list of segmented image.',
        epilog="It is advised to performs a sequence registration before using this program!")

    parser.add_argument('time_series_name', type=str,
                        help="name to give to the time-series.")
    parser.add_argument('segmented_images', type=str, nargs='+',
                        help="list of segmented images to analyse.")

    lin = parser.add_argument_group('Lineage options')
    lin.add_argument('--lineages', type=str, nargs='+',
                        help="list of cell lineage files, should be one less than segmented images.")
    lin.add_argument('--lineage_format', type=str,
                        default=DEFAULT_LINEAGE_FMT, choices=AVAILABLES_LINEAGE_FMT,
                        help="lineage format to use, default is '{}'.".format(AVAILABLES_LINEAGE_FMT, DEFAULT_LINEAGE_FMT))

    ts = parser.add_argument_group('Time-steps options')
    ts.add_argument('-ts', '--time_steps', type=int, nargs='+',
                        help="list of time-steps indicating the time elapsed from the first time point, should have the same number than segmented images.")
    parser.add_argument('-u', '--time_steps_unit', type=str, default="h",
                        help="the unit of the time-steps, eg. 'min' or 'h' (default).")

    out = parser.add_argument_group('Output options')
    out.add_argument('--output_path', type=str,
                        help="if specified, change the output path.")

    args = parser.parse_args()
    # - Variables definition from argument parsing:

    # -- Segmented image:
    seg_img_fnames = args.segmented_images
    # Check the segmented images provided exists:
    seg_names = []
    for seg_img_fname in seg_img_fnames:
        assert_exists(seg_img_fname)
        path, name, ext = get_pne(seg_img_fname)
        seg_names.append(name)
    print("Got {} segmented images: '{}'".format(len(seg_names), seg_names))
    # - Defines the path from the common path of the segmented images:
    path = os.path.commonpath(seg_img_fnames)

    # -- Time-steps:
    time_steps = args.time_steps
    try:
        assert len(time_steps) == len(seg_img_fnames)
    except AssertionError:
        msg = "Not the same number of segmented images ({}) and time-steps ({})!"
        raise ValueError(msg.format(len(seg_img_fnames), len(time_steps)))

    ts_name = args.time_series_name

    # - OPTIONAL ARGUMENTS PARSING:
    # -- Output path:
    # Defines the output path:
    if args.output_path:
        if not args.output_path.startswith('/'):
            # Relative path case:
            out_path = absolute_path(path, args.output_path)
        else:
            # Absolute path case:
            out_path = args.output_path
    else:
        out_path = path
    # Make sure it exists:
    try:
        os.mkdir(out_path)
    except OSError:
        pass
    else:
        print("Created output path: {}".format(out_path))
    print("Files will be written under this path: {}".format(out_path))

    # -- Lineage format:
    lineage_fmt = args.lineage_format

    ############################################################################
    # - Start the timer:
    t_start = time.time()

    for float_tp, float_ts in enumerate(time_steps[:-1]):
        ref_tp = float_tp + 1
        ref_ts = time_steps[ref_tp]
        # Time interval between the two time-steps:
        DeltaT = ref_tp - float_tp
        # Read floating (T_n) and reference (T_n+1) segmented images:
        print("\n# - Loading t{} & t{} segmented images...".format(float_tp, ref_tp))
        float_seg_img = imread(seg_img_fnames[float_tp])
        ref_seg_img = imread(seg_img_fnames[ref_tp])
        # Load the lineage:
        print("\n# - Loading corresponding lineage...")
        lin_file = args.lineages[float_tp]
        lin = Lineage(lin_file, lineage_fmt=lineage_fmt)
        print("Got {} ancestors!".format(len(lin.ancestors_list())))
        # - Create TissueImage:
        print("\n# - Initializing TissueImage:")
        float_seg_img = TissueImage(float_seg_img, background=1, no_label_id=0,
                                    voxelsize=float_seg_img.voxelsize)
        ref_seg_img = TissueImage(ref_seg_img, background=1, no_label_id=0,
                                  voxelsize=ref_seg_img.voxelsize)

        # -- Keep only lineaged cells @ T_n:
        print("\n# - Relabelling T_n segmentation:")
        float_seg_img = float_seg_img.get_image_with_labels(lin.ancestors_list() + [1])  # also keep the background
        float_seg_img = TissueImage(float_seg_img, background=1, no_label_id=0,
                                    voxelsize=float_seg_img.voxelsize)

        # -- Relabel T_n+1 image with T_n lineaged labels:
        print("\n# - Relabelling T_n+1 segmentation:")
        mapping = {desc: anc for anc, desc_list in lin.lineage_dict.items() for desc in desc_list}
        mapping.update({1: 1})  # add background to mapping to avoid loosing it with `clear_unmapped=True`
        ref_seg_img = relabel_from_mapping(ref_seg_img, mapping, clear_unmapped=True, verbose=True)

        print("\n# - Initializing TissueAnalysis objects:")
        tissue0 = TissueAnalysis(float_seg_img, background=1)
        tissue1 = TissueAnalysis(ref_seg_img, background=1)

        print("\n# - Extracting cell volumes...")
        vol0 = tissue0.label.volume(real=True)
        vol1 = tissue1.label.volume(real=True)

        # print("\n# - Extracting wall areas...")
        # area0 = tissue0.wall.area(real=True)
        # area1 = tissue1.wall.area(real=True)

        print("\n# - Computing cell growth rates...")
        growth_rate = {k: (vol1[k] - vol0[k]) / vol0[k] * 1 / DeltaT for k in
                       vol0.keys()}
        log_growth_rate = {k: np.log(vol1[k] / vol0[k]) * 1 / DeltaT for k in
                           vol0.keys()}

        print("\n# - Extracting points of interest:")
        # - Extract Points Of Interest:
        pts0, pts1 = {}, {}

        # - Add cell barycenters as POI:
        com0 = tissue0.label.center_of_mass(real=True)
        com1 = tissue1.label.center_of_mass(real=True)
        c_ids = set(com0.keys()) & set(com1.keys()) - {1}
        msg = "Found {} common ids between T_n (n={}) and T_n+1 (n={}) cell center of mass!"
        print(msg.format(len(c_ids), len(com0), len(com1)))
        for k in c_ids:
            pts0.update({k: v for k, v in com0.items() if k in c_ids})
            pts1.update({k: v for k, v in com1.items() if k in c_ids})

        # - Add wall medians as POI:
        wm0 = tissue0.wall.get_medians(real=True)
        wm1 = tissue1.wall.get_medians(real=True)
        w_ids = set(wm0.keys()) & set(wm1.keys())
        msg = "Found {} common ids between T_n (n={}) and T_n+1 (n={}) wall median!"
        print(msg.format(len(w_ids), len(wm0), len(wm1)))
        for k in w_ids:
            pts0.update({k: v for k, v in wm0.items() if k in w_ids})
            pts1.update({k: v for k, v in wm1.items() if k in w_ids})

        # - Add edge medians as POI:
        em0 = tissue0.edge.get_medians(real=True)
        em1 = tissue1.edge.get_medians(real=True)
        e_ids = set(em0.keys()) & set(em1.keys())
        msg = "Found {} common ids between T_n (n={}) and T_n+1 (n={}) edge median!"
        print(msg.format(len(e_ids), len(em0), len(em1)))
        for k in e_ids:
            pts0.update({k: v for k, v in em0.items() if k in e_ids})
            pts1.update({k: v for k, v in em1.items() if k in e_ids})

        # - Add vertex medians as POI:
        vm0 = tissue0.vertex.get_medians(real=True)
        vm1 = tissue1.vertex.get_medians(real=True)
        v_ids = set(vm0.keys()) & set(vm1.keys())
        msg = "Found {} common ids between T_n (n={}) and T_n+1 (n={}) vertex median!"
        print(msg.format(len(v_ids), len(vm0), len(vm1)))
        for k in v_ids:
            pts0.update({k: v for k, v in vm0.items() if k in v_ids})
            pts1.update({k: v for k, v in vm1.items() if k in v_ids})

        print("\n# - Defining 3D landmarks from points of interest...")
        ldmk_3d = landmark_pairing(pts0, pts1)
        ldmk_score = landmark_scoring(pts0)

        print("\n# - Computing cells 3D deformation estimators...")
        # - Deformation estimators:
        def_tensor = {}
        stretch_tensors, r2_score = {}, {}
        strain_rate = {}
        strain_rates_anisotropy = {}
        volumetric_strain_rates = {}
        for c_id, ldmks in ldmk_3d.items():
            ldmks = np.array(ldmks)
            ldmk_0 = ldmks[:, 0]
            ldmk_1 = ldmks[:, 1]
            def_tensor[c_id], r2_score[c_id] = affine_deformation_tensor(ldmk_0, ldmk_1)
            stretch_tensors[c_id] = svd(def_tensor[c_id])
            stretch_ratios = stretch_tensors[c_id][1]
            strain_rate[c_id] = [np.log(dk) / float(DeltaT) for dk in stretch_ratios]
            strain_rates_anisotropy[c_id] = fractional_anisotropy_eigenval(strain_rate[c_id])
            volumetric_strain_rates[c_id] = sum(strain_rate[c_id])

        ########################################################################
        # - EPIDERM
        ########################################################################
        common_msg = "Found {} common epidermal {}, against {} @ T_n & {} @ T_n+1!"
        miss_t0_msg = "Missing {} epidermal {} @ T_n wrt T_n+1: {}"
        miss_t1_msg = "Missing {} epidermal {} @ T_n+1 wrt T_n: {}"

        print("\n# - Listing epidermal cells (from known neighbors)...")
        ep_cells0 = tissue0.label.list_epidermal_labels_from_neighbors()
        ep_cells1 = tissue1.label.list_epidermal_labels_from_neighbors()
        # - Checking for epidermal cells defined @ T_n and not @ T_n+1 or the other way around:
        missing_epw0 = list(set(ep_cells1) - set(ep_cells0))
        missing_epw1 = list(set(ep_cells0) - set(ep_cells1))
        if missing_epw0:
            print(miss_t0_msg.format(len(missing_epw0), 'cells', sorted(missing_epw0)))
        if missing_epw1:
            print(miss_t1_msg.format(len(missing_epw1), 'cells', sorted(missing_epw1)))
        # Defines surfacic cell ids:
        sc_ids = list(set(ep_cells0) & set(ep_cells1))
        print(common_msg.format(len(sc_ids), 'cells', len(ep_cells0), len(ep_cells1)))

        print("\n# - Listing epidermal walls...")
        ep_walls0 = tissue0.wall.list_epidermal_walls()
        ep_walls1 = tissue1.wall.list_epidermal_walls()
        # - Checking for epidermal walls defined @ T_n and not @ T_n+1 or the other way around:
        missing_epw0 = list(set(ep_walls1) - set(ep_walls0))
        missing_epw1 = list(set(ep_walls0) - set(ep_walls1))
        if missing_epw0:
            print(miss_t0_msg.format(len(missing_epw0), "walls", missing_epw0))
        if missing_epw1:
            print(miss_t1_msg.format(len(missing_epw1), "walls", missing_epw1))
        # Defines surfacic wall ids:
        sw_ids = list(set(ep_walls0) & set(ep_walls1) - {(0, 1)})
        print(common_msg.format(len(sw_ids), 'walls', len(ep_walls0), len(ep_walls1)))

        print("\n# - Listing epidermal edges...")
        ep_edges0 = tissue0.edge.list_epidermal_edges()
        ep_edges1 = tissue1.edge.list_epidermal_edges()
        # - Checking for epidermal edges defined @ T_n and not @ T_n+1 or the other way around:
        missing_epe0 = list(set(ep_edges1) - set(ep_edges0))
        missing_epe1 = list(set(ep_edges0) - set(ep_edges1))
        if missing_epe0:
            print(miss_t0_msg.format(len(missing_epe0), 'edges', missing_epe0))
        if missing_epe1:
            print(miss_t1_msg.format(len(missing_epe1), 'edges', missing_epe1))
        # Defines surfacic edge ids:
        se_ids = list(set(ep_edges0) & set(ep_edges1))
        print(common_msg.format(len(se_ids), 'edges', len(ep_edges0), len(ep_edges1)))

        print("\n# - Listing epidermal vertices...")
        ep_vertices0 = tissue0.vertex.list_epidermal_vertices()
        ep_vertices1 = tissue1.vertex.list_epidermal_vertices()
        # - Checking for epidermal vertices defined @ T_n and not @ T_n+1 or the other way around:
        missing_epv0 = list(set(ep_vertices1) - set(ep_vertices0))
        missing_epv1 = list(set(ep_vertices0) - set(ep_vertices1))
        if missing_epv0:
            print(miss_t0_msg.format(len(missing_epv0), "vertices", missing_epv0))
        if missing_epv1:
            print(miss_t1_msg.format(len(missing_epv1), "vertices", missing_epv1))
        # Defines surfacic vertices ids:
        sv_ids = list(set(ep_vertices0) & set(ep_vertices1))
        print(common_msg.format(len(sv_ids), 'vertices', len(ep_vertices0), len(ep_vertices1)))

        print("\n# - Defining surfacic 3D landmarks from points of interest...")
        surf_ldmk = epidermal_landmark_pairing(pts0, pts1)

        print("\n# - Projecting surfacic 3D landmarks in a 2D subspace...")
        # Get the wall voxel coordinate for those ids:
        walls0 = tissue0.wall.get_coordinates(sw_ids)
        walls1 = tissue1.wall.get_coordinates(sw_ids)
        # Compute the 2D projection matrix using those wall coordinates: 
        proj_mat0 = {sw_id: projection_matrix(walls0[sw_id], 2) for sw_id in sw_ids}
        proj_mat1 = {sw_id: projection_matrix(walls1[sw_id], 2) for sw_id in sw_ids}
        # Project landmarks using those projection matrix:
        flattened_ldmk = {}
        for sw_id in sw_ids:
            s_ldmks = np.array(surf_ldmk[sw_id].copy())
            if s_ldmks.shape[0] <= 2:
                continue
            s_ldmks[:, 0] = np.dot(s_ldmks[:, 0], proj_mat0[sw_id])
            s_ldmks[:, 1] = np.dot(s_ldmks[:, 1], proj_mat1[sw_id])
            # s_ldmks[:, 0] = np.dot(s_ldmks[:, 0]-wm0[sw_id], proj_mat0[sw_id])+wm0[sw_id]
            # s_ldmks[:, 1] = np.dot(s_ldmks[:, 1]-wm1[sw_id], proj_mat1[sw_id])+wm1[sw_id]
            flattened_ldmk[sw_id] = s_ldmks

        print("\n# - Computing cells 2D deformation estimators...")
        # - 2D deformation estimators:
        def_tensor_2d = {}
        stretch_tensors_2d, r2_score_2d = {}, {}
        strain_rate_2d = {}
        strain_rates_anisotropy_2d = {}
        areal_strain_rates_2d = {}
        for sw_id, ldmks in flattened_ldmk.items():
            ldmk_0 = ldmks[:, 0]
            ldmk_1 = ldmks[:, 1]
            _, c_id = sw_id
            def_tensor_2d[c_id], r2_score[c_id] = affine_deformation_tensor(ldmk_0, ldmk_1)
            stretch_tensors_2d[c_id] = svd(def_tensor_2d[c_id])
            stretch_ratios_2d = stretch_tensors_2d[c_id][1][0:2]
            strain_rate_2d[c_id] = [np.log(dk) / float(DeltaT) for dk in stretch_ratios_2d]
            strain_rates_anisotropy_2d[c_id] = strain_anisotropy_2d(strain_rate_2d[c_id])
            areal_strain_rates_2d[c_id] = sum(strain_rate_2d[c_id])

        big_dict = combine_dictionaries([vol0, vol1, strain_rate_2d, strain_rates_anisotropy_2d, areal_strain_rates_2d, strain_rate, strain_rates_anisotropy, volumetric_strain_rates, ldmk_score, growth_rate, log_growth_rate])
        df = pd.DataFrame.from_dict(big_dict, orient='index', columns=['volume mother', 'volume daughter', '2D strain rates', '2D strain rates anisotropy', 'areal strain rate', '3D strain rates', '3D strain rates anisotropy', 'volumetric strain rate', 'percentage of undefined 3D landmarks', 'volumetric growth_rate', 'log volumetric growth_rate'])
        # big_dict = combine_dictionaries([area0, area1, vol0, vol1, strain_rate_2d, strain_rates_anisotropy_2d, areal_strain_rates_2d, strain_rate, strain_rates_anisotropy, volumetric_strain_rates, ldmk_score, growth_rate, log_growth_rate])
        # df = pd.DataFrame.from_dict(big_dict, orient='index', columns=['epidermal area mother', 'epidermal area daughter', 'volume mother', 'volume daughter', '2D strain rates', '2D strain rates anisotropy', 'areal strain rate', '3D strain rates', '3D strain rates anisotropy', 'volumetric strain rate', 'percentage of undefined 3D landmarks', 'volumetric growth_rate', 'log volumetric growth_rate'])
        csv_path = os.path.join(out_path, "deformations_t{}-t{}.csv".format(float_tp, ref_tp))
        print("\n# - Saving it all to a csv file...")
        df.to_csv(csv_path, index_label='labels')
        print(csv_path)

    print("\nAll {}".format(elapsed_time(t_start)))